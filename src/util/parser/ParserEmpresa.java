package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import logica.dto.ActividadDto;
import logica.dto.EmpresaDto;
import util.DtoFactory;

public class ParserEmpresa {

	public EmpresaDto parseEmpresas(ResultSet rs) throws SQLException{
 		rs.next();
 		if(rs.getRow()==0)
 		{
 			return null;
 		}
		 
		
		return parseEmpresa(rs);
	}
	
	public EmpresaDto parseEmpresa(ResultSet rs) throws SQLException {
		EmpresaDto emp = new EmpresaDto();
		
		ActividadDto act = new ActividadDto();
 		 
		
		act.id = rs.getInt("id_actividad");
		act.nombre = rs.getString("nombre_act");
 		
		emp.id_empresa = rs.getInt("id_empresa"); 
		emp.nombre = rs.getString("nombre_emp");

		emp.actividad = act;
		emp.cif = rs.getString("cif");
		 
		emp.pagado_empresa = rs.getDouble("pagado_empresa");
		emp.remuneracion = rs.getInt("remuneracion_empresa");
		
		ResultSet rsfact = new DtoFactory().getfacturas(emp.id_empresa, act.id);
		rsfact.next();
		if (rsfact.getRow()==0)
		{
			;
		}
		else
		{
		emp.factura_cantidad = rsfact.getDouble("cantidad_factura");
		emp.factura_fecha=rsfact.getString("fecha_factura");
		emp.factura_id= rsfact.getInt("id_factura");
		}

 		 
 		return emp;
	}
}
