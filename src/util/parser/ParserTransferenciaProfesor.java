package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.ActividadDto;
import logica.dto.InscripcionDto;
import logica.dto.ProfesorDto;
import logica.dto.TransferenciaProfesorDto;

public class ParserTransferenciaProfesor {
	public List<TransferenciaProfesorDto> parseTransferenciasProfesor(ResultSet rs) throws SQLException{
		List<TransferenciaProfesorDto> trans = new ArrayList<TransferenciaProfesorDto>();
		while(rs.next())
			trans.add(parseTransferenciaProfesor(rs));
		return trans;
	}
	
	public TransferenciaProfesorDto parseTransferenciaProfesor(ResultSet rs) throws SQLException {
		ActividadDto act = new ActividadDto();
		ProfesorDto pro = new ProfesorDto();
		TransferenciaProfesorDto trans = new TransferenciaProfesorDto();
		if(!rs.isBeforeFirst() || rs.next()) {
			trans.id = rs.getInt("id_transferencia_profesor");
			
			act.id = rs.getInt("id_actividad");
			pro.nombre = rs.getString("nombre");
			pro.id = rs.getInt("id_profesor");
			
			trans.actividad = act;
			trans.profesor = pro;
			trans.fecha_transferencia = rs.getDate("fecha_transferencia");
			trans.cantidad_pagada_por_COIIPA =rs.getDouble("cantidad_pagada_por_COIIPA");
			trans.cantidad_debida_profesor = rs.getDouble("cantidad_pagada_por_profesor");
			trans.tipoPago = rs.getString("tipo");
		}
		return trans;
	}

}
