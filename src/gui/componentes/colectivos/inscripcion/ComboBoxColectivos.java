package gui.componentes.colectivos.inscripcion;

import logica.dto.ColectivoDto;
import util.DtoFactory;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class ComboBoxColectivos extends JComboBox<ColectivoDto> {
	private static final long serialVersionUID = 1L;
	private List<ColectivoDto> colectivos;
	private DefaultComboBoxModel<ColectivoDto> modelo;

	public ComboBoxColectivos() {
		actualizarColectivos();
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ColectivoDto col = (ColectivoDto) getSelectedItem();
				if (col != null && col.id == -1) {
					crearNuevoColectivo();
				}
			}
		});
		setSelectedIndex(0);
	}

	public ColectivoDto getColectivo() {
		return (ColectivoDto) getSelectedItem();
	}

	private void crearNuevoColectivo() {
		String nombre = JOptionPane.showInputDialog(this, "Introduce nombre de nuevo colectivo:");
		nombre = nombre.substring(0, 1).toUpperCase().concat(nombre.substring(1, nombre.length()).toLowerCase());
		int id = colectivos.get(colectivos.size() - 1).id + 1;
		ColectivoDto nuevoColectivo = new ColectivoDto(id, nombre);
		try {
			if (DtoFactory.getColectivoByName(nuevoColectivo.nombre) != null)
				throw new IllegalArgumentException("Ya existe un colectivo con este nombre");
			if (!DtoFactory.saveColectivo(nuevoColectivo))
				throw new SQLException();
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Se ha producido un error. No se ha podido crear el nuevo colectivo.");
		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

		actualizarColectivos();
	}

	public void actualizarColectivos() {
		try {
			colectivos = new DtoFactory().getColectivos();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		actualizarModelo(true);
	}

	public void actualizarColectivosActividad(int idActividad) {
		try {
			colectivos = new DtoFactory().getColectivosActividad(idActividad);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		actualizarModelo(false);
	}
	
	private void actualizarModelo(boolean otro) {
		crearModelo(otro);
	}

	private void crearModelo(boolean aņadirOtro) {
		ColectivoDto[] array = (ColectivoDto[]) colectivos.toArray(new ColectivoDto[colectivos.size() + 1]);
		
		if(aņadirOtro) {
			ColectivoDto otro = new ColectivoDto(-1, "Otro...");
			array[array.length - 1] = otro;
		}

		int select = getSelectedIndex();

		modelo = new DefaultComboBoxModel<ColectivoDto>(array);
		setModel(modelo);
		setSelectedIndex(0);

		if (select != -1)
			setSelectedIndex(select);
	}
}
