package gui.responsableFormacion;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.GridLayout;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import java.awt.FlowLayout;
import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import com.toedter.calendar.JDateChooser;

import connection.ConnectionUtil;
import logica.EstadoActividad;
import logica.dto.ActividadDto;
import util.DtoFactory;
import util.parser.Conf;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class VentanaBalanceActividades extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private CustomRenderer crAbiertas;
	private CustomRenderer crCerradas;
	
	private JFrame ventanaAnterior;

	private TableModel modeloCerradas;
	private TableModel modeloAbiertas;

	private JPanel pnPrincipal;
	private JPanel pnTablas;
	private JPanel pnCerradas;
	private JLabel lbCerradas;
	private JScrollPane scrCerradas;
	private JTable tbCerradas;
	private JPanel pnAbiertas;
	private JLabel lbAbiertas;
	private JScrollPane scrAbiertas;
	private JTable tbAbiertas;
	private JPanel pnOpciones;
	private JComboBox<String> cbEstado;
	private JPanel pnEstado;
	private JLabel lbEstado;
	private JPanel pnFechas;
	private JDateChooser dtDesde;
	private JLabel lblDesde;
	private JLabel lblHasta;
	private JDateChooser dtHasta;
	private JPanel pnChooser;
	private JPanel pnCombo;
	private JComboBox<String> cbFecha;
	private JButton btnBuscar;
	private JPanel pnInformacion;
	private JLabel lblEstado;
	private JTextField txEstado;
	private JLabel lblDesde_1;
	private JTextField txDesde;
	private JLabel lblHas;
	private JTextField txHasta;

	public VentanaBalanceActividades(JFrame v) {
		ventanaAnterior = v;
		
		try {
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateIngresosReales"));
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateGastosRealesEmpresa"));
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateGastosRealesProfesores"));
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateIngresosEstimadosTotales"));
		} catch (SQLException e) {
			e.printStackTrace();
			mostrarError();
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				ventanaAnterior.setVisible(true);
				dispose();
			}
		});
		setTitle("Balance de Actividades");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBalanceActividades.class.getResource("/img/logo-COIIPA.png")));
		crAbiertas = new CustomRenderer();
		crCerradas = new CustomRenderer();

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 1081, 601);
		pnPrincipal = new JPanel();
		pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnPrincipal);
		pnPrincipal.setLayout(new BorderLayout(0, 0));
		pnPrincipal.add(getPnTablas(), BorderLayout.CENTER);
		pnPrincipal.add(getPnInformacion(), BorderLayout.SOUTH);
		pnPrincipal.add(getPnOpciones(), BorderLayout.NORTH);
		mostrarTodasActividades();
		actualizarInformacion();
	}

	@SuppressWarnings("serial")
	private void crearModeloAbiertas(List<ActividadDto> actividades) {
		Object[] columnNames = { "Nombre", "Fecha", "Estado", "Ingresos", "Gastos", "Balance", "Ingresos estimados", "Gastos estimados", "Balance estimado" };
		modeloAbiertas = new DefaultTableModel(new Object[actividades.size()][6], columnNames) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tbAbiertas.setDefaultRenderer(tbCerradas.getColumnClass(0), crAbiertas);
		for (int i = 0; i < actividades.size(); i++) {
			int j = 0;
			ActividadDto act = actividades.get(i);
			modeloAbiertas.setValueAt(act.nombre, i, j++);
			modeloAbiertas.setValueAt(act.inicioActividad.toString().substring(0, 16), i, j++);
			modeloAbiertas.setValueAt(act.estado.toString(), i, j++);
			modeloAbiertas.setValueAt(act.ingresosReales, i, j++);
			modeloAbiertas.setValueAt(act.gastosReales, i, j++);
			modeloAbiertas.setValueAt(act.ingresosReales - act.gastosReales, i, j++);
			modeloAbiertas.setValueAt(act.ingresosEstimados, i, j++);
			modeloAbiertas.setValueAt(act.gastosEstimados, i, j++);
			modeloAbiertas.setValueAt(act.ingresosEstimados - act.gastosEstimados, i, j++);
		}

		tbAbiertas.setModel(modeloAbiertas);
	}

	@SuppressWarnings("serial")
	private void crearModeloCerradas(List<ActividadDto> actividades) {
		Object[] columnNames = { "Nombre", "Fecha", "Estado", "Total de ingresos", "Total de gastos", "Balance" };
		modeloCerradas = new DefaultTableModel(new Object[actividades.size()][6], columnNames) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		tbCerradas.setModel(modeloCerradas);
		
		tbCerradas.setDefaultRenderer(tbCerradas.getColumnClass(0), crCerradas);
		
		for (int i = 0; i < actividades.size(); i++) {
			int j = 0;
			ActividadDto act = actividades.get(i);
			modeloCerradas.setValueAt(act.nombre, i, j++);
			modeloCerradas.setValueAt(act.inicioActividad.toString().substring(0, 16), i, j++);
			modeloCerradas.setValueAt(act.estado.toString(), i, j++);
			modeloCerradas.setValueAt(act.ingresosReales, i, j++);
			modeloCerradas.setValueAt(act.gastosReales, i, j++);
			modeloCerradas.setValueAt(act.ingresosReales - act.gastosReales, i, j++);
		}

		tbCerradas.setModel(modeloCerradas);
	}

	private void mostrarTodasActividades() {
		mostrarCerradas();
		mostrarAbiertas();
	}

	private void mostrarCerradas() {
		try {
			Date desde = new java.sql.Date(dtDesde.getDate().getTime());
			Date hasta = new java.sql.Date(dtHasta.getDate().getTime());
			List<ActividadDto> cerradas = new DtoFactory().getActividadesPorEstadoYFecha(EstadoActividad.CERRADA, desde,
					hasta);
			List<ActividadDto> canceladas = new DtoFactory().getActividadesPorEstadoYFecha(EstadoActividad.CANCELADA, desde,
					hasta);
			cerradas.addAll(canceladas);
			crearModeloCerradas(cerradas);
		} catch (SQLException | IOException e) {
			mostrarError();
			e.printStackTrace();
		}
	}

	private void mostrarAbiertas() {
		try {
			Date desde = new java.sql.Date(dtDesde.getDate().getTime());
			Date hasta = new java.sql.Date(dtHasta.getDate().getTime());
			List<ActividadDto> abiertas = new DtoFactory().getActividadesAbiertas(desde, hasta);
			crearModeloAbiertas(abiertas);
		} catch (SQLException | IOException e) {
			mostrarError();
			e.printStackTrace();
		}
	}

	private JPanel getPnTablas() {
		if (pnTablas == null) {
			pnTablas = new JPanel();
			pnTablas.setLayout(new GridLayout(0, 1, 0, 5));
			pnTablas.add(getPnCerradas());
			pnTablas.add(getPnAbiertas());
		}
		return pnTablas;
	}

	private JPanel getPnCerradas() {
		if (pnCerradas == null) {
			pnCerradas = new JPanel();
			pnCerradas.setLayout(new BorderLayout(0, 0));
			pnCerradas.add(getLbCerradas(), BorderLayout.NORTH);
			pnCerradas.add(getScrCerradas());
		}
		return pnCerradas;
	}

	private JLabel getLbCerradas() {
		if (lbCerradas == null) {
			lbCerradas = new JLabel("Cerradas:");
		}
		return lbCerradas;
	}

	private JScrollPane getScrCerradas() {
		if (scrCerradas == null) {
			scrCerradas = new JScrollPane();
			scrCerradas.setViewportView(getTbCerradas());
		}
		return scrCerradas;
	}

	private JTable getTbCerradas() {
		if (tbCerradas == null) {
			tbCerradas = new JTable();
			tbCerradas.setRowSelectionAllowed(false);
			
		}
		return tbCerradas;
	}

	private JPanel getPnAbiertas() {
		if (pnAbiertas == null) {
			pnAbiertas = new JPanel();
			pnAbiertas.setLayout(new BorderLayout(0, 0));
			pnAbiertas.add(getLbAbiertas(), BorderLayout.NORTH);
			pnAbiertas.add(getScrAbiertas(), BorderLayout.CENTER);
		}
		return pnAbiertas;
	}

	private JLabel getLbAbiertas() {
		if (lbAbiertas == null) {
			lbAbiertas = new JLabel("Abiertas:");
		}
		return lbAbiertas;
	}

	private JScrollPane getScrAbiertas() {
		if (scrAbiertas == null) {
			scrAbiertas = new JScrollPane();
			scrAbiertas.setViewportView(getTbAbiertas());
		}
		return scrAbiertas;
	}

	private JTable getTbAbiertas() {
		if (tbAbiertas == null) {
			tbAbiertas = new JTable();
			tbAbiertas.setRowSelectionAllowed(false);
		}
		return tbAbiertas;
	}

	private JPanel getPnOpciones() {
		if (pnOpciones == null) {
			pnOpciones = new JPanel();
			pnOpciones.setBorder(new TitledBorder(null, "Ajustes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnOpciones.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnOpciones.add(getPnEstado());
			pnOpciones.add(getPnFechas());
			pnOpciones.add(getBtnBuscar());
		}
		return pnOpciones;
	}

	private JComboBox<String> getCbEstado() {
		if (cbEstado == null) {
			cbEstado = new JComboBox<String>();
			cbEstado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String estado = (String) cbEstado.getSelectedItem();
					pnTablas.remove(pnAbiertas);
					pnTablas.remove(pnCerradas);
					mostrarActividades(estado);
					actualizarInformacion();
				}
			});
			cbEstado.setFont(new Font("Tahoma", Font.PLAIN, 11));
			List<String> l = new ArrayList<String>();
			l.add("TODO");
			for (EstadoActividad e : EstadoActividad.values())
				l.add(e.toString());
			String[] a = new String[l.size()];
			cbEstado.setModel(new DefaultComboBoxModel<String>(l.toArray(a)));
		}
		return cbEstado;
	}
	
	private void mostrarActividades(String estado) {
		SwingUtilities.updateComponentTreeUI(this);
		switch(estado) {
			case "TODO":
				pnTablas.add(getPnCerradas());
				pnTablas.add(getPnAbiertas());
				mostrarTodasActividades();
				break;
			case "CERRADA":
				pnTablas.add(getPnCerradas());
				mostrarCerradas();
				break;
			default:
				pnTablas.add(getPnAbiertas());
				mostrar(estado);
				break;
		}
	}

	private void mostrar(String estado) {
		try {
			Date desde = new java.sql.Date(dtDesde.getDate().getTime());
			Date hasta = new java.sql.Date(dtHasta.getDate().getTime());
			EstadoActividad est =  EstadoActividad.getEnum(estado);
			List<ActividadDto> acts = new DtoFactory().getActividadesPorEstadoYFecha(est, desde,
					hasta);
			crearModeloAbiertas(acts);
		}catch(Exception e) {
			mostrarError();
			e.printStackTrace();
		}
	}
	
	private JPanel getPnEstado() {
		if (pnEstado == null) {
			pnEstado = new JPanel();
			pnEstado.add(getLbEstado());
			pnEstado.add(getCbEstado());
		}
		return pnEstado;
	}

	private JLabel getLbEstado() {
		if (lbEstado == null) {
			lbEstado = new JLabel("Estado:");
			lbEstado.setDisplayedMnemonic('E');
			lbEstado.setLabelFor(getCbEstado());
		}
		return lbEstado;
	}

	private JPanel getPnFechas() {
		if (pnFechas == null) {
			pnFechas = new JPanel();
			pnFechas.setLayout(new GridLayout(2, 1, 0, 5));
			pnFechas.add(getPnCombo());
			pnFechas.add(getPnChooser());
		}
		return pnFechas;
	}

	private JDateChooser getDtDesde() {
		if (dtDesde == null) {
			dtDesde = new JDateChooser();
			dtDesde.getCalendarButton().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cbFecha.setSelectedIndex(0);
				}
			});
			dtDesde.setDateFormatString("yyyy-MM-dd");
			dtDesde.getDateEditor().setDate(Date.valueOf(LocalDate.now().minusMonths(1)));
		}
		return dtDesde;
	}

	private JLabel getLblDesde() {
		if (lblDesde == null) {
			lblDesde = new JLabel("Desde");
			lblDesde.setDisplayedMnemonic('D');
			lblDesde.setLabelFor(getDtDesde());
		}
		return lblDesde;
	}

	private JLabel getLblHasta() {
		if (lblHasta == null) {
			lblHasta = new JLabel("hasta");
			lblHasta.setLabelFor(getDtHasta());
			lblHasta.setDisplayedMnemonic('h');
		}
		return lblHasta;
	}

	private JDateChooser getDtHasta() {
		if (dtHasta == null) {
			dtHasta = new JDateChooser();
			dtHasta.getCalendarButton().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cbFecha.setSelectedIndex(0);
				}
			});
			dtHasta.setDateFormatString("yyyy-MM-dd");
			dtHasta.getDateEditor().setDate(Date.valueOf(LocalDate.now()));
		}
		return dtHasta;
	}

	private void mostrarError() {
		JOptionPane.showMessageDialog(this, "Se ha producido un error.", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	@SuppressWarnings("serial")
	private class CustomRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			DefaultTableModel tm = (DefaultTableModel) table.getModel();

			if (column == tm.getColumnCount() - 1 || column == 5) {
				Double balance = (Double) tm.getValueAt(row, column);
				if (balance < 0)
					setForeground(Color.red);
				else if (balance > 0)
					setForeground(Color.green);
				else
					setForeground(Color.black);
			} else {
				setForeground(Color.black);
			}

			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		}
	}
	private JPanel getPnChooser() {
		if (pnChooser == null) {
			pnChooser = new JPanel();
			pnChooser.add(getLblDesde());
			pnChooser.add(getDtDesde());
			pnChooser.add(getLblHasta());
			pnChooser.add(getDtHasta());
		}
		return pnChooser;
	}
	private JPanel getPnCombo() {
		if (pnCombo == null) {
			pnCombo = new JPanel();
			pnCombo.add(getCbFecha());
		}
		return pnCombo;
	}
	private JComboBox<String> getCbFecha() {
		if (cbFecha == null) {
			cbFecha = new JComboBox<String>();
			cbFecha.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String seleccion = (String ) cbFecha.getSelectedItem();
					cambiarFechas(seleccion);
				}
			});
			cbFecha.setModel(new DefaultComboBoxModel<String>(new String[] {"-", "\u00DAltima semana", "\u00DAltimo mes", "\u00DAltimo a\u00F1o"}));
			cbFecha.setSelectedIndex(2);
		}
		return cbFecha;
	}
	
	private void cambiarFechas(String seleccion) {
		if(seleccion.equals("-"))	return;
		
		getDtHasta().getDateEditor().setDate(Date.valueOf(LocalDate.now()));
		
		switch(seleccion) {
			case "�ltimo mes":
				getDtDesde().getDateEditor().setDate(Date.valueOf(LocalDate.now().minusMonths(1)));
				break;
			case "�ltima semana":
				getDtDesde().getDateEditor().setDate(Date.valueOf(LocalDate.now().minusWeeks(1)));
				break;
			case "�ltimo a�o":
				getDtDesde().getDateEditor().setDate(Date.valueOf(LocalDate.now().minusYears(1)));
				break;
		}
	}
	
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Consultar");
			btnBuscar.setMnemonic('c');
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String estado = (String) cbEstado.getSelectedItem();
					pnTablas.remove(pnAbiertas);
					pnTablas.remove(pnCerradas);
					mostrarActividades(estado);
					actualizarInformacion();
				}
			});
		}
		return btnBuscar;
	}
	
	@SuppressWarnings("deprecation")
	private void actualizarInformacion() {
		txEstado.setText(cbEstado.getSelectedItem().toString());
		txDesde.setText(dtDesde.getDate().toGMTString().substring(0, 11));
		txHasta.setText(dtHasta.getDate().toGMTString().substring(0, 11));
	}
	
	private JPanel getPnInformacion() {
		if (pnInformacion == null) {
			pnInformacion = new JPanel();
			pnInformacion.add(getLblEstado());
			pnInformacion.add(getTxEstado());
			pnInformacion.add(getLblDesde_1());
			pnInformacion.add(getTxDesde());
			pnInformacion.add(getLblHas());
			pnInformacion.add(getTxHasta());
		}
		return pnInformacion;
	}
	private JLabel getLblEstado() {
		if (lblEstado == null) {
			lblEstado = new JLabel("Estado:");
		}
		return lblEstado;
	}
	private JTextField getTxEstado() {
		if (txEstado == null) {
			txEstado = new JTextField();
			txEstado.setEditable(false);
			txEstado.setColumns(17);
		}
		return txEstado;
	}
	private JLabel getLblDesde_1() {
		if (lblDesde_1 == null) {
			lblDesde_1 = new JLabel("Desde");
		}
		return lblDesde_1;
	}
	private JTextField getTxDesde() {
		if (txDesde == null) {
			txDesde = new JTextField();
			txDesde.setEditable(false);
			txDesde.setColumns(10);
		}
		return txDesde;
	}
	private JLabel getLblHas() {
		if (lblHas == null) {
			lblHas = new JLabel("hasta");
		}
		return lblHas;
	}
	private JTextField getTxHasta() {
		if (txHasta == null) {
			txHasta = new JTextField();
			txHasta.setEditable(false);
			txHasta.setColumns(10);
		}
		return txHasta;
	}
}
