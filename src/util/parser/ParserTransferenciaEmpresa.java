package util.parser;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.EmpresaDto;
import logica.dto.TransferenciasEmpresaDto;

public class ParserTransferenciaEmpresa {

	public List<TransferenciasEmpresaDto> parseTransferenciasEmpresas(ResultSet rs) throws SQLException {
		List<TransferenciasEmpresaDto> trans = new ArrayList<TransferenciasEmpresaDto>();
		while(rs.next())
			trans.add(parseTransferenciaEmpresa(rs));
		return trans;
	}
	public TransferenciasEmpresaDto parseTransferenciaEmpresa(ResultSet rs) throws SQLException {
		
		EmpresaDto emp = new EmpresaDto();
		emp.id_empresa = rs.getInt("id_empresa");
		TransferenciasEmpresaDto trans = new TransferenciasEmpresaDto();
		 
		trans.fecha_trans = (Date) rs.getDate("fecha_transferencia_empresa"); 
		trans.id_trans_emp = rs.getInt("id_transferencia_empresa");
		trans.empresa = emp;
		trans.cantidad_pagada = rs.getInt("cantidad_pagada_por_COIIPA");
 		trans.tipoPago = rs.getString("tipo");
		if(trans.cantidad_pagada<=0)
		{
			trans.movimento= "devolucion";
 
		}
		else
		{
		trans.movimento = "pago";
		}
		return trans;
	}

}
