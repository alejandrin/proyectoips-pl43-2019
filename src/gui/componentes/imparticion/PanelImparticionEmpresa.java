package gui.componentes.imparticion;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;

import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import logica.dto.DatosEmpresaDto;
import logica.dto.ProfesorDto;
import util.DtoFactory;

import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class PanelImparticionEmpresa extends JPanel {
	private JPanel pnEmpresa;
	private JPanel pnComboBox;
	private JComboBox<DatosEmpresaDto> cbEmpresa;
	private JPanel pnRemuneracion;
	private JLabel lbRemuneracion;
	private JTextField txRemuneracion;
	private JPanel pnProfesores;
	private JLabel lbProfesores;
	private JScrollPane scrProfesores;
	private JPanel pnTexto;
	private JTable tbProfesores;
	private DefaultTableModel modeloProfesores;
	private JPanel pnRegistroProfesor;
	private JButton btRegistrar;
	private DefaultComboBoxModel<DatosEmpresaDto> modeloEmpresas;

	private List<ProfesorDto> profesores;
	private List<DatosEmpresaDto> empresas;
	private DatosEmpresaDto otra;
	private JLabel lblEmpresa;
	private JPanel panel;
	private DatosEmpresaDto ninguna;

	/**
	 * Create the panel.
	 */
	public PanelImparticionEmpresa() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(getPnEmpresa());
		add(getPnProfesores());

		otra = new DatosEmpresaDto();
		otra.id_empresa = -1;
		otra.nombre = "Otra...";

	}

	private void cargarDatos() {
		try {
			profesores = new DtoFactory().getProfesores();
			
			ninguna = new DatosEmpresaDto();
			ninguna.nombre = "Ninguna";
			empresas = new ArrayList<DatosEmpresaDto>();
			empresas.add(ninguna);
			empresas.addAll( new DtoFactory().getDatosEmpresas() );
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error en la base de datos.", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void crearModeloEmpresas() {
		DatosEmpresaDto[] array = (DatosEmpresaDto[]) empresas.toArray(new DatosEmpresaDto[empresas.size() + 1]);

		array[array.length - 1] = otra;

		modeloEmpresas = new DefaultComboBoxModel<DatosEmpresaDto>(array);

		cbEmpresa.setModel(modeloEmpresas);

	}
	
	private void crearModeloProfesores() {
		String[] columns = { "Profesor" };

		modeloProfesores = new DefaultTableModel(new Object[profesores.size()][1], columns) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		for (int i = 0; i < profesores.size(); i++) {
			ProfesorDto profe = profesores.get(i);
			String nombreProfesor = profe.nombre + " " + profe.apellidos;
			modeloProfesores.setValueAt(nombreProfesor, i, 0);
		}

		tbProfesores.setModel(modeloProfesores);
	}

	private JPanel getPnEmpresa() {
		if (pnEmpresa == null) {
			pnEmpresa = new JPanel();
			pnEmpresa.setLayout(new GridLayout(2, 1, 0, 0));
			pnEmpresa.add(getPnComboBox());
			pnEmpresa.add(getPnRemuneracion());
		}
		return pnEmpresa;
	}

	private JPanel getPnComboBox() {
		if (pnComboBox == null) {
			pnComboBox = new JPanel();
			pnComboBox.setLayout(new BoxLayout(pnComboBox, BoxLayout.Y_AXIS));
			pnComboBox.add(getLblEmpresa());
			pnComboBox.add(getPanel());
		}
		return pnComboBox;
	}

	private JComboBox<DatosEmpresaDto> getCbEmpresa() {
		if (cbEmpresa == null) {
			cbEmpresa = new JComboBox<DatosEmpresaDto>();
			cbEmpresa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (cbEmpresa.getSelectedItem().equals(otra)) {
						nuevaEmpresa();
					}
				}
			});
		}
		return cbEmpresa;
	}

	private void nuevaEmpresa() {
		DialogNuevaEmpresa dialog = new DialogNuevaEmpresa(this);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	
	private void nuevoProfesor() {
		DialogNuevoProfesor dialog = new DialogNuevoProfesor(this);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	private JPanel getPnRemuneracion() {
		if (pnRemuneracion == null) {
			pnRemuneracion = new JPanel();
			pnRemuneracion.setLayout(new BoxLayout(pnRemuneracion, BoxLayout.PAGE_AXIS));
			pnRemuneracion.add(getLbRemuneracion());
			pnRemuneracion.add(getPnTexto());
		}
		return pnRemuneracion;
	}

	private JLabel getLbRemuneracion() {
		if (lbRemuneracion == null) {
			lbRemuneracion = new JLabel("Remuneracion");
		}
		return lbRemuneracion;
	}

	private JTextField getTxRemuneracion() {
		if (txRemuneracion == null) {
			txRemuneracion = new JTextField();
			txRemuneracion.setColumns(7);
			txRemuneracion.addKeyListener(new FiltroTextoRemuneracion());
		}
		return txRemuneracion;
	}

	private JPanel getPnProfesores() {
		if (pnProfesores == null) {
			pnProfesores = new JPanel();
			pnProfesores.setLayout(new BorderLayout(0, 0));
			pnProfesores.add(getLbProfesores(), BorderLayout.NORTH);
			pnProfesores.add(getScrProfesores(), BorderLayout.CENTER);
			pnProfesores.add(getPnRegistroProfesor(), BorderLayout.EAST);
		}
		return pnProfesores;
	}

	private JLabel getLbProfesores() {
		if (lbProfesores == null) {
			lbProfesores = new JLabel("Profesores");
		}
		return lbProfesores;
	}

	private JScrollPane getScrProfesores() {
		if (scrProfesores == null) {
			scrProfesores = new JScrollPane();
			scrProfesores.setViewportView(getTbProfesores());
		}
		return scrProfesores;
	}

	private JPanel getPnTexto() {
		if (pnTexto == null) {
			pnTexto = new JPanel();
			pnTexto.add(getTxRemuneracion());
		}
		return pnTexto;
	}

	private JTable getTbProfesores() {
		if (tbProfesores == null) {
			tbProfesores = new JTable();
		}
		return tbProfesores;
	}

	private JPanel getPnRegistroProfesor() {
		if (pnRegistroProfesor == null) {
			pnRegistroProfesor = new JPanel();
			pnRegistroProfesor.setLayout(new BoxLayout(pnRegistroProfesor, BoxLayout.LINE_AXIS));
			pnRegistroProfesor.add(getBtRegistrar());
		}
		return pnRegistroProfesor;
	}

	private JButton getBtRegistrar() {
		if (btRegistrar == null) {
			btRegistrar = new JButton("Registrar...");
			btRegistrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					nuevoProfesor();
				}
			});
		}
		return btRegistrar;
	}

	private class FiltroTextoRemuneracion extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent ke) {
			Character c = ke.getKeyChar();
			if (!Character.isDigit(c) && c != '.') {
				ke.consume();
			}
		}
	}

	public void aņadirEmpresa(int id_emp) {
		try {
			empresas.add(new DtoFactory().getEmpresaById(id_emp));
			crearModeloEmpresas();
			cbEmpresa.setSelectedIndex(empresas.size() - 2);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error en la base de datos.", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void aņadirProfesor(int id_pro) {
		try {
			profesores.add(new DtoFactory().getProfesorDto(id_pro));
			crearModeloProfesores();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error en la base de datos.", "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private JLabel getLblEmpresa() {
		if (lblEmpresa == null) {
			lblEmpresa = new JLabel("Empresa:");
		}
		return lblEmpresa;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getCbEmpresa());
		}
		return panel;
	}

	public double[] getProfesores() {
		int[] rows = tbProfesores.getSelectedRows();
		double[] ids = new double[rows.length];
		int i = 0;
		
		for(int row : rows) {
			ProfesorDto profesor = profesores.get(row);
			ids[i++] = profesor.id;
		}
		
		return ids;
	}

	public DatosEmpresaDto getEmpresa() {
		DatosEmpresaDto empresa = (DatosEmpresaDto) cbEmpresa.getSelectedItem();
		if(empresa.equals(ninguna) || empresa.equals(otra))
			return null;
		
		return empresa;
	}

	public void actualizar() {
		cargarDatos();
		crearModeloProfesores();
		crearModeloEmpresas();
	}

	public double getRemuneracion() {
		String text = txRemuneracion.getText();
		if(text.isEmpty())
			text="0";
		return Double.parseDouble(text);
	}
}
