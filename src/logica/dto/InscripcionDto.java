package logica.dto;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import logica.EstadoInscripcion;

public class InscripcionDto {

	public ProfesionalDto profesional;
	public ActividadDto actividad;
	public Date fechaInscripcion;
	public double cantidadPagada;
	public EstadoInscripcion estado;
	public double precio;
	public int id;
	public String plaza;
	public double cantidad_a_devolver;

	public double cantidadDevuelta(Date inicioimparticion, String estado) {
		Date hoy = new Date();
		double cantidadDevuelta = 0;
		
 		/*//fecha incio actividad
		LocalDate inicio=LocalDate.of(inicioimparticion.getYear(),inicioimparticion.getMonth()+1,inicioimparticion.getDay()+1);
		LocalDate inscripcion=LocalDate.of(hoy.getYear(),hoy.getMonth()+1,hoy.getDay()+1);
		//long faltan = ChronoUnit.DAYS.between(inscripcion,inicio);*/
		
		long faltan = getDifferenceDays(hoy,inicioimparticion);
		
		if( cantidadPagada  > precio)
		{
			cantidadDevuelta = cantidadPagada - precio;
			
		}
		 
		 
		if(estado.toLowerCase().equals("retrasada") )
		{
			JOptionPane.showMessageDialog(null, "Inscripcion cancelada de una actividad retrasada. Se devolvera 100%. Cantidad de "+ cantidadPagada+ " euros a devolver", "Bien", 1);

			return 1.0 * cantidadPagada;
		}
		
		else if(faltan>=7)
		{
			JOptionPane.showMessageDialog(null, "Inscripcion cancelada. Faltan mas de 7 dias. Se devolvera 100%. Cantidad de "+ cantidadPagada+ " euros a devolver", "Bien", 1);

			return 1.0 * cantidadPagada;
		}
		else if(faltan>=3 && faltan<=6)
		{
			if( cantidadPagada  > precio)
			{
				cantidadDevuelta = cantidadPagada - precio;
				return 0.5* precio + cantidadDevuelta;
			}
			JOptionPane.showMessageDialog(null, 
					"Inscripcion cancelada. Faltan entre 3 y 6 dias."
					+ " Se devolvera 50%. Cantidad de "+ cantidadPagada*0.5+ " euros a devolver", "Bien", 1);

			return 0.5* cantidadPagada;
			 
			
		}
		JOptionPane.showMessageDialog(null, "Inscripcion cancelada."
				+ " Faltan menos de 3 dias. Se devolvera solo la cantidad sobrante."
				+ " Cantidad de "+ Math.abs(cantidadDevuelta)+ " euros a devolver", "Bien", 1);

		return 0+cantidadDevuelta;
		
	}
	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
}
