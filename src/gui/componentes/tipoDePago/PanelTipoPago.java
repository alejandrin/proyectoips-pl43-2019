package gui.componentes.tipoDePago;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class PanelTipoPago extends JPanel {
	
	private JRadioButton rbTransferencia;
	private JRadioButton rbEnCaja;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Create the panel.
	 */
	public PanelTipoPago() {
		setLayout(new GridLayout(2, 1, 0, 0));
		add(getRbTransferencia());
		add(getRbEnCaja());

	}

	private JRadioButton getRbTransferencia() {
		if (rbTransferencia == null) {
			rbTransferencia = new JRadioButton("Transferencia");
			buttonGroup.add(rbTransferencia);
			rbTransferencia.setSelected(true);
		}
		return rbTransferencia;
	}
	private JRadioButton getRbEnCaja() {
		if (rbEnCaja == null) {
			rbEnCaja = new JRadioButton("En caja");
			buttonGroup.add(rbEnCaja);
		}
		return rbEnCaja;
	}
	
	public String getTipoPago() {
		if(rbEnCaja.isSelected())
			return "EN CAJA";
		else
			return "TRANSFERENCIA";
	}
}
