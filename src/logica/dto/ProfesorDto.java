package logica.dto;

public class ProfesorDto {

	public String nombre;
	public int id;
	public String apellidos;
	public String email;
	public String dni;
	public String telefono;
	
}
