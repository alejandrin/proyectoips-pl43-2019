//Este es el bueno
package gui.secretaria;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import connection.ConnectionUtil;
import db.Persistencia;
import gui.componentes.tipoDePago.PanelTipoPago;
import logica.EstadoInscripcion;
import logica.TransferenciaProfesional;
import logica.dto.ActividadDto;
import logica.dto.InscripcionDto;
import logica.dto.InscripcionMultipleDto;
import logica.dto.ProfesionalDto;
import logica.dto.TransferenciaProfesionalDto;
import logica.dto.TransferenciaProfesorDto;
import util.DtoFactory;
import java.awt.GridLayout;
import javax.swing.JLabel;

public class VentanaProfesionales extends JFrame {

	/**
	 * 
	 */
	private TableModel modelo;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tablaProfesionales;
	private JButton btnAdd;
	private List<InscripcionDto> i;
	private JScrollPane scrollPane;
	private JSpinner spinnerPago;
	private List<Integer> listaIds = new ArrayList<>();
	private int id;
	private JScrollPane scrollPaneMovimientos;
	private ActividadDto a;
	private JComboBox<ActividadDto> comboBoxActividades;
	private List<ActividadDto> actividades = new ArrayList<ActividadDto>();
	private DefaultComboBoxModel<ActividadDto> modeloActividades;
	private DefaultTableModel modeloTransferencias;
	private JTable tablaTransferencias;
	private List<Integer> transferencias;

	private JButton btnDevolucion;
	private List<TransferenciaProfesionalDto> transferenciasProfesional;
	private List<TransferenciaProfesorDto> transferenciasProfesor;
	private List<TransferenciaProfesionalDto> transferenciasPrueba;
	private DefaultTableModel modeloTransferenciasProfesor;
	private JTable tableProfesores;
	private PanelTipoPago pnTipoPagoProfesional;
	private JButton btnQuitarMovimiento;
	private JLabel lblDia;
	private JLabel lblMes;
	private JLabel lblYear;
	private JSpinner spinnerDia;
	private JSpinner spinnerMes;
	private JSpinner spinnerYear;
	private JButton btnAtras;
	private JLabel lblInscripciones;
	private JLabel lblMovimientos;

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 * @throws SQLException
	 */
	public VentanaProfesionales(int id, List<ActividadDto> actividades) throws SQLException, IOException {
		this.id = id;
//		private JScrollPane scrollPaneProfesores;
//		private JButton btnPagarAProfesor;
//		private JButton btnPagarTodoA;
//		private JSpinner spinnerProfesor;
//		private JButton btnRecibirFactura;
//		private List<TransferenciaProfesorDto> numeroTransaccionesProfesor;
//		private PanelTipoPago pnTipoPagoProfesor;
//		numeroTransaccionesProfesor = new DtoFactory().getNumeroTransferenciasProfesor(id);
        a= new DtoFactory().getActividadDto(id);
		transferenciasProfesional = new DtoFactory().getTransferenciasClientes(id);
		this.actividades = actividades;
		crearModeloActividades();
		transferenciasPrueba = new DtoFactory().getAllTransferenciasClientes(0);
		transferencias = new ArrayList<Integer>();
		sizeMovimientos();
		transferenciasProfesor = new DtoFactory().getTransferenciasProfesor(id);
		List<InscripcionDto> inscripciones = new DtoFactory().getInscripciones(id);
		for (InscripcionDto ins : inscripciones) {
			listaIds.add(ins.actividad.id);
		}
		i = inscripciones;
		setTitle("Lista de Profesionales");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaActividad.class.getResource("/img/logo-COIIPA.png")));
		Object[] columnNames = { "Nombre Profesional", "Fecha de inscripci�n", "Cantidad Pagada", "Precio Total",
				"Estado", "Cantidad a Devolver" };
		modelo = new DefaultTableModel(new Object[inscripciones.size()][5], columnNames);
		a�adirFilas(inscripciones);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				volver();
			}
		});
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1470, 760);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getBtnAdd());
		contentPane.add(getScrollPane());
		contentPane.add(getSpinnerPago());
		contentPane.add(getScrollPaneMovimientos());
		contentPane.add(getComboBoxActividades());
		contentPane.add(getBtnDevolucion());
		contentPane.add(getPnTipoPagoProfesional());
		contentPane.add(getBtnQuitarMovimiento());
		contentPane.add(getLblDia());
		contentPane.add(getLblMes());
		contentPane.add(getLblYear());
		contentPane.add(getSpinnerDia());
		contentPane.add(getSpinnerMes());
		contentPane.add(getSpinnerYear());
		contentPane.add(getBtnAtras());
		contentPane.add(getLblInscripciones());
		contentPane.add(getLblMovimientos());
		setLocationRelativeTo(null);
	}

	private void crearModeloActividades() {
		ActividadDto[] a = new ActividadDto[actividades.size()];
		modeloActividades = new DefaultComboBoxModel<ActividadDto>(actividades.toArray(a));

		
	}

	private JTable getTablaProfesionales() {
		if (tablaProfesionales == null) {
			tablaProfesionales = new JTable(modelo);
			tablaProfesionales.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return tablaProfesionales;
	}

	private void sizeMovimientos() {
		if(transferenciasPrueba.size()>0)
		{
		int t = transferenciasPrueba.get(transferenciasPrueba.size() - 1).id;
		for (int i = 0; i < t; i++) {
			transferencias.add(i);
		}
		}
	}

	private JButton getBtnAdd() {
		if (btnAdd == null) {
			btnAdd = new JButton("Agregar Pago");
			btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnAdd.setBounds(225, 288, 210, 37);
			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String estado = "recibida";
					String einicial = i.get(getTablaProfesionales().getSelectedRow()).estado.toString().toLowerCase();
				
					if (getTablaProfesionales().getSelectedRow() != -1) {
						InscripcionDto inscripcion = i.get(getTablaProfesionales().getSelectedRow());
						if ((int) getSpinnerPago().getValue() > (inscripcion.precio - inscripcion.cantidadPagada)) {
							inscripcion.estado = EstadoInscripcion.INCIDENCIA;
							estado = "incidencia";
							try {
								cantidadMayorQueDeuda(inscripcion, a);
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (NumberFormatException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} else if ((int) getSpinnerPago().getValue() < (inscripcion.precio
								- inscripcion.cantidadPagada)) {
							try {
								cantidadMenorQueDeuda(inscripcion, a);
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (NumberFormatException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							inscripcion.estado = EstadoInscripcion.INCIDENCIA;
							estado = "incidencia";
						} else {
							try {
								cantidadIgualQueDeuda(i.get(getTablaProfesionales().getSelectedRow()), a);
							} catch (SQLException e1) {
								mostrarError();
								e1.printStackTrace();
							} catch (NumberFormatException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							i.get(getTablaProfesionales().getSelectedRow()).estado = EstadoInscripcion.COBRADA;
							estado = "cobrada";
						}
						if(einicial.equals("cancelada"))
						{
							estado = "cancelada";
							InscripcionDto ins = i.get(getTablaProfesionales().getSelectedRow());
							try {
								int pago = (int) spinnerPago.getValue();
								int rescan = new DtoFactory().cambiarDevolver(ins.id,ins.cantidad_a_devolver+pago);
							} catch (SQLException exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}

						try {
							InscripcionDto insc = i.get(getTablaProfesionales().getSelectedRow());

							new DtoFactory().agregarCantidadPagada(estado, insc.cantidadPagada, insc.profesional.id,
									insc.id);
							/// lista_inscritos cantidad pagada

						} catch (SQLException e1) {
							mostrarError();
							e1.printStackTrace();
						}
						VentanaProfesionales vP;
						try {
							vP = new VentanaProfesionales(id, actividades);
							vP.setVisible(true);
						} catch (SQLException | IOException e1) {
							mostrarError();
							e1.printStackTrace();
						}
						dispose();

					}
				}
			});
		}
		return btnAdd;
	}

	private void volver() {
		this.dispose();
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 131, 1434, 120);
			scrollPane.setViewportView(getTablaProfesionales());
		}
		return scrollPane;
	}

	private void mandarEmail(InscripcionDto i, ActividadDto a) throws SQLException, NumberFormatException, ParseException {
		int total = (int) (a.precio - i.cantidadPagada);
		i.cantidadPagada += ((int) getSpinnerPago().getValue());
		JOptionPane.showMessageDialog(null,
				"Querido Se�or " + i.profesional.nombre + " le faltan por pagar "
						+ (total - (int) getSpinnerPago().getValue()) + "� por favor, pague el resto del importe antes "
						+ "del d�a l�mite.");

		crearTransferenciaPago(transferencia(transferencias.size(), i, i.profesional, a, 0, i.cantidadPagada));

		System.out.print("actividad "+ a.id);
		crearTransferenciaPago(transferencia(transferencias.size(),i, i.profesional, a, 0, i.cantidadPagada));
	}

	private void cantidadMenorQueDeuda(InscripcionDto i, ActividadDto a) throws SQLException, NumberFormatException, ParseException {
		i.cantidadPagada += ((int) getSpinnerPago().getValue());
		JOptionPane.showMessageDialog(null, "Querido Se�or " + i.profesional.nombre + " le faltan por pagar "
				+ (i.precio - i.cantidadPagada) + "� por favor, pague el resto del importe antes " + "del d�a l�mite.");
		crearTransferenciaPago(
				transferencia(transferencias.size(), i, i.profesional, a,(int) getSpinnerPago().getValue(), (int) getSpinnerPago().getValue()));
	}

	private void cantidadIgualQueDeuda(InscripcionDto i, ActividadDto a) throws SQLException, NumberFormatException, ParseException {
		i.cantidadPagada += ((int) getSpinnerPago().getValue());
		JOptionPane.showMessageDialog(null, "Querido Se�or " + i.profesional.nombre + " usted ha pagado "
				+ (int) getSpinnerPago().getValue() + "� completando as� el pago, no hace falta que pague m�s.");
		crearTransferenciaPago(
				transferencia(transferencias.size(), i, i.profesional, a, (int) getSpinnerPago().getValue(), (int) getSpinnerPago().getValue()));
	}

	private void cantidadMayorQueDeuda(InscripcionDto i, ActividadDto a) throws SQLException, NumberFormatException, ParseException {
		int total = (int) ((int) getSpinnerPago().getValue() - (i.precio - i.cantidadPagada));
		i.cantidadPagada+= (int) getSpinnerPago().getValue(); 
		JOptionPane.showMessageDialog(null, "Querido Se�or " + i.profesional.nombre + " ha sobrepasado el pago por "
				+ (total) + "� se le devolvera el importe en cuanto sea posible ");
		crearTransferenciaPago(
				transferencia(transferencias.size(), i, i.profesional, a, (int) getSpinnerPago().getValue(), (int) getSpinnerPago().getValue()));
	}

	private JSpinner getSpinnerPago() {
		if (spinnerPago == null) {
			spinnerPago = new JSpinner();
			spinnerPago.setFont(new Font("Tahoma", Font.PLAIN, 20));
			spinnerPago.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spinnerPago.setBounds(489, 316, 97, 37);
		}
		return spinnerPago;
	}

	private TransferenciaProfesionalDto transferencia(int id, InscripcionDto i, ProfesionalDto p, ActividadDto a,
			double cantidad1, double cantidad2) {
		TransferenciaProfesionalDto t = new TransferenciaProfesionalDto();
		t.id = id;

		t.inscripcion = i;
		t.profesional = p; 

		t.inscripcion  = i ;
		t.profesional =  p;  

		t.actividad = a;
		t.cantidad_pagada_por_COIIPA = cantidad1;
		t.cantidad_pagada_por_profesional = cantidad2;
		t.tipoPago = pnTipoPagoProfesional.getTipoPago();
		return t;

	}


	private void crearTransferenciaPago(TransferenciaProfesionalDto t) throws SQLException, NumberFormatException, ParseException {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		Date d = objSDF.parse(Integer.parseInt(spinnerYear.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerMes.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerDia.getValue().toString()));
		TransferenciaProfesional trans = new TransferenciaProfesional(t.id+1, t.profesional.id, t.inscripcion.id,
				t.actividad.id, t.cantidad_pagada_por_COIIPA, t.cantidad_pagada_por_profesional, d, t.tipoPago);
		try {
		Persistencia.agregarTransferenciaProfesional(trans);
		}
		catch(Exception e)
		{
			;
		}
	
	}
	private void crearTransferenciaDevolucion(TransferenciaProfesionalDto t) throws SQLException, NumberFormatException, ParseException {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		Date d = objSDF.parse(Integer.parseInt(spinnerYear.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerMes.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerDia.getValue().toString()));
		TransferenciaProfesional trans = new TransferenciaProfesional(t.id+1, t.profesional.id, t.inscripcion.id,
				t.actividad.id, -t.cantidad_pagada_por_COIIPA, t.cantidad_pagada_por_profesional, d, t.tipoPago);
		try {
				Persistencia.agregarTransferenciaProfesional(trans);
		}
		catch(Exception e)
		{
			;
		}
	} 

	private JScrollPane getScrollPaneMovimientos() {
		if (scrollPaneMovimientos == null) {
			scrollPaneMovimientos = new JScrollPane();
			scrollPaneMovimientos.setBounds(10, 501, 1434, 120);
			scrollPaneMovimientos.setViewportView(getTablaTransferencias());
		}
		return scrollPaneMovimientos;
	}

	private JComboBox<ActividadDto> getComboBoxActividades() {
		if (comboBoxActividades == null) {
			comboBoxActividades = new JComboBox<ActividadDto>();
			comboBoxActividades.setFont(new Font("Tahoma", Font.PLAIN, 20));
			comboBoxActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					a = (ActividadDto) modeloActividades.getSelectedItem();
					mostrarDatosActividad(a);

				}
			});
			comboBoxActividades.setModel(modeloActividades);
			comboBoxActividades.setBounds(90, 31, 973, 31);
		}
		return comboBoxActividades;
	}

	protected void mostrarDatosActividad(ActividadDto a) {
		// TODO Auto-generated method stub
		try {
			VentanaProfesionales vP = new VentanaProfesionales(a.id, actividades);
			vP.setVisible(true);
			dispose();
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@SuppressWarnings("serial")
	private void mostrarDatosTransferencias() {
		try {
			Object[] columnNames = new Object[] { "Nombre", "cantidad pagada", "estado", "fecha", "Tipo de pago" };
			modeloTransferencias = new DefaultTableModel(new Object[transferenciasProfesional.size()][5], columnNames) {
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			a�adirFilasTransferencia(transferenciasProfesional);
			tablaTransferencias.setModel(modeloTransferencias);
		} catch (Exception e) {
			e.printStackTrace();
			mostrarError();
		}
	}
	
	private void a�adirFilasTransferencia(List<TransferenciaProfesionalDto> transferencias) {
		for (int i = 0; i < transferencias.size(); i++) {
			TransferenciaProfesionalDto trans = transferencias.get(i);;
			modeloTransferencias.setValueAt(trans.inscripcion.profesional.nombre + " " + trans.inscripcion.profesional.apellidos, i, 0);
			//// bug!!
			if (trans.cantidad_pagada_por_COIIPA < 0) {
				modeloTransferencias.setValueAt(-trans.cantidad_pagada_por_COIIPA, i, 1);
				modeloTransferencias.setValueAt("DEVOLUCI�N", i, 2);
			} else {
				modeloTransferencias.setValueAt(trans.cantidad_pagada_por_COIIPA, i, 1);
				modeloTransferencias.setValueAt("PAGO", i, 2);
			}
			modeloTransferencias.setValueAt(trans.fecha_transferencia, i, 3);
			modeloTransferencias.setValueAt(trans.tipoPago, i, 4);
		}
	}

	private void a�adirFilas(List<InscripcionDto> inscripciones) {
		for (int i = 0; i < inscripciones.size(); i++) {
			try {
				new DtoFactory().getActividadDto(listaIds.get(i));
				InscripcionDto ins = inscripciones.get(i);
				modelo.setValueAt(ins.profesional.nombre, i, 0);
				modelo.setValueAt(ins.fechaInscripcion.toString(), i, 1);
				modelo.setValueAt(ins.cantidadPagada, i, 2);
				modelo.setValueAt(ins.precio, i, 3);
				modelo.setValueAt(ins.estado.toString(), i, 4);
				modelo.setValueAt(ins.cantidad_a_devolver, i, 5);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}


	private void mostrarError() {
		JOptionPane.showMessageDialog(this, "Se ha producido un error", "Error", JOptionPane.ERROR_MESSAGE);
	}

	private JTable getTablaTransferencias() {
		if (tablaTransferencias == null) {
			tablaTransferencias = new JTable();
			mostrarDatosTransferencias();
		}
		return tablaTransferencias;
	}
	// Dos opciones, que el dinero sea suficiente y se ponga en recibido, que no sea
	// suficiente y se quede en

	private JButton getBtnDevolucion() {
		if (btnDevolucion == null) {
			btnDevolucion = new JButton("A\u00F1adir Devoluci\u00F3n");
			btnDevolucion.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnDevolucion.addActionListener(new ActionListener() {
				@SuppressWarnings("unlikely-arg-type")
				public void actionPerformed(ActionEvent arg0) {
					if (getTablaProfesionales().getSelectedRow() != -1) {
						String estado="incidencia";
						if(i.get(getTablaProfesionales().getSelectedRow()).estado.toString().toLowerCase().equals("cancelada"))
						{
							estado = "cancelada";
							InscripcionDto ins = i.get(getTablaProfesionales().getSelectedRow());
							try {
								int pago = (int) spinnerPago.getValue();
								int rescan = new DtoFactory().cambiarDevolver(ins.id,ins.cantidad_a_devolver-pago);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
						if ((i.get(getTablaProfesionales().getSelectedRow()).cantidadPagada)-i.get(getTablaProfesionales().getSelectedRow()).precio == (int) getSpinnerPago()
										.getValue()) {
							try {
								devolverDineroEntero(i.get(getTablaProfesionales().getSelectedRow()), a);
								estado = "cobrada";
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if((i.get(getTablaProfesionales().getSelectedRow()).cantidadPagada) >= (int) getSpinnerPago()
								.getValue()){
							try {
								devolverDineroNoEntero(i.get(getTablaProfesionales().getSelectedRow()), a);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else
						{
							JOptionPane.showMessageDialog(null, "No se puede devolver un importe mayor al pagado");
						}
						try {
							InscripcionDto insc = i.get(getTablaProfesionales().getSelectedRow());

							new DtoFactory().agregarCantidadPagada(estado, insc.cantidadPagada, insc.profesional.id,
									insc.id);
							/// lista_inscritos cantidad pagada

						} catch (SQLException e1) {
							mostrarError();
							e1.printStackTrace();
						}
						VentanaProfesionales vP;
						try {
							vP = new VentanaProfesionales(id, actividades);
							vP.setVisible(true);
						} catch (SQLException | IOException e1) {
							mostrarError();
							e1.printStackTrace();
						}
						dispose();
						
					} 
					
				}
			});
			btnDevolucion.setBounds(225, 332, 210, 43);
		}
		return btnDevolucion;
	}

	@SuppressWarnings("static-access")
	private void devolverDineroEntero(InscripcionDto i, ActividadDto a) throws SQLException, NumberFormatException, ParseException {
		i.cantidadPagada -= (int) spinnerPago.getValue();
		i.estado = i.estado.RECIBIDA;
		crearTransferenciaDevolucion(
				transferencia(transferencias.size(), i, i.profesional, a, (int) spinnerPago.getValue(), 0));
	}

	private void devolverDineroNoEntero(InscripcionDto i, ActividadDto a) throws SQLException, NumberFormatException, ParseException {
		i.cantidadPagada -= (int) spinnerPago.getValue();
		crearTransferenciaDevolucion(
				transferencia(transferencias.size(), i, i.profesional, a, (int) spinnerPago.getValue(), 0));
	}

//	contentPane.add(getScrollPaneProfesores());	

//	private JScrollPane getScrollPaneProfesores() {
//		if (scrollPaneProfesores == null) {
//			scrollPaneProfesores = new JScrollPane();
//			scrollPaneProfesores.setBounds(10, 626, 1434, 127);
//			scrollPaneProfesores.setViewportView(getTableProfesores());
//		}
//		return scrollPaneProfesores;
//	}
//	private JTable getTableProfesores() {
//		if (tableProfesores == null) {
//			tableProfesores = new JTable();
//			mostrarDatosTransferenciasProfesor();
//		}
//		return tableProfesores;
//	}

	private void a�adirFilasProfesor(List<TransferenciaProfesorDto> transferencias) {
		for (int i = 0; i < transferencias.size(); i++) {
			TransferenciaProfesorDto trans = transferencias.get(i);
			modeloTransferenciasProfesor.setValueAt(trans.id, i, 0);
			modeloTransferenciasProfesor.setValueAt(trans.actividad.id, i, 1);
			modeloTransferenciasProfesor.setValueAt(trans.profesor.id, i, 2);
			//// bug!!
			if (trans.cantidad_pagada_por_COIIPA > 0
					&& trans.cantidad_debida_profesor > trans.cantidad_pagada_por_COIIPA) {
				modeloTransferenciasProfesor.setValueAt(trans.cantidad_pagada_por_COIIPA, i, 3);
				modeloTransferenciasProfesor.setValueAt(trans.cantidad_debida_profesor, i, 4);
				modeloTransferenciasProfesor.setValueAt("Incidencia", i, 5);
			} else if (trans.cantidad_debida_profesor == trans.cantidad_pagada_por_COIIPA) {
				modeloTransferenciasProfesor.setValueAt(trans.cantidad_pagada_por_COIIPA, i, 3);
				modeloTransferenciasProfesor.setValueAt(trans.cantidad_debida_profesor, i, 4);
				modeloTransferenciasProfesor.setValueAt("Pagado", i, 5);
			} else {
				modeloTransferenciasProfesor.setValueAt(0, i, 3);
				modeloTransferenciasProfesor.setValueAt(trans.cantidad_debida_profesor, i, 4);
				modeloTransferenciasProfesor.setValueAt("Pago a Profesor", i, 5);
			}
			modeloTransferenciasProfesor.setValueAt(trans.fecha_transferencia, i, 6);
			modeloTransferenciasProfesor.setValueAt(trans.tipoPago, i, 7);
		}
	}

//	private JButton getBtnPagarAProfesor() {
//		if (btnPagarAProfesor == null) {
//			btnPagarAProfesor = new JButton("Pagar a Profesor");
//			btnPagarAProfesor.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent arg0) {
//					if(tableProfesores.getSelectedRow()!=-1)
//					{
//					if(!tableProfesores.getValueAt(tableProfesores.getSelectedRow(),5).equals("Pagado"))
//					{
//						if((double)spinnerProfesor.getValue()>transferenciasProfesor.get(tableProfesores.getSelectedRow()).cantidad_debida_profesor)
//						{
//							try {
//								pagarAProfesor(transferenciasProfesor.get(tableProfesores.getSelectedRow()).cantidad_debida_profesor);
//							} catch (SQLException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						}
//						else
//						{
//							try {
//								pagarAProfesor((double)spinnerProfesor.getValue());
//							} catch (SQLException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						}
//					}
//					else
//					{
//						JOptionPane.showMessageDialog(null, "Ya se le ha pagado el dinero al profesor");
//					}
//					 VentanaProfesionales vP;
//						try {
//							vP = new VentanaProfesionales(id, actividades);
//							vP.setVisible(true);
//						} catch (SQLException | IOException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
//						 dispose();
//				}
//				}
//			});
//			btnPagarAProfesor.setBounds(147, 764, 161, 23);
//		}
//		return btnPagarAProfesor;
//	}

	private void pagarAProfesor(double cantidad_pagada_por_profesional) throws SQLException {
		double cantidad = transferenciasProfesor.get(tableProfesores.getSelectedRow()).cantidad_debida_profesor;
		if (cantidad == cantidad_pagada_por_profesional) {
			cantidad = 0;
			String query = "UPDATE transferencias_profesor SET "
					+ " cantidad_pagada_por_profesor=? WHERE id_actividad=? AND " + " id_profesor=?";
			EmitirFactura(a);
			ConnectionUtil.executeUpdate(query, new String[] { cantidad + "", a.id + "", a.profesor.id + "" });
		} else {
			cantidad = transferenciasProfesor.get(tableProfesores.getSelectedRow()).cantidad_pagada_por_COIIPA
					+ cantidad_pagada_por_profesional;
			String query = "UPDATE transferencias_profesor SET "
					+ " cantidad_pagada_por_COIIPA=? WHERE id_actividad=? AND " + " id_profesor=?";
			ConnectionUtil.executeUpdate(query, new String[] { cantidad + "", a.id + "", a.profesor.id + "" });
		}

	}

//	contentPane.add(getBtnPagarAProfesor());
//	contentPane.add(getBtnPagarTodoA());
//	contentPane.add(getSpinnerProfesor());
//	contentPane.add(getBtnRecibirFactura());
//	contentPane.add(getPnTipoPagoProfesor());

//	private JButton getBtnPagarTodoA() {
//		if (btnPagarTodoA == null) {
//			btnPagarTodoA = new JButton("Pagar todo a Profesor");
//			btnPagarTodoA.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					if(tableProfesores.getSelectedRow()!=-1)
//					{
//						if(!tableProfesores.getValueAt(tableProfesores.getSelectedRow(),5).equals("Pagado"))
//						{
//						try {
//							pagarAProfesor(transferenciasProfesor.get(tableProfesores.getSelectedRow()).cantidad_debida_profesor);
//							 VentanaProfesionales vP;
//								try {
//									vP = new VentanaProfesionales(id, actividades);
//									vP.setVisible(true);
//								} catch (SQLException | IOException e1) {
//									// TODO Auto-generated catch block
//									e1.printStackTrace();
//								}
//								 dispose();
//						} catch (SQLException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
//						}
//						else
//						{
//							JOptionPane.showMessageDialog(null, "Ya se le ha pagado el dinero al profesor");
//						}
//					}
//				}
//			});
//			btnPagarTodoA.setBounds(147, 798, 161, 23);
//		}
//		return btnPagarTodoA;
//	}

//	private JSpinner getSpinnerProfesor() {
//		if (spinnerProfesor == null) {
//			spinnerProfesor = new JSpinner();
//			spinnerProfesor.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
//			spinnerProfesor.setBounds(342, 781, 97, 20);
//		}
//		return spinnerProfesor;
//	}

	private void EmitirFactura(ActividadDto a) {
		guardarFactura(a, a.profesor.dni, a.finActividad);
	}

	public int guardarFactura(ActividadDto a, String nif, java.sql.Timestamp finActividad) {
		String line = hacerFactura(a.profesor.nombre, a.profesor.apellidos, a.profesor.dni, finActividad, a.precio,
				a.nombre);
		String fileName = "files/" + nif + ".dat";
		try {
			BufferedWriter fichero = new BufferedWriter(new FileWriter(fileName));
			fichero.write(line);
			fichero.close();
			return (0);
		} catch (FileNotFoundException fnfe) {
			System.out.println("The file could not be saved");
			return (-1);
		} catch (IOException ioe) {
			new RuntimeException("I/O Error.");
			return (-2);
		}
	}

	public String hacerFactura(String nombre, String apellidos, String nif, java.sql.Timestamp finActividad,
			double precio, String nombreActividad) {
		String linea = "FACTURA Profesor";
		linea += "\n";
		linea += "\n";
		linea += "----------------------";
		linea += "\n";
		linea += "\n";
		linea += nombre + " " + apellidos + " " + "ha impartido la actividad: ";
		linea += "\n";
		linea += nombreActividad;
		linea += "\n";
		linea += "----------------------";
		linea += "\n";
		linea += "Se le ha pagado: " + precio;
		linea += "\n";
		linea += "La actividad ha finalizado el: " + finActividad;
		linea += "\n";
		linea += "\n";
		linea += "----------------------";
		return linea;
	}

	public ActividadDto buscarActividad(String nombre, List<ActividadDto> listactividades) {
		for (ActividadDto a : listactividades) {
			if (a.nombre.equals(nombre)) {
				return a;
			}
		}
		return null;
	}
//	private JButton getBtnRecibirFactura() {
//		if (btnRecibirFactura == null) {
//			btnRecibirFactura = new JButton("Recibir factura");
//			btnRecibirFactura.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent arg0) {
//					Date date = new Date();
//					if(transferenciasProfesor.size()<=0)
//					{
//						String query = "INSERT INTO TRANSFERENCIAS_PROFESOR VALUES(?,?,?,?,?,?)";
//						try {
//							Calendar calendar = new GregorianCalendar();
//							calendar.setTime(date);
//							calendar.get(Calendar.YEAR);
//							calendar.get(Calendar.MONTH);
//							calendar.get(Calendar.DAY_OF_MONTH);
//							ConnectionUtil.executeUpdate(query, new String[] {numeroTransaccionesProfesor.size()+"",+a.id+"",+a.profesor.id+"",0+"", a.precio+"",null});
////							"TO_DATE('" +year + "/" 
////							+ month + "/"
////							+ day+ " " +
////							date.getHours() + ":" 
////							+ date.getMinutes()
////							+ ":00','YYYY/MM/DD HH:MI:SS'"
//						} catch (SQLException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						 VentanaProfesionales vP;
//							try {
//								vP = new VentanaProfesionales(id, actividades);
//								vP.setVisible(true);
//							} catch (SQLException | IOException e1) {
//								// TODO Auto-generated catch block
//								e1.printStackTrace();
//							}
//							 dispose();
//					}
//					else
//					{
//						JOptionPane.showMessageDialog(null, "Ya tenemos la factura esperando");
//					}
//				}
//			});
//			btnRecibirFactura.setBounds(729, 780, 130, 23);
//		}
//		return btnRecibirFactura;
//	}

	private List<InscripcionMultipleDto> mostrarMultipleIns(int id) {
		List<InscripcionMultipleDto> ins = new ArrayList<InscripcionMultipleDto>();
		try {
			ins = new DtoFactory().getInsMul(id);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ins;
	}

	private PanelTipoPago getPnTipoPagoProfesional() {
		if (pnTipoPagoProfesional == null) {
			pnTipoPagoProfesional = new PanelTipoPago();
			pnTipoPagoProfesional.setBounds(730, 288, 230, 87);
			pnTipoPagoProfesional.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnTipoPagoProfesional;
	}

	private JButton getBtnQuitarMovimiento() {
		if (btnQuitarMovimiento == null) {
			btnQuitarMovimiento = new JButton("Quitar movimiento");
			btnQuitarMovimiento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (tablaTransferencias.getSelectedRow() != -1) {
						try {
							TransferenciaProfesionalDto t = transferenciasProfesional.get(tablaTransferencias.getSelectedRow());
							
								new DtoFactory().agregarCantidadPagada("incidencia", t.inscripcion.cantidadPagada - t.cantidad_pagada_por_COIIPA, t.inscripcion.profesional.id,
										t.inscripcion.id);
							
							new DtoFactory().cancelarMovimiento(
									transferenciasProfesional.get(tablaTransferencias.getSelectedRow()).id);
						} catch (SQLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						VentanaProfesionales vP;
						try {
							vP = new VentanaProfesionales(id, actividades);
							vP.setVisible(true);
						} catch (SQLException | IOException e1) {
							mostrarError();
							e1.printStackTrace();
						}
						dispose();

					}
				}
			});
			btnQuitarMovimiento.setBounds(817, 671, 143, 23);
		}
		return btnQuitarMovimiento;
	}

	private JLabel getLblDia() {
		if (lblDia == null) {
			lblDia = new JLabel("D\u00EDa:");
			lblDia.setBounds(494, 440, 46, 23);
		}
		return lblDia;
	}

	private JLabel getLblMes() {
		if (lblMes == null) {
			lblMes = new JLabel("Mes:");
			lblMes.setBounds(613, 444, 46, 14);
		}
		return lblMes;
	}

	private JLabel getLblYear() {
		if (lblYear == null) {
			lblYear = new JLabel("A\u00F1o:");
			lblYear.setBounds(730, 444, 46, 14);
		}
		return lblYear;
	}

	private JSpinner getSpinnerDia() {
		if (spinnerDia == null) {
			spinnerDia = new JSpinner();
			spinnerDia.setModel(new SpinnerNumberModel(1, 1, 31, 1));
			spinnerDia.setBounds(527, 441, 59, 20);
		}
		return spinnerDia;
	}

	private JSpinner getSpinnerMes() {
		if (spinnerMes == null) {
			spinnerMes = new JSpinner();
			spinnerMes.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			spinnerMes.setBounds(643, 441, 77, 20);
		}
		return spinnerMes;
	}

	private JSpinner getSpinnerYear() {
		if (spinnerYear == null) {
			spinnerYear = new JSpinner();
			spinnerYear.setModel(new SpinnerNumberModel(2010, 2010, 2099, 1));
			spinnerYear.setBounds(769, 441, 191, 20);
		}
		return spinnerYear;
	}

	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnAtras.setBounds(15, 668, 115, 29);
		}
		return btnAtras;
	}
	private JLabel getLblInscripciones() {
		if (lblInscripciones == null) {
			lblInscripciones = new JLabel("Inscripciones:");
			lblInscripciones.setBounds(10, 99, 90, 31);
		}
		return lblInscripciones;
	}
	private JLabel getLblMovimientos() {
		if (lblMovimientos == null) {
			lblMovimientos = new JLabel("Movimientos:");
			lblMovimientos.setBounds(10, 473, 90, 31);
		}
		return lblMovimientos;
	}
}