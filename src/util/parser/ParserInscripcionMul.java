package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.ActividadDto;
import logica.dto.InscripcionMultipleDto;
import util.DtoFactory;

public class ParserInscripcionMul {

	public List<InscripcionMultipleDto> parseInsMul(ResultSet rs) throws SQLException{
		List<InscripcionMultipleDto> inscripciones = new ArrayList<InscripcionMultipleDto>();
		
		while(rs.next()) {
			inscripciones.add(parseIns(rs));
		}
		
		return inscripciones;
	}
	
	public InscripcionMultipleDto parseIns(ResultSet rs) throws SQLException {
		InscripcionMultipleDto ins = new InscripcionMultipleDto();
 		ActividadDto act = new ActividadDto();
 		 
		ins.numins = rs.getInt("numins");
		
		act.id = rs.getInt("id_actividad");
		act.nombre = rs.getString("nombre");
 		
		ins.id = rs.getInt("id_inscripcion"); 
		ins.actividad = act; 
  		ins.preciototal = rs.getDouble("total");
  		ins.plaza = (String) new DtoFactory().getPlaza(ins.id);
  		ins.responsable = (String) new DtoFactory().getResponsable(ins.id);
  		ins.cantidad_pagada = new DtoFactory().getCantidadPagadaInsMul(ins.id);
  		int res  = new DtoFactory().setCantidadPagada(ins);
  		ins.cantidad_a_devolver = rs.getDouble("cdev");
 		return ins;
	}
	
}