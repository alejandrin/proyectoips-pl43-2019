package logica.dto;

public class ColectivoDto {

	public static final int ID_GENERAL = 1;
	public static final int SIN_PRECIO_GENERAL = -1;			
	
	public String nombre;
	public int id;
	
	public ColectivoDto(int i, String string) {
		nombre = string;
		id = i;
	}

	public String toString() {
		return nombre;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		if(!(otherObject instanceof ColectivoDto))
			return false;
		ColectivoDto otroCol = (ColectivoDto) otherObject;
		return otroCol.id == this.id;
	}
}
