package gui.secretaria;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import db.Persistencia;
import logica.EstadoActividad;
import logica.dto.ActividadDto;
import logica.dto.IncidenciaDto;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Timestamp;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("unused")
public class VentanaCerrarActividad extends JFrame {

	private static final long serialVersionUID = 1L;
	//Modelo para la tabla
	private ModeloNoEditable modeloTabla;
	private ModeloNoEditable modeloTablaIncidencias;
	public class ModeloNoEditable extends DefaultTableModel {
		private static final long serialVersionUID = 1L;

		public ModeloNoEditable(Object[] columnNames, int rowCount) {
			super(columnNames, rowCount); 
		}
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	}
	private List<ActividadDto> listactividades;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTable table;
	private JLabel lblCerrarActividades;
	private JLabel lblListaDeActividades;
	private JButton btnCerrarActividadSeleccionada;
	private JScrollPane scrollPane_1;
	private JLabel lblListaDeIncidencias;
	private JTable table_1;


	/**
	 * Create the frame.
	 */
	public VentanaCerrarActividad() {
		setResizable(false);
		setTitle("Cerrar Actividad");
		setBounds(100, 100, 1350, 746);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScrollPane());
		contentPane.add(getLblCerrarActividades());
		contentPane.add(getLblListaDeActividades());
		contentPane.add(getBtnCerrarActividadSeleccionada());
		contentPane.add(getScrollPane_1());
		contentPane.add(getLblListaDeIncidencias());
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 61, 1324, 296);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
	private JTable getTable() {
		if (table == null) {

			//Antes de crear la tabla, preparamos el modelo
			String[]nombreColumnas= {"Nombre","Abre inscripci�n","Cierra inscripci�n","Inicio impartici�n","Fin impartici�n","Estado","n� plazas","n� inscripciones"};
			modeloTabla=new ModeloNoEditable(nombreColumnas,0);

			table = new JTable(modeloTabla);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			try {
				a�adirFilas();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return table;
	}
	private void a�adirFilas() throws SQLException {		
		Object[]nuevaFila=new Object[8];//La fila tendr� tantos objetos como columnas

		ArrayList<ActividadDto>actividades=Persistencia.getActividadesParaCerrar();
		listactividades = actividades;

		for(int i=0;i<actividades.size();i++) {
			nuevaFila[0]=actividades.get(i).nombre;

			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
			nuevaFila[1]=dt1.format(actividades.get(i).inicioInscripcion);
			nuevaFila[2]=dt1.format(actividades.get(i).finInscripcion);
			nuevaFila[3]=dt1.format(actividades.get(i).inicioActividad);
			nuevaFila[4]=dt1.format(actividades.get(i).finActividad);
			nuevaFila[5]=actividades.get(i).estado;
			nuevaFila[6]=actividades.get(i).numPlazas;
			nuevaFila[7]=Persistencia.getNumInscripciones(actividades.get(i).id);

			if( ! (actividades.get(i).estado==EstadoActividad.CANCELADA || actividades.get(i).estado==EstadoActividad.CERRADA))
				modeloTabla.addRow(nuevaFila);
		}

	}
	private void a�adirFilasIncidencias() throws SQLException {

		Object[]nuevaFila=new Object[5];//La fila tendr� tantos objetos como columnas

		List<IncidenciaDto> listaDto=Persistencia.getIncidencias();
		for(int i=0;i<listaDto.size();i++) {
			nuevaFila[0]=listaDto.get(i).nombreActividad;
			nuevaFila[1]=listaDto.get(i).precioActividad;
			nuevaFila[2]=listaDto.get(i).cantidadPagada;
			nuevaFila[3]=listaDto.get(i).nombreProfesional;
			nuevaFila[4]=listaDto.get(i).apellidoProfesional;
			modeloTablaIncidencias.addRow(nuevaFila);

		}
	}

	private JLabel getLblCerrarActividades() {
		if (lblCerrarActividades == null) {
			lblCerrarActividades = new JLabel("Cerrar actividades:");
			lblCerrarActividades.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblCerrarActividades.setBounds(10, 11, 383, 14);
		}
		return lblCerrarActividades;
	}
	private JLabel getLblListaDeActividades() {
		if (lblListaDeActividades == null) {
			lblListaDeActividades = new JLabel("Lista de actividades: (s\u00F3lo se muestran las que se pueden cerrar, no salen las canceladas ni las cerradas)");
			lblListaDeActividades.setBounds(10, 36, 1274, 14);
		}
		return lblListaDeActividades;
	}
	private JButton getBtnCerrarActividadSeleccionada() {
		if (btnCerrarActividadSeleccionada == null) {
			btnCerrarActividadSeleccionada = new JButton("Cerrar actividad seleccionada");
			btnCerrarActividadSeleccionada.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					///////////////////////// Realizar comprobaciones /////////////////////////

					//Todav�a no ha finalizado la actividad?
					//Fecha actual
					if(table.getSelectedRow()==-1)
						JOptionPane.showMessageDialog(null, "Seleccione una actividad.");
					else {

						boolean noHaTerminado=false;
						boolean tieneIncidencias=false;

						java.sql.Timestamp fechaActual = new java.sql.Timestamp(System.currentTimeMillis());
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date fechaFin=null;
						try {
							fechaFin=sdf.parse((String) table.getValueAt(table.getSelectedRow(), 4));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						// Ha terminado?
						if(fechaFin.after(fechaActual))
							noHaTerminado=true;

						//Tiene incidencias?
						try {
							tieneIncidencias=Persistencia.tieneIncidencias((String)table.getValueAt(table.getSelectedRow(), 0));
						} catch (SQLException e) {
							e.printStackTrace();
						}

						if(noHaTerminado || tieneIncidencias) {
							String aviso="�Seguro que desea cerrar la actividad?\n";
							if(noHaTerminado)
								aviso+="\t-La actividad no ha terminado\n";
							if(tieneIncidencias)
								aviso+="\t-La actividad tiene incidencias\n";
							int resultado=JOptionPane.showConfirmDialog (null, aviso, "�Cuidado!", 0, 2);
							if (resultado == JOptionPane.YES_OPTION) {
								cerrarActividad();
								inicializar();
							}
						}
						else {
							int resultado=JOptionPane.showConfirmDialog (null, "�Seguro que desea cerrar la actividad?", "�Cuidado!", 0, 2);
							if (resultado == JOptionPane.YES_OPTION) {
								cerrarActividad();
								inicializar();
							}
						}
					}
				}
				private void cerrarActividad() {
					try {
						Persistencia.cerrarActividad((String)table.getValueAt(table.getSelectedRow(), 0));
						//JOptionPane.showMessageDialog(null, "La actividad se ha cerrado correctamente");

						// Parte Guillermo
						//EmitirFactura(buscarActividad((String)table.getValueAt(table.getSelectedRow(),0),listactividades));
						JOptionPane.showMessageDialog(null, "La actividad se ha cerrado correctamente");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
			btnCerrarActividadSeleccionada.setBounds(1119, 376, 215, 23);
		}
		return btnCerrarActividadSeleccionada;
	}
	private void inicializar() {
		this.dispose();
		VentanaSecretaria.mostrarVentanaCerrarActividad();
	}


	// Parte Guillermo Historia 6

	private void EmitirFactura(ActividadDto a)
	{
		guardarFactura(a,a.profesor.dni, a.finActividad);
	}
	public int guardarFactura(ActividadDto a,String nif, java.sql.Timestamp finActividad)
	{
		String line = hacerFactura(a.profesor.nombre, a.profesor.apellidos, a.profesor.dni, finActividad, a.precio,a.nombre);
		String fileName = "files/"+ nif +".dat";
		try {
			BufferedWriter fichero = new BufferedWriter(new FileWriter(fileName));
			fichero.write(line);
			fichero.close();
			return(0);
		}
		catch (FileNotFoundException fnfe) {
			System.out.println("The file could not be saved");
			return(-1);
		}
		catch (IOException ioe) {
			new RuntimeException("I/O Error.");
			return(-2);
		}
	}
	public String hacerFactura(String nombre, String apellidos, String nif, java.sql.Timestamp finActividad, double precio, String nombreActividad)
	{	
		String linea = "FACTURA Profesor";
		linea+= "\n";
		linea+= "\n";
		linea+= "----------------------";
		linea+= "\n";
		linea+= "\n";
		linea+= nombre + " " + apellidos + " " + "ha impartido la actividad: ";
		linea+= "\n";
		linea+= nombreActividad;
		linea+= "\n";
		linea+= "----------------------";
		linea+= "\n";
		linea+= "Se le ha pagado: " + precio;
		linea+= "\n";
		linea+= "La actividad ha finalizado el: " + finActividad;
		linea+= "\n";
		linea+= "\n";
		linea+= "----------------------";
		return linea;
	}
	public ActividadDto buscarActividad(String nombre,List<ActividadDto> listactividades)
	{
		for(ActividadDto a:listactividades)
		{
			if(a.nombre.equals(nombre))
			{
				return a;
			}
		}
		return null;
	}
	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(10, 410, 1324, 296);
			scrollPane_1.setViewportView(getTable_1());
		}
		return scrollPane_1;
	}
	private JLabel getLblListaDeIncidencias() {
		if (lblListaDeIncidencias == null) {
			lblListaDeIncidencias = new JLabel("Lista de incidencias con los profesionales: (si un profesional hace un pago incorrecto aparece aqu\u00ED)");
			lblListaDeIncidencias.setBounds(10, 385, 697, 14);
		}
		return lblListaDeIncidencias;
	}
	private JTable getTable_1() {
		if (table_1 == null) {

			//Antes de crear la tabla, preparamos el modelo
			String[]nombreColumnas= {"Nombre actividad","Precio actividad","Cantidad pagada","Nombre profesional","Apellidos profesional"};
			modeloTablaIncidencias=new ModeloNoEditable(nombreColumnas,0);

			table_1 = new JTable(modeloTablaIncidencias);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			try {
				a�adirFilasIncidencias();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return table_1;
	}
}
