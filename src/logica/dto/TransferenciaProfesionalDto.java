package logica.dto;

import java.util.Date;

public class TransferenciaProfesionalDto {
	public int id;
	public InscripcionDto inscripcion;
	public ProfesionalDto profesional;
	public ActividadDto actividad;
	public double cantidad_pagada_por_COIIPA;
	public double cantidad_pagada_por_profesional;
	public Date fecha_transferencia;
	public String fact;
	public String tipoPago;
	public double cantidad_pagada;
	 
	public String movimiento;
}
