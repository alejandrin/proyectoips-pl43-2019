package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import logica.dto.ColectivoDto;

public class ParserColectivos {

	public List<ColectivoDto> parseColectivos(ResultSet rs) throws SQLException{
		List<ColectivoDto> cols = new ArrayList<ColectivoDto>();
		
		while(rs.next())
			cols.add(parseColectivo(rs));
		
		return cols;
	}
	
	public ColectivoDto parseColectivo(ResultSet rs) throws SQLException {
		ColectivoDto col = null;
		
		if(!rs.isBeforeFirst() || rs.next()) {
			col = new ColectivoDto(rs.getInt("id_colectivo"), rs.getString("nombre"));
		}
		
		return col;
	}

	public Map<ColectivoDto, Double> parsePrecios(ResultSet rs) throws SQLException {
		Map<ColectivoDto, Double> res = new HashMap<ColectivoDto, Double>();
		
		while(rs.next()) {
			ColectivoDto col = parseColectivo(rs);
			double precio = rs.getDouble("precio");
			
			res.put(col, precio);
		}
		
		return res;
	}
}
