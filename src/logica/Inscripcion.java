package logica;

import java.util.Date;

public class Inscripcion {
	public Date fecha_inscripcion;
	private float cantidad_pagada;
	
	private String estado;
	private Actividad actividad;
	private Profesional profesional;
	public Inscripcion(Date fecha_inscripcion, float cantidad_pagada, String estado, Actividad actividad,
			Profesional profesional) {
 		this.fecha_inscripcion = fecha_inscripcion;
		this.cantidad_pagada = cantidad_pagada;
		this.estado = estado;
		this.actividad = actividad;
		this.profesional = profesional;
	}
	public String getFecha_inscripcion() {
		return fecha_inscripcion.toString().substring(0, 10);
	}
	public void setFecha_inscripcion(Date fecha_inscripcion) {
		this.fecha_inscripcion = fecha_inscripcion;
	}
	public float getCantidad_pagada() {
		return cantidad_pagada;
	}
	public void setCantidad_pagada(float cantidad_pagada) {
		this.cantidad_pagada = cantidad_pagada;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Actividad getActividad() {
		return actividad;
	}
	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}
	public Profesional getProfesional() {
		return profesional;
	}
	public void setProfesional(Profesional profesional) {
		this.profesional = profesional;
	}
	
	
}
