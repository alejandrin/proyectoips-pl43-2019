package logica;

import java.sql.SQLException;
import java.util.Date;

import db.Persistencia;

public class Actividad {
	 String nombre,   objetivos, 
	  contenidos, espacio;
	 
	 Date fecha_abre_inscripcion, 
	  fecha_cierra_inscripcion,
	  fecha_inicio_imparticion,
	  fecha_fin_imparticion;
	 
	int num_plazas, id_profesor, id_actividad;
	double precio, remuneracion_profesor;
	
	public Actividad(
			String nombre, String objetivos, String contenidos, String espacio,
			Date fecha_abre_inscripcion,Date fecha_cierra_inscripcion, 
			Date fecha_inicio_imparticion, Date fecha_fin_imparticion,
			int num_plazas,	int id_profesor,int id_actividad, double precio2, double sueldoProfesor)
	{
		this.id_actividad = id_actividad;
		this.nombre = nombre;
		this.objetivos = objetivos;
		this.contenidos = contenidos;
		this.espacio = espacio;
		this.fecha_abre_inscripcion = fecha_abre_inscripcion;
		this.fecha_cierra_inscripcion = fecha_cierra_inscripcion;
		this.fecha_inicio_imparticion = fecha_inicio_imparticion;
		this.fecha_fin_imparticion = fecha_fin_imparticion;
		this.num_plazas = num_plazas;
		this.id_profesor = id_profesor;
		this.precio = precio2;
		this.remuneracion_profesor = sueldoProfesor;
	}
	
	public String toString() {
		return this.nombre+" "+this.espacio+" "+this.precio+" �";
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObjetivos() {
		return objetivos;
	}

	public void setObjetivos(String objetivos) {
		this.objetivos = objetivos;
	}

	public String getContenidos() {
		return contenidos;
	}

	public void setContenidos(String contenidos) {
		this.contenidos = contenidos;
	}

	public String getEspacio() {
		return espacio;
	}

	public void setEspacio(String espacio) {
		this.espacio = espacio;
	}

	public Date getFecha_abre_inscripcion() {
		return fecha_abre_inscripcion;
	}

	public void setFecha_abre_inscripcion(Date fecha_abre_inscripcion) {
		this.fecha_abre_inscripcion = fecha_abre_inscripcion;
	}

	public Date getFecha_cierra_inscripcion() {
		return fecha_cierra_inscripcion;
	}

	public void setFecha_cierra_inscripcion(Date fecha_cierra_inscripcion) {
		this.fecha_cierra_inscripcion = fecha_cierra_inscripcion;
	}

	public Date getFecha_inicio_imparticion() {
		return fecha_inicio_imparticion;
	}

	public void setFecha_inicio_imparticion(Date fecha_inicio_imparticion) {
		this.fecha_inicio_imparticion = fecha_inicio_imparticion;
	}

	public Date getFecha_fin_imparticion() {
		return fecha_fin_imparticion;
	}

	public void setFecha_fin_imparticion(Date fecha_fin_imparticion) {
		this.fecha_fin_imparticion = fecha_fin_imparticion;
	}

	public int getNum_plazas() {
		return num_plazas;
	}

	public void setNum_plazas(int num_plazas) {
		this.num_plazas = num_plazas;
	}

	public int getId_profesor() {
		return id_profesor;
	}

	public void setId_profesor(int id_profesor) {
		this.id_profesor = id_profesor;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public double getRemuneracion_profesor() {
		return remuneracion_profesor;
	}

	public void setRemuneracion_profesor(float remuneracion_profesor) {
		this.remuneracion_profesor = remuneracion_profesor;
	}

	public int getId_actividad() {
		return id_actividad;
	}

	public void setId_actividad(int id_actividad) {
		this.id_actividad = id_actividad;
	}

	public boolean comprobarActividad()  {
  		 int inscritos = 0;
		inscritos = Persistencia.getNumInsMul(id_actividad);
 		 if (inscritos+1>this.num_plazas)
		 {
 
				return false;

		 }
		return true;

	}

	public boolean comprobarActividadLista(int id_ins) {
		 int inscritos = 0;
			try {
				inscritos = Persistencia.getNumListaInscritos(id_ins);
 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if (inscritos+1>this.num_plazas)
			 {
	 
					return false;

			 }
			return true;
	}

}
