package util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import connection.ConnectionUtil;
import logica.EstadoActividad;
import logica.dto.ActividadDto;
import logica.dto.ColectivoDto;
import logica.dto.DatosEmpresaDto;
import logica.dto.EmpresaDto;
import logica.dto.FacturaProfesorDto;
import logica.dto.InscripcionDto;
import logica.dto.InscripcionMultipleDto;
import logica.dto.ProfesionalDto;
import logica.dto.ProfesorDto;
import logica.dto.TransferenciaProfesionalDto;
import logica.dto.TransferenciaProfesorDto;
import logica.dto.TransferenciasEmpresaDto;
import util.parser.Conf;
import util.parser.ParserActividad;
import util.parser.ParserColectivos;
import util.parser.ParserDatosEmpresa;
import util.parser.ParserEmpresa;
import util.parser.ParserFacturasProfesor;
import util.parser.ParserInscripcion;
import util.parser.ParserInscripcionMul;
import util.parser.ParserProfesionales;
import util.parser.ParserProfesor;
import util.parser.ParserTransferencia;
import util.parser.ParserTransferenciaEmpresa;
import util.parser.ParserTransferenciaProfesor;

public class DtoFactory {
	
	public int cancelaInscripcion(InscripcionDto canins) throws SQLException {
		String query = " UPDATE  lista_inscritos" + " SET estado='cancelada', plaza='sin plaza' "
				+ " WHERE id_profesional=" + canins.profesional.id + " AND id_inscripcion= " + canins.id;
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;

	}
	
	public static int actualizarFactura(int id2, double precio_a_pagar, double precio_pagado) throws SQLException {
		String query = "UPDATE facturas_profesores SET precio_a_pagar = ? , precio_pagado= ? WHERE id_factura=?";
		int rs = ConnectionUtil.executeUpdate(query,
				new String[] { precio_a_pagar + "", precio_pagado + "", id2 + "" });
		return rs;
	}

	public ProfesorDto getProfesorDto(int idProfesor) throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_DatosProfesor"),
				new Object[] { idProfesor });
		return new ParserProfesor().parseProfesor(rs);
	}

	public List<ProfesorDto> getProfesoresByActividad(int idActividad) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetProfesoresByActividad"),
				idActividad);
		return new ParserProfesor().parseProfesores(rs);
	}

	public ActividadDto getActividadDto(int idActividad) throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_DatosActividad"),
				new Object[] { idActividad });
		ActividadDto act = new ParserActividad().parseActividad(rs);
		act.id = idActividad;
		return act;
	}

	public List<InscripcionDto> getInscripciones(int idActividad) throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_InscripcionesActividad"),
				new Object[] { idActividad });
		return new ParserInscripcion().parseInscripciones(rs);
	}

	public List<ActividadDto> getActividades() throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_Actividades"), new Object[] {});
		return new ParserActividad().parseActividades(rs);
	}

	public List<ActividadDto> getActividadesPorEstadoYFecha(EstadoActividad estado, Date fechaInicio, Date fechaFin)
			throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_ActividadesPorEstadoYFecha"),
				estado.toString().toLowerCase(), fechaInicio, fechaFin);
		return new ParserActividad().parseActividades(rs);
	}

	public List<ActividadDto> getActividadesPorFecha(Date fechaInicio, Date fechaFin) throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_ActividadesPorFecha"),
				fechaInicio, fechaFin);
		return new ParserActividad().parseActividades(rs);
	}

	// Parse de transferencia Profesional
	public List<TransferenciaProfesionalDto> getTransferenciasClientes(int id_actividad) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_Transferencia"), id_actividad);
		return new ParserTransferencia().parseTransferenciasProfesional(rs);
	}

	public List<ActividadDto> getActividadesAbiertas(Date fechaInicio, Date fechaFin) throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_ActividadesAbiertas"),
				fechaInicio, fechaFin);
		return new ParserActividad().parseActividades(rs);
	}

	public List<ActividadDto> getActividadesIns() throws SQLException, IOException {
		String fechaInicio = new java.sql.Date(Calendar.getInstance().getTime().getTime()).toString() + " "
				+ LocalDateTime.now().getHour() + ":00:00";
		String query = "SELECT id_actividad,a.nombre,objetivos,contenidos,fecha_abre_inscripcion,"
				+ "fecha_cierra_inscripcion,fecha_inicio_imparticion,fecha_fin_imparticion,num_plazas,estado,ingresos_confirmados,"

				+ "ingresos_estimados,gastos_confirmados,gastos_estimados,precio,espacio " + "FROM actividades a "
				+ "WHERE fecha_abre_inscripcion <= '" + fechaInicio + "' AND fecha_cierra_inscripcion>= '" + fechaInicio
				+ "' AND estado='en periodo de inscripción' OR estado ='retrasada'";
		ResultSet rs = ConnectionUtil.executeQuery(query);

		return new ParserActividad().parseActividadesIns(rs);
	}

	public List<InscripcionDto> getInsACancelar() throws SQLException {
		String query = "SELECT id_inscripcion,id_profesional,id_actividad,precio,"
				+ " i.fecha_inscripcion, p.nombre, cantidad_pagada, l.estado "
				+ "FROM inscripcion i , actividades a, lista_inscritos l,"
				+ " profesionales p WHERE l.id_profesional=p.id_profesional AND"
				+ " estado!='cancelada' AND l.id_inscripcion=i.id_inscripcion " + "AND a.id_actividad = i.id_actividad";
		ResultSet rs = ConnectionUtil.executeQuery(query);
		return new ParserInscripcion().parseInscripciones(rs);
	}

	

	public java.util.Date fechaInicioImparticion(int id) throws SQLException {
		ResultSet rs = ConnectionUtil
				.executeQuery("SELECT fecha_inicio_imparticion FROM actividades " + "WHERE id_actividad=?", id);
		rs.next();
		return rs.getDate("fecha_inicio_imparticion");
	}

	public List<ActividadDto> getActividadesACancelar() throws SQLException {

		ResultSet rs = ConnectionUtil
				.executeQuery("SELECT id_actividad,a.nombre,objetivos,contenidos,fecha_abre_inscripcion,"
						+ "fecha_cierra_inscripcion,fecha_inicio_imparticion,fecha_fin_imparticion,num_plazas,estado,ingresos_confirmados,"
						+ "ingresos_estimados,gastos_confirmados,gastos_estimados,precio,espacio "
						+ "FROM actividades a  WHERE estado!='cerrada' AND estado!='cancelada'");
		return new ParserActividad().parseActividadesIns(rs);
	}

	public int cancelaactividad(ActividadDto canact) throws SQLException {

		int rs = ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_CancelaActividad"),
				new String[] { canact.id + "" });
		cancelartodasInscripciones(canact);
		return rs;
	}

	private int cancelartodasInscripciones(ActividadDto canact) throws SQLException {
		
 
 		 
		String query = "UPDATE lista_inscritos SET estado='cancelada', plaza='sin plaza', cantidad_a_devolver= cantidad_pagada WHERE id_inscripcion in (SELECT id_inscripcion FROM inscripcion i WHERE id_actividad="
				+ canact.id + ") ";
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;

	}

	public List<ProfesionalDto> profesionalesInscritos(int id_actividad) throws SQLException {
		String query = "SELECT p.nombre,p.apellidos, p.id_profesional FROM inscripcion i,lista_inscritos l,"
				+ " profesionales p WHERE i.id_inscripcion= l.id_inscripcion "
				+ "AND l.id_profesional = p.id_profesional " + "AND i.id_actividad =" + id_actividad;
		ResultSet rs = ConnectionUtil.executeQuery(query);
		return new ParserProfesionales().parseProfesionales(rs);
	}

	public double cantidadpagadaProfesionalActividad(int id_actividad, int id_profesional) throws SQLException {
		String query = "SELECT cantidad_pagada " + "FROM inscripcion i, lista_inscritos l "
				+ "WHERE i.id_inscripcion= l.id_inscripcion AND l.id_profesional =" + id_profesional
				+ " AND id_actividad =" + id_actividad;
		ResultSet rs = ConnectionUtil.executeQuery(query);

		rs.next();

		double cantidad_pagada = rs.getDouble("cantidad_pagada");
		return cantidad_pagada;
	}

	// Historia 6
	public void cancelarMovimiento(int id) throws SQLException {
		ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_BorrarMovimiento"), new String[] { id + "" });

	}

	public List<ProfesionalDto> getListaInscripciones(int idInscripcion) throws SQLException {
		// TODO Auto-generated method stub
		String query = "SELECT p.nombre,p.apellidos, p.id_profesional FROM lista_inscritos l,"
				+ " profesionales p WHERE l.id_profesional = p.id_profesional " + "AND l.id_inscripcion ="
				+ idInscripcion;
		ResultSet rs = ConnectionUtil.executeQuery(query);
		return new ParserProfesionales().parseProfesionales(rs);
	}

	public int agregarCantidadPagada(String estado, double cantidadPagada, int idpro, int idins) throws SQLException {
		String query = "UPDATE lista_inscritos SET estado= ?," + " cantidad_pagada=? WHERE id_profesional=? AND"
				+ " id_inscripcion=?";
		int rs = ConnectionUtil.executeUpdate(query,
				new String[] { estado, cantidadPagada + "", idpro + "", idins + "" });
		return rs;
	}

	// HISTORIA 3
	public void savePriceColectivo(ColectivoDto col, double price) throws SQLException {
		ResultSet id_actividad = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_UltimoIDActividad"));
		if (id_actividad.next())
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_InsertPrecioColectivo"), col.id,
					id_actividad.getInt(1), price);
	}

	public List<ColectivoDto> getColectivos() throws SQLException {
		ResultSet cols = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetColectivos"));
		return new ParserColectivos().parseColectivos(cols);
	}

	public static boolean saveColectivo(ColectivoDto nuevoColectivo) throws SQLException {
		return ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_SaveColectivo"), nuevoColectivo.id,
				nuevoColectivo.nombre) == 1;
	}

	public static ColectivoDto getColectivoByName(String name) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_ColectivoByName"), name);
		List<ColectivoDto> list = new ParserColectivos().parseColectivos(rs);
		return list.isEmpty() ? null : list.get(0);
	}

	public List<ColectivoDto> getColectivosActividad(int idActividad) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_ColectivosByActividad"),
				idActividad);
		return new ParserColectivos().parseColectivos(rs);
	}

	public double getPrecioColectivoActividad(int idActividad, int idColectivo) throws SQLException, IOException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetPrecioActividadColectivo"),
				idActividad, idColectivo);
		if (rs.next())
			return rs.getDouble(1);
		else
			return getPrecioActividad(idActividad);
	}

	public double getPrecioActividad(int idActividad) throws SQLException, IOException {
		return getActividadDto(idActividad).precio;
	}

	public Map<ColectivoDto, Double> getPreciosColectivos(int idActividad) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetPreciosColectivos"),
				idActividad);
		return new ParserColectivos().parsePrecios(rs);
	}

	public List<TransferenciaProfesorDto> getTransferenciasProfesor(int id) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_Transferencia_Profesor"), id);
		return new ParserTransferenciaProfesor().parseTransferenciasProfesor(rs);
	}

	public List<TransferenciaProfesorDto> getNumeroTransferenciasProfesor(int id) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_Transferencia_Profesor_Total"));
		return new ParserTransferenciaProfesor().parseTransferenciasProfesor(rs);
	}

	public List<InscripcionMultipleDto> getInsMul(int act_id) throws SQLException {
		String query = "SELECT    count(l.id_profesional) as numins ,  sum(precio) as total,sum(cantidad_a_devolver) as cdev, id_inscripcion,  id_actividad, nombre\r\n"
				+ "FROM inscripcion i, lista_inscritos l, actividades a\r\n"
				+ "WHERE  i.id_inscripcion = l.id_inscripcion and a.id_actividad=" + act_id
				+ " and a.id_actividad = i.id_actividad   \r\n"
				+ "GROUP BY id_inscripcion, nombre,precio";
		ResultSet rs = ConnectionUtil.executeQuery(query);

		return new ParserInscripcionMul().parseInsMul(rs);
	}

	public int pagarInsMultiple(InscripcionMultipleDto mulins, int pago, String estado) throws SQLException {
		String query = " UPDATE  lista_inscritos SET estado='" + estado + "' , cantidad_pagada= cantidad_pagada +"
				+ pago / mulins.numins + " WHERE id_inscripcion=" + mulins.id;
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public EmpresaDto getPagoEmpresas(ActividadDto act) throws SQLException {
		String query = "SELECT a.id_actividad, a.nombre as nombre_act ," + "cif, id_empresa ,e.nombre as nombre_emp, "
				+ "remuneracion_empresa, pagado_empresa\r\n" + "FROM actividades a, empresas e \r\n"
				+ "WHERE a.id_empresa is not null" + " and a.id_empresa = e.id_empresa and a.id_actividad= " + act.id;
		ResultSet rs = ConnectionUtil.executeQuery(query);
		return new ParserEmpresa().parseEmpresas(rs);
	}

	public int pagarEmpresa(EmpresaDto emp, double pago) throws SQLException {
		String query = "UPDATE actividades" + " SET pagado_empresa =   " + pago
				+ " WHERE id_actividad = " + emp.actividad.id;
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public int eliminarInscripcion(int idInscripcion) throws SQLException {
		String query = "DELETE FROM lista_inscritos WHERE id_inscripcion=" + idInscripcion;
		int rs = ConnectionUtil.executeUpdate(query);

		String query2 = "DELETE FROM inscripcion WHERE id_inscripcion=" + idInscripcion;
		rs = ConnectionUtil.executeUpdate(query2);

		System.out.print("inscripciones eliminadas");

		return rs;

	}

	public int eliminarIncrito(ProfesionalDto caninscrito) throws SQLException {
		String query = "DELETE FROM lista_inscritos WHERE id_profesional=" + caninscrito.id;
		int rs = ConnectionUtil.executeUpdate(query);
		String query2 = "DELETE FROM profesionales WHERE id_profesional=" + caninscrito.id;
		rs = ConnectionUtil.executeUpdate(query2);
		return rs;

	}

	public List<TransferenciasEmpresaDto> getTransferenciasEmpresa(EmpresaDto emp, int act_id) throws SQLException {
		String query = "select id_empresa, cantidad_pagada_por_COIIPA, " + "     id_transferencia_empresa, "
				+ "fecha_transferencia_empresa " + ",tipo FROM transferencias_empresa t " + "WHERE id_actividad ="
				+ act_id + " AND id_empresa = " + emp.id_empresa;
		ResultSet rs = ConnectionUtil.executeQuery(query);
		return new ParserTransferenciaEmpresa().parseTransferenciasEmpresas(rs);

	}

	public List<TransferenciaProfesionalDto> getAllTransferenciasClientes(int id_actividad) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_TransferenciaAll"));
		return new ParserTransferencia().parseTransferenciasProfesional(rs);
	}

	public int tranferenciaEmpresa(EmpresaDto emp, int pago, int ano, int mes, int dia, int id_trans, String tipoPago,
			int id_act) throws SQLException {
		String query = "insert into transferencias_empresa values " + "( '" + emp.id_empresa + "','" + id_trans + "',"
				+ "TO_DATE('" + ano + "/" + mes + "/" + dia + " " + "00:00:00','YYYY/MM/DD HH:MI:SS')" + ",'" + pago
				+ "', '" + tipoPago + "', '" + id_act + "')";
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public int getid_trans_emp() throws SQLException {

		String query = "select max(id_transferencia_empresa) as max_id from transferencias_empresa ";
		ResultSet rs = ConnectionUtil.executeQuery(query);

		rs.next();
		int idtrans = rs.getInt(1) + 1;

		return idtrans;
	}

	public String getResponsable(int id) throws SQLException {
		ResultSet rs;
		String query = "SELECT TOP 1 apellidos FROM profesionales p, lista_inscritos l, inscripcion i\r\n"
				+ "WHERE  i.id_inscripcion = l.id_inscripcion and i.id_inscripcion = " + id
				+ " and p.id_profesional = l.id_profesional\r\n" + "ORDER BY id_profesional ASC; ";

		rs = ConnectionUtil.executeQuery(query);

		rs.next();
		return rs.getString("apellidos");

	}
	

	public int getid_trans_prof() throws SQLException {
		String query = "select max(id_transferencia_profesional) as max_id from transferencias_profesional ";
		ResultSet rs = ConnectionUtil.executeQuery(query);

		rs.next();
		int idtrans = rs.getInt(1) + 1;

		return idtrans;
	}

	public int tranferenciaInsMul(InscripcionMultipleDto mulins, int pago, int ano, int mes, int dia, int hora,
			int id_trans, String tipo, String plaza) throws SQLException {
		String query = "insert into transferencias_profesional values " + "( '" + id_trans + "','" +

				mulins.actividad.id + "','" + pago + "'," + "TO_DATE('" + ano + "/" + mes + "/" + dia + " " + hora + ":"

				+ "00:00','YYYY/MM/DD HH:MI:SS')" + ",'" + mulins.id + "','" + 1.0 + "', '" + tipo + "' )";
		int rs = ConnectionUtil.executeUpdate(query);
		String query2 = "update lista_inscritos set plaza = '" + plaza + "' where id_inscripcion =" + mulins.id;
		int rs2 = ConnectionUtil.executeUpdate(query2);

		return rs2;
	}

	public List<TransferenciaProfesionalDto> getTransferenciasMulIns(InscripcionMultipleDto mulins)
			throws SQLException {
		String query = "Select  id_transferencia_profesional, id_actividad , cantidad_pagada_por_coiipa  ,\r\n"
				+ "fecha_transferencia  , id_inscripcion  , id_profesional   \r\n"
				+ ", tipo from transferencias_profesional where id_inscripcion = " + mulins.id;
		ResultSet rs = ConnectionUtil.executeQuery(query);
		return new ParserTransferencia().parseTransferenciasMulIns(rs);

	}

	public int registrarprofesor(int id_pro, String nombre, String apellidos, String email, String tlf, String dni)
			throws SQLException {
		String query = "insert into profesores values " + "( '" + id_pro + "','" +

				nombre + "','" + apellidos + "','" + email + "','" + tlf + "','" + dni + "' )";
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;

	}

	public int getIdprofesor() throws SQLException {
		String query = "select max(id_profesor) as max_id from profesores ";
		ResultSet rs = ConnectionUtil.executeQuery(query);

		rs.next();
		int idpro = rs.getInt(1) + 1;

		return idpro;
	}

	public int getId_emp() throws SQLException {
		String query = "select max(id_empresa) as max_id from empresas ";
		ResultSet rs = ConnectionUtil.executeQuery(query);

		rs.next();
		int idemp = rs.getInt(1) + 1;

		return idemp;
	}

	public int registrarEmpresa(int id_emp, String nombre_emp, String repre, String email_emp, String tlf_emp,
			String cif) throws SQLException {
		String query = "insert into empresas values " + "( '" + id_emp + "','" +

				nombre_emp + "','" + repre + "','" + email_emp + "','" + tlf_emp + "','" + cif + "' )";
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public ColectivoDto getColectivoProfesional(int id) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_ColectivoProfesional"), id);
		return new ParserColectivos().parseColectivo(rs);
	}

	public double getCantidadPagadaInsMul(int id) throws SQLException {
		String query = "select sum(cantidad_pagada_por_coiipa) as cp from transferencias_profesional t where id_inscripcion = "
				+ id;

		ResultSet rs = ConnectionUtil.executeQuery(query);

		rs.next();
		int cp = rs.getInt(1);

		return cp;
	}

	public ResultSet getfacturas(int id_empresa, int id_act) throws SQLException {
		String query = "SELECT id_factura, cantidad_factura, fecha_factura "
				+ "FROM factura_empresa WHERE id_actividad= " + id_act + " AND id_empresa =" + id_empresa;

		ResultSet rs = ConnectionUtil.executeQuery(query);
		return rs;
	}

	public int agregarFactura(int idfact, int anofact, int mesfact, int diafact, int cantidad,
			ActividadDto actividadcmb, EmpresaDto empresaPago) throws SQLException {
		String query = "insert into factura_empresa values " + "( '" + idfact + "','" + cantidad + "'," + "TO_DATE('"
				+ anofact + "/" + mesfact + "/" + diafact + " "

				+ "00:00:00','YYYY/MM/DD HH:MI:SS')" + ",'" +

				empresaPago.id_empresa + "','" + actividadcmb.id + "' )";
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;

	}

	public int modificarFactura(int idfact, int anofact, int mesfact, int diafact, int cantidad, EmpresaDto empresaPago)
			throws SQLException {
		String query = "update factura_empresa set " + "id_factura= " + idfact + ", cantidad_factura=" + cantidad
				+ ", fecha_factura=" + "TO_DATE('" + anofact + "/" + mesfact + "/" + diafact + " "

				+ "00:00:00','YYYY/MM/DD HH:MI:SS')" + " where id_empresa =" + empresaPago.id_empresa;
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public List<ProfesorDto> getProfesores() throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetProfesores"));
		return new ParserProfesor().parseProfesores(rs);
	}

	public List<DatosEmpresaDto> getDatosEmpresas() throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetDatosEmpresas"));
		return new ParserDatosEmpresa().parseEmpresas(rs);
	}

	public DatosEmpresaDto getEmpresaById(int id_emp) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetDatosEmpresaById"), id_emp);
		return new ParserDatosEmpresa().parseEmpresa(rs);
	}
	
	public List<FacturaProfesorDto> getFacturasProfesor() throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetFacturasProfesor"));
		return new ParserFacturasProfesor().parseFacturas(rs);
	}
	
	public FacturaProfesorDto getFacturaProfesor(int id_profesor,int id_actividad) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetFacturasProfesorID"),id_profesor,id_actividad);
		return new ParserFacturasProfesor().parseFacturaB(rs);
	}

	public void asignarEmpresa(int id_empresa, double remuneracion) throws SQLException {
		ResultSet id_actividad = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_UltimoIDActividad"));
		if (id_actividad.next()) {
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_AsignarEmpresa"), id_empresa, remuneracion,
					id_actividad.getInt(1));
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateGastosEstimados"), remuneracion,
					id_actividad.getInt(1));
		}
	}

	public DatosEmpresaDto getEmpresaByActividad(int id) throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_GetDatosEmpresaByActividad"),
				id);
		return new ParserDatosEmpresa().parseEmpresa(rs);
	}

	public int retrasaractividad(ActividadDto retact, int ano, int mes, int dia) throws SQLException {

		String query = "update actividades set " + "estado= 'retrasada' ," + " fecha_inicio_imparticion= " + "TO_DATE('"
				+ ano + "/" + mes + "/" + dia + " "

				+ "00:00:00','YYYY/MM/DD HH:MI:SS')" + " where id_actividad =" + retact.id;
		int rs2 = ConnectionUtil.executeUpdate(query);
		return rs2;
	}

	public List<TransferenciaProfesorDto> getNumeroTransferenciasProfesorTotales() throws SQLException {
		ResultSet rs = ConnectionUtil.executeQuery(Conf.getInstance().getProperty("SQL_Transferencia_Profesor_Totales"));
		return new ParserTransferenciaProfesor().parseTransferenciasProfesor(rs);
	}

	public void cancelarMovimientoProfesor(int id) throws SQLException {
		ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_BorrarMovimiento_Profesor"),
				new String[] { id + "" });
		
	}
	
	public void cancelarFactura(int id) throws SQLException{
		ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_BorrarFactura_Profesor"),
				new String[] { id + "" });
	}
	public int agregarcantidadadevolver(double cantidadDevuelta, InscripcionDto ins) throws SQLException {
		String query = "update lista_inscritos set " + "cantidad_a_devolver= "  +cantidadDevuelta 
				+ " where id_inscripcion =" + ins.id + " and estado = 'cancelada'";
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public int setCantidadPagada(InscripcionMultipleDto ins) throws SQLException {
		String query = "update lista_inscritos set " + "cantidad_pagada= "  +ins.cantidad_pagada/ins.numins 
				+ " where id_inscripcion =" + ins.id;
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public int cambiarDevolver(int id, double cd) throws SQLException {
		String query = "update lista_inscritos set " + "cantidad_a_devolver= "  +cd
				+ " where estado= 'cancelada' and id_inscripcion =" + id;
		int rs = ConnectionUtil.executeUpdate(query);
		return rs;
	}

	public String getPlaza(int id) throws SQLException {
		ResultSet rs;
		String query = "SELECT TOP 1 plaza FROM   lista_inscritos l \r\n" + 
				"				 WHERE l.id_inscripcion = " + id+
				"				  ORDER BY plaza DESC; ";

		rs = ConnectionUtil.executeQuery(query);

		rs.next();
		return rs.getString("plaza");
	}

	

 
}
