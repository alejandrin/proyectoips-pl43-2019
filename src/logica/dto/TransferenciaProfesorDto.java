package logica.dto;

import java.util.Date;

public class TransferenciaProfesorDto {
	public int id;
	public InscripcionDto inscripcion;
	public ProfesorDto profesor;
	public ActividadDto actividad;
	public double cantidad_pagada_por_COIIPA;
	public double cantidad_debida_profesor;
	public Date fecha_transferencia;
	public String tipoPago;
}
