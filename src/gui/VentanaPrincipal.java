package gui;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import connection.ConnectionUtil;
import db.Persistencia;
import gui.componentes.colectivos.inscripcion.ComboBoxColectivos;
import gui.componentes.colectivos.planificacion.PanelPrecios;
import gui.componentes.imparticion.PanelImparticionEmpresa;
import gui.componentes.tipoDePago.PanelTipoPago;
import gui.responsableFormacion.VentanaBalanceActividades;
import gui.secretaria.VentanaActividad;
import gui.secretaria.VentanaCerrarActividad;
import gui.secretaria.VentanaProfesionales;
import gui.secretaria.VentanaProfesores;
import logica.Actividad;
import logica.Inscripcion;
import logica.Profesional;
import logica.dto.ActividadDto;
import logica.dto.ColectivoDto;
import logica.dto.DatosEmpresaDto;
import logica.dto.EmpresaDto;
import logica.dto.InscripcionDto;
import logica.dto.InscripcionMultipleDto;
import logica.dto.ProfesionalDto;
import logica.dto.TransferenciaProfesionalDto;
import logica.dto.TransferenciasEmpresaDto;
import util.Dates;
import util.DtoFactory;
import util.parser.Conf;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("unused")
public class VentanaPrincipal extends JFrame {

	private static final long serialVersionUID = 1L;
	protected static final int WARNING_MESSAGE = 2;
	protected static final int INFORMATION_MESSAGE = 1;


	private JPanel cardLayout;
	private JPanel pnPrincipal;
	private JLabel lblSeleccioneRol;
	private JButton btnPlanificarUnaActividad;
	private JPanel pnRespForm_Planif_Activ;
	private JLabel lblNombreDeLa;
	private JTextField textNombreActividad;
	private JLabel lblObjetivos;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JLabel lblContenidos;
	private JLabel lblIdDelProfesor;
	private JLabel lblRemuneracin;
	private JLabel lblInscripcinDesde;
	private JLabel lblHasta;
	private JLabel lblNmeroDePlazas;
	private JSpinner num_plazas;
	private JLabel lblCreacinDeUna;
	private JButton btnAceptar;

	private JLabel lblDa;
	private JLabel lblMs;
	private JLabel lblAo;

	private JSpinner insDesdeDia;
	private JSpinner insDesdeMes;
	private JSpinner insDesdeAno;
	private JSpinner insHastaDia;
	private JSpinner insHastaMes;
	private JSpinner insHastaAno;

	private JLabel lblHora;
	private JLabel lblMinuto;
	private JLabel label;
	private JLabel label_1;

	private JSpinner insDesdeHora;
	private JSpinner insDesdeMinuto;
	private JSpinner insHastaHora;
	private JSpinner insHastaMinuto;
	private JSpinner spinnerRemuneracion;
	private JButton btnInscribirse;
	private JPanel pnListadoActividades;
	private JButton btnFinalizarIns;

	private JScrollPane sPaneAct;
	private JTable tblActividades;
	private TableModel modeloInscr;
	private TableModel modeloMulIns;
	private TableModel modeloActCan;
	private TableModel modeloListIncr;
	private JTextField txtNombre;
	private JTextField txtApellidos;
	private JTextField txtEmail;

	private List<ActividadDto> listactividades;
	private List<ActividadDto> listactcan;
	private List<ProfesionalDto> listins;
	private List<InscripcionMultipleDto> listmulins;

	private CardLayout parentcard;
	private JButton btnAtras;
	private List<InscripcionDto> listinscripciones;
	private JButton btnCancelarUnaInscripcion;
	private TableModel modeloICan;

	private InscripcionDto canins;
	private ActividadDto canact;
	private InscripcionMultipleDto mulins;

	private JLabel lblElegirUnaActividad;
	private JButton btnNext;

	Actividad actins;
	private JButton btnCancelar_pnRespForm_Planif_Activ;
	private JScrollPane scrollPane_2;
	private JTable tableProfesores;

	private int idInscripcion;
	private ProfesionalDto caninscrito;

	private EmpresaDto emp;
	private ActividadDto actividadcmb;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logo-COIIPA.png")));
		setTitle("Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1277, 706);
		cardLayout = new JPanel();
		cardLayout.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(cardLayout);
		parentcard = new CardLayout(0, 0);
		cardLayout.setLayout(parentcard);
		cardLayout.add(getPnPrincipal(), "pnPrincipal");
		cardLayout.add(getPnRespForm_Planif_Activ(), "pnRespForm_Planif_Activ");
		cardLayout.add(getPnListadoActividades(), "pnListadoActividades");
		cardLayout.add(getPnCancelarActividades(), "pnCancelarAct");
		cardLayout.add(getPnPagarEmpresa(), "pnPagoEmpresa");
		cardLayout.add(getPnPagarInsMul(), "pnInsMul");
		cardLayout.add(getPnRegistrarProfesor(), "pnRegistrarProfesor");
		cardLayout.add(getPnRegistrarEmpresa(), "pnRegistrarEmpresa");
		cardLayout.add(getPnRetrasar(), "pnRetrasar");

		// �sto es para que no se vea el id en la tabla de profesores de crear
		// actividad
		tableProfesores.getColumnModel().getColumn(1).setPreferredWidth(0);
		tableProfesores.getColumnModel().getColumn(1).setMaxWidth(0);
		tableProfesores.getColumnModel().getColumn(1).setMinWidth(0);
		tableProfesores.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
		tableProfesores.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
		tableProfesores.getColumnModel().getColumn(1).setResizable(false);


		// �sto es para que no se vea el id en la tabla de profesores de crear
		// actividad
		tableProfesoresAnadidos.getColumnModel().getColumn(1).setPreferredWidth(0);
		tableProfesoresAnadidos.getColumnModel().getColumn(1).setMaxWidth(0);
		tableProfesoresAnadidos.getColumnModel().getColumn(1).setMinWidth(0);
		tableProfesoresAnadidos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
		tableProfesoresAnadidos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
		tableProfesoresAnadidos.getColumnModel().getColumn(1).setResizable(false);

		this.setExtendedState(MAXIMIZED_BOTH);

	}

	private void inicializar() {
		((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");
		textNombreActividad.setText("");
		textObjetivos.setText("");
		textContenidos.setText("");
		spinnerRemuneracion.setValue(0);
		//textEspacio.setText("");


	}

	// Modelo para la tabla
	private ModeloNoEditable modeloTabla;
	private ModeloNoEditable modeloTablaProfesoresAnadidos;
	private ModeloNoEditable modeloTablaSesiones;

	private JTextArea textObjetivos;
	private JTextArea textContenidos;
	private JButton btnConsultarIngresosY;
	private JLabel lblActividades;
	private JLabel lblInscripciones;
	private JButton btnConsultarElEstado;
	private JButton btnCerrarActividad;
	private JButton btnCancelarActividad;
	private JPanel pnCancelarActividades;
	private JButton btnCancelarActividad_1;
	private JLabel lblSeleccioneUnaActividad_1;
	private JScrollPane scrollPane_3;
	private JButton buttonAtraspCA;
	private JTable tableActividadesAcancelar;
	private JPanel pnLote;
	private JLabel lblInscripcionesEnLote;
	private JScrollPane sPLote;
	private JTable tableLote;
	private JButton btnAgregarInscritoA;
	private List<ProfesionalDto> profesionaleslista;
	private Inscripcion inscripcion;
	private PanelPrecios pnPrecios;
	private JButton btnPagarIncripcion;
	private JLabel lbColectivo;
	private ComboBoxColectivos cbColectivo;
	private ListInACell listInACell;
	private JPanel pnPagarEmpresa;
	private JLabel lblPagarAUna;
	private JButton btnAgregarPago;
	private JSpinner spinnerPagoEmpresa;
	private TableModel modeloEmpr;
	private List<EmpresaDto> listempresa;
	private JLabel lblEmpresas;
	private JButton btnPagarEmpresas;
	private JButton btnAtras_1;
	private JButton btnEliminarInscrito;
	private JComboBox<ActividadDto> cmbActividades;
	private JLabel lblSeleccionaActividad;
	private ComboBoxModel<ActividadDto> modeloActividades;
	private JLabel lblFechaTransferencia;
	private JSpinner spAno;
	private JSpinner spDia;
	private JLabel lblAno_emp;
	private JLabel lblDia_emp;
	private JSpinner spMes;
	private JLabel lblMes;
	private JLabel lblPagosRealizados;
	private JScrollPane sPtransEmp;
	private JTable temptrans;
	private JLabel lblNumeroFactura;
	private JTextField txtIdFactnueva;
	private List<TransferenciasEmpresaDto> list_trans_emp;
	private DefaultTableModel modeloTrans;
	private JButton btnDevolverPago;
	private JPanel pnPagarInsMul;
	private JLabel lblpagarInsMul;
	private JScrollPane spInsMul;
	private JButton btn_add_ins_mul;
	private JSpinner spPrecio_insmul;
	private JButton btnAtras_insmul;
	private JComboBox<ActividadDto> cmb_act_ins_mul;
	private JLabel label_3;
	private JLabel label_4;
	private JSpinner sp_ano_ins_mul;
	private JSpinner sp_dia_ins_mul;
	private JLabel label_5;
	private JLabel label_6;
	private JSpinner sp_mes_ins_mul;
	private JLabel label_8;
	private JLabel lblpagosinsm;
	private JScrollPane spTransferencias_ins_mul;
	private JButton btn_devolver_ins_mul;
	private JButton btnPagarInscripcionMultiple;
	private JTable tMulIns;
	private JTable tTransferenciaInscripcion;
	private DefaultTableModel modeloTransProf;
	private List<TransferenciaProfesionalDto> list_trans_prof;
	private JPanel pnRegistrarProfesor;
	private JLabel lblRegistrarProfesor;
	private JLabel lblNombre_1;
	private JLabel lblApellidos_1;
	private JLabel lblEmail_1;
	private JLabel lblTelefonoopcional;
	private JLabel lblDniopcional;
	private JTextField txtNombre_pro;
	private JTextField txtapellidos_pro;
	private JTextField txtEmail_pro;
	private JTextField txttlf_pro;
	private JTextField txtdni_pro;
	private JButton btnAtras_pro;
	private JButton btnRegistrarProfesor;
	private JPanel pnRegistrarEmpresa;
	private JLabel lblRegistrarEmpresa;
	private JLabel lblNombreEmpresa;
	private JLabel lblNombreDelRepresentante;
	private JLabel label_12;
	private JLabel label_13;
	private JLabel lblCifopcional;
	private JTextField txtnombre_emp;
	private JTextField txtrepresentante_emp;
	private JTextField txtemail_emp;
	private JTextField txttlf_emp;
	private JTextField txtCIF_emp;
	private JButton button;
	private JButton btnRegistrarEmpresa;
	private PanelTipoPago pnTipoPagoEmpresa;
	private PanelTipoPago pnTipoPagoMultiple;
	private JButton btnAadir;
	private JScrollPane scrollPane_4;
	private JLabel lblProfesoresAadidos;
	private JTable tableProfesoresAnadidos;

	private JLabel lblNombre_2;
	private JLabel lblRemuneracion;
	private JLabel lblCif;
	private JLabel lblFactura;
	private JLabel lblCantidadFactura;
	private JLabel lblFecha_1;
	private JTextField txtnombreEmp;
	private JTextField txtremuneracionemp;
	private JTextField txtnicifemp;
	private JLabel label_2;
	private JSpinner spDiafact;
	private JSpinner spmesfact;
	private JSpinner spAnofact;
	private JLabel label_7;
	private JLabel label_9;
	private JLabel lblIdFactura;
	private JTextField txtidfactemp;
	private JTextField txtcantfactemp;
	private JTextField txtfechafactemp;
	private JButton btnModificar;
	private JSpinner spcantidadfact;
	private JLabel lblFechaFactura;
	private JLabel lblAgregarOModificar;
	private JLabel lblCantidadPagada;
	private JTextField txtcantidadpagadaemp;


	private JButton btnQuitar;
	private JLabel lblSePuedenCancelar;
	private JTabbedPane tbImparticion;
	private JPanel pnProfesores;
	private PanelImparticionEmpresa pnEmpresa;
	private JLabel lbImparticion;
	private JLabel lblSesiones;
	private JLabel lblEspacio_1;
	private JLabel lblDia;
	private JTextField textEspacioSesion;
	private JSpinner diaSesion;
	private JSpinner mesSesion;
	private JLabel lblMes_1;
	private JLabel lblFecha_2;
	private JLabel lblAo_1;
	private JSpinner anoSesion;
	private JButton btnAadirSesion;
	private JScrollPane scrollPane_5;
	private JTable tableSesiones;
	private JLabel label_10;
	private JLabel label_11;
	private JSpinner horaSesion;
	private JSpinner minutoSesion;
	private JLabel lblDuracion_1;
	private JSpinner duracionSesion;
	private JButton btnBorrarSesion;
	private JPanel pnRetrasar;
	private JScrollPane sPretrasar;
	private JTable tretrasar;
	private DefaultTableModel modeloActRet;
	private List<ActividadDto> listactret;
	private JSpinner spDiaRet;
	private JLabel label_14;
	private JSpinner spAnoRet;
	private JLabel label_15;
	private JLabel label_16;
	private JSpinner spMesRet;
	private JLabel label_17;
	private JButton btnRetrasarActividad;

	private ActividadDto retact;
	private JButton btnAtras_ret;
	private JButton botonPagarProfesores;
	private JLabel label_18;
	private JLabel label_19;
	private JLabel label_20;


	public class ModeloNoEditable extends DefaultTableModel {
		private static final long serialVersionUID = 1L;

		public ModeloNoEditable(Object[] columnNames, int rowCount) {
			super(columnNames, rowCount);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	}

	private JPanel getPnPrincipal() {
		if (pnPrincipal == null) {
			pnPrincipal = new JPanel();
			pnPrincipal.setLayout(null);
			pnPrincipal.add(getLblSeleccioneRol());
			pnPrincipal.add(getBtnPlanificarUnaActividad());
			pnPrincipal.add(getBtnConsultarIngresosY());
			pnPrincipal.add(getBtnInscribirse());
			pnPrincipal.add(getLblActividades());
			pnPrincipal.add(getLblInscripciones());
			pnPrincipal.add(getBtnConsultarElEstado());
			pnPrincipal.add(getBtnCerrarActividad());
			pnPrincipal.add(getBtnCancelarActividad());
			pnPrincipal.add(getBtnPagarIncripcion());
			pnPrincipal.add(getLblEmpresas());
			pnPrincipal.add(getBtnPagarEmpresas());
			pnPrincipal.add(getBtnPagarInscripcionMultiple());
			pnPrincipal.add(getBotonPagarProfesores());
		}
		return pnPrincipal;
	}

	private JLabel getLblSeleccioneRol() {
		if (lblSeleccioneRol == null) {
			lblSeleccioneRol = new JLabel("Seleccione una opci\u00F3n:");
			lblSeleccioneRol.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblSeleccioneRol.setBounds(10, 11, 380, 30);
		}
		return lblSeleccioneRol;
	}

	private JButton getBtnPlanificarUnaActividad() {
		if (btnPlanificarUnaActividad == null) {
			btnPlanificarUnaActividad = new JButton("Planificar una actividad");
			btnPlanificarUnaActividad.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnPlanificarUnaActividad.setBounds(10, 90, 419, 30);
			btnPlanificarUnaActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setTitle("Planificar una actividad");
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnRespForm_Planif_Activ");

					// Borrar las filas que ya halla pa que no se repitan
					borrarFilasProfesores();

					// Cargar profesores
					agregarFilasProfesores();

					// Crear el panel de precios
					pnRespForm_Planif_Activ.add(getPnPrecios());

					//Seleccionar pesta�a de profesores en imparticion
					tbImparticion.setSelectedIndex(0);
				}
			});
			btnPlanificarUnaActividad.setMnemonic('p');
		}
		return btnPlanificarUnaActividad;
	}

	private void agregarFilasProfesores() {
		Object[] nuevaFila = new Object[2];// La fila tendr� tantos objetos como columnas

		try {
			ArrayList<ArrayList<Object>> datos = Persistencia.getListaProfesores();
			for (int i = 0; i < datos.size(); i++) {
				nuevaFila[0] = datos.get(i).get(0);
				nuevaFila[1] = datos.get(i).get(1);
				modeloTabla.addRow(nuevaFila);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void borrarFilasProfesores() {
		ModeloNoEditable tab = (ModeloNoEditable) tableProfesores.getModel();
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.removeRow(i);
		}

	}

	private JPanel getPnRespForm_Planif_Activ() {
		if (pnRespForm_Planif_Activ == null) {
			pnRespForm_Planif_Activ = new JPanel();
			pnRespForm_Planif_Activ.setLayout(null);
			pnRespForm_Planif_Activ.add(getLblNombreDeLa());
			pnRespForm_Planif_Activ.add(getTextNombreActividad());
			pnRespForm_Planif_Activ.add(getLblObjetivos());
			pnRespForm_Planif_Activ.add(getScrollPane());
			pnRespForm_Planif_Activ.add(getScrollPane_1());
			pnRespForm_Planif_Activ.add(getLblContenidos());
			pnRespForm_Planif_Activ.add(getLblInscripcinDesde());
			pnRespForm_Planif_Activ.add(getLblHasta());
			pnRespForm_Planif_Activ.add(getLblNmeroDePlazas());
			pnRespForm_Planif_Activ.add(getNum_plazas());
			pnRespForm_Planif_Activ.add(getLblCreacinDeUna());
			pnRespForm_Planif_Activ.add(getBtnAceptar());
			pnRespForm_Planif_Activ.add(getLblDa());
			pnRespForm_Planif_Activ.add(getLblMs());
			pnRespForm_Planif_Activ.add(getLblAo());
			pnRespForm_Planif_Activ.add(getInsDesdeDia());
			pnRespForm_Planif_Activ.add(getInsDesdeMes());
			pnRespForm_Planif_Activ.add(getInsDesdeAno());
			pnRespForm_Planif_Activ.add(getInsHastaDia());
			pnRespForm_Planif_Activ.add(getInsHastaMes());
			pnRespForm_Planif_Activ.add(getInsHastaAno());
			pnRespForm_Planif_Activ.add(getLblHora());
			pnRespForm_Planif_Activ.add(getLblMinuto());
			pnRespForm_Planif_Activ.add(getLabel());
			pnRespForm_Planif_Activ.add(getLabel_1());
			pnRespForm_Planif_Activ.add(getInsDesdeHora());
			pnRespForm_Planif_Activ.add(getInsDesdeMinuto());
			pnRespForm_Planif_Activ.add(getInsHastaHora());
			pnRespForm_Planif_Activ.add(getInsHastaMinuto());
			pnRespForm_Planif_Activ.add(getBtnCancelar_pnRespForm_Planif_Activ());
			pnRespForm_Planif_Activ.add(getTbImparticion());
			pnRespForm_Planif_Activ.add(getLbImparticion());
			pnRespForm_Planif_Activ.add(getLblSesiones());
			pnRespForm_Planif_Activ.add(getLblEspacio_1());
			pnRespForm_Planif_Activ.add(getLblDia());
			pnRespForm_Planif_Activ.add(getTextEspacioSesion());
			pnRespForm_Planif_Activ.add(getDiaSesion());
			pnRespForm_Planif_Activ.add(getMesSesion());
			pnRespForm_Planif_Activ.add(getLblMes_1());
			pnRespForm_Planif_Activ.add(getLblFecha_2());
			pnRespForm_Planif_Activ.add(getLblAo_1());
			pnRespForm_Planif_Activ.add(getAnoSesion());
			pnRespForm_Planif_Activ.add(getBtnAadirSesion());
			pnRespForm_Planif_Activ.add(getScrollPane_5());
			pnRespForm_Planif_Activ.add(getLabel_10());
			pnRespForm_Planif_Activ.add(getLabel_11());
			pnRespForm_Planif_Activ.add(getHoraSesion());
			pnRespForm_Planif_Activ.add(getMinutoSesion());
			pnRespForm_Planif_Activ.add(getLblDuracion_1());
			pnRespForm_Planif_Activ.add(getDuracionSesion());
			pnRespForm_Planif_Activ.add(getBtnBorrarSesion());
			pnRespForm_Planif_Activ.add(getLabel_18());
			pnRespForm_Planif_Activ.add(getLabel_19());
			pnRespForm_Planif_Activ.add(getLabel_20());
		}
		return pnRespForm_Planif_Activ;
	}

	private JLabel getLblNombreDeLa() {
		if (lblNombreDeLa == null) {
			lblNombreDeLa = new JLabel("Nombre de la actividad: ");
			lblNombreDeLa.setBounds(10, 36, 150, 14);
		}
		return lblNombreDeLa;
	}

	private JTextField getTextNombreActividad() {
		if (textNombreActividad == null) {
			textNombreActividad = new JTextField();
			textNombreActividad.setBounds(170, 36, 468, 20);
			textNombreActividad.setColumns(10);
		}
		return textNombreActividad;
	}

	private JLabel getLblObjetivos() {
		if (lblObjetivos == null) {
			lblObjetivos = new JLabel("Objetivos:");
			lblObjetivos.setBounds(10, 61, 150, 14);
		}
		return lblObjetivos;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(170, 67, 468, 60);
			scrollPane.setViewportView(getTextObjetivos());
		}
		return scrollPane;
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(170, 143, 468, 60);
			scrollPane_1.setViewportView(getTextContenidos());
		}
		return scrollPane_1;
	}

	private JLabel getLblContenidos() {
		if (lblContenidos == null) {
			lblContenidos = new JLabel("Contenidos: ");
			lblContenidos.setBounds(10, 148, 150, 14);
		}
		return lblContenidos;
	}

	private JLabel getLblIdDelProfesor() {
		if (lblIdDelProfesor == null) {
			lblIdDelProfesor = new JLabel("Profesores disponibles :");
			lblIdDelProfesor.setBounds(15, 16, 222, 14);
		}
		return lblIdDelProfesor;
	}

	private JLabel getLblRemuneracin() {
		if (lblRemuneracin == null) {
			lblRemuneracin = new JLabel("Remuneracion :");
			lblRemuneracin.setBounds(252, 16, 150, 14);
		}
		return lblRemuneracin;
	}

	private JLabel getLblInscripcinDesde() {
		if (lblInscripcinDesde == null) {
			lblInscripcinDesde = new JLabel("Inscripci\u00F3n desde:");
			lblInscripcinDesde.setBounds(10, 545, 150, 14);
		}
		return lblInscripcinDesde;
	}

	private JLabel getLblHasta() {
		if (lblHasta == null) {
			lblHasta = new JLabel("Inscripcion hasta:");
			lblHasta.setBounds(689, 548, 106, 14);
		}
		return lblHasta;
	}

	private JLabel getLblNmeroDePlazas() {
		if (lblNmeroDePlazas == null) {
			lblNmeroDePlazas = new JLabel("N\u00FAmero de plazas:");
			lblNmeroDePlazas.setBounds(10, 470, 150, 14);
		}
		return lblNmeroDePlazas;
	}

	private JSpinner getNum_plazas() {
		if (num_plazas == null) {
			num_plazas = new JSpinner();
			num_plazas.setModel(new SpinnerNumberModel(1, 1, 1000, 1));
			num_plazas.setBounds(170, 467, 96, 20);
		}
		return num_plazas;
	}

	private JLabel getLblCreacinDeUna() {
		if (lblCreacinDeUna == null) {
			lblCreacinDeUna = new JLabel("Creaci\u00F3n de una actividad");
			lblCreacinDeUna.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblCreacinDeUna.setBounds(7, 11, 315, 14);
		}
		return lblCreacinDeUna;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton("Aceptar");
			btnAceptar.addActionListener(new ActionListener() {
				@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
				public void actionPerformed(ActionEvent arg0) {
					try {					

						// Comprobar nombre
						if (textNombreActividad.getText().isEmpty()) {
							JOptionPane.showMessageDialog(null, "El nombre de la actividad es un campo obligatorio.",
									"Lo sentimos", WARNING_MESSAGE);
						} else {
							if(tableSesiones.getRowCount()<2)
								JOptionPane.showMessageDialog(null, "Una actividad debe tener como m�nimo dos sesiones.",
										"Lo sentimos", WARNING_MESSAGE);
							else {

								//Sacar fecha de fin imparticion y inicio imparticion a partir de las sesiones
								List<String> fechasString=new ArrayList();
								for(int i=0;i<tableSesiones.getRowCount();i++)
									fechasString.add((String)tableSesiones.getValueAt(i, 1));
								//Pasarlo a date
								List<Date>fechas=new ArrayList();
								for(String o:fechasString)
									fechas.add(Dates.fromString(o.replace(" ", "/")));
								//Sacar la mas anterior
								Date impDesde=Dates.fromDdMmYyyy(1, 1, 2200);
								for(Date d:fechas)
									if(d.compareTo(impDesde)<=0)
										impDesde=d;
								//Sacar la mas posterior
								Date impHasta=Dates.fromDdMmYyyy(1, 1, 1800);
								for(Date d:fechas)
									if(d.compareTo(impHasta)>=0)
										impHasta=d;
								// Comprobar fechas
								SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
								Boolean fechasCorrectas = false;
								Date insDesde = objSDF.parse(Integer.parseInt(insDesdeAno.getValue().toString()) + "-"
										+ Integer.parseInt(insDesdeMes.getValue().toString()) + "-"
										+ Integer.parseInt(insDesdeDia.getValue().toString()) + " "
										+ Integer.parseInt(insDesdeHora.getValue().toString()) + ":"
										+ Integer.parseInt(insDesdeMinuto.getValue().toString()));
								Date insHasta = objSDF.parse(Integer.parseInt(insHastaAno.getValue().toString()) + "-"
										+ Integer.parseInt(insHastaMes.getValue().toString()) + "-"
										+ Integer.parseInt(insHastaDia.getValue().toString()) + " "
										+ Integer.parseInt(insHastaHora.getValue().toString()) + ":"
										+ Integer.parseInt(insHastaMinuto.getValue().toString()));
								/*Date impDesde = objSDF.parse(Integer.parseInt(impDesdeAno.getValue().toString()) + "-"
									+ Integer.parseInt(impDesdeMes.getValue().toString()) + "-"
									+ Integer.parseInt(impDesdeDia.getValue().toString()) + " "
									+ Integer.parseInt(insDesdeHora.getValue().toString()) + ":"
									+ Integer.parseInt(insDesdeMinuto.getValue().toString()));*/
								/*Date impHasta = objSDF.parse(Integer.parseInt(impHastaAno.getValue().toString()) + "-"
									+ Integer.parseInt(impHastaMes.getValue().toString()) + "-"
									+ Integer.parseInt(impHastaDia.getValue().toString()) + " "
									+ Integer.parseInt(insHastaHora.getValue().toString()) + ":"
									+ Integer.parseInt(insHastaMinuto.getValue().toString()));*/
								if (insDesde.compareTo(insHasta) >= 0)
									JOptionPane.showMessageDialog(null,
											"La fecha de inicio de inscripcion debe ser anterior a la de fin de inscripcion.",
											"Lo sentimos", WARNING_MESSAGE);
								else {
									if(insHasta.compareTo(impDesde) >= 0) {
										JOptionPane.showMessageDialog(null,
												"La fecha de fin de inscripcion debe ser anterior que la de inicio imparticion. y la de inicio imparticion menor que la de fin de imparticion.",
												"Lo sentimos", WARNING_MESSAGE);
									}
									else {
										if(impDesde.compareTo(impHasta) >= 0) {
											JOptionPane.showMessageDialog(null,
													"La fecha de inicio de imparticion debe ser anterior a la de fin de imparticion.",
													"Lo sentimos", WARNING_MESSAGE);
										}
										else {
											Date today = Dates.today();// hoy a las 00:00:00
											if (insDesde.compareTo(today) < 0)
												JOptionPane.showMessageDialog(null,
														"La fecha de inscripci�n del curso debe ser a partir de hoy a las 00:00.",
														"Lo sentimos", WARNING_MESSAGE);
											else {
												//Solo lo comprobamos si esta seleccionada la pesta�a de profesores
												if (tbImparticion.getSelectedIndex() == 0 && tableProfesores.getSelectedRow() == -1) {
													JOptionPane.showMessageDialog(null, "Seleccione un profesor para continuar",
															"Seleccione un profesor.", WARNING_MESSAGE);
												} else if (comprobarPrecios()) {
													int numFilas=tableProfesoresAnadidos.getRowCount();

													double[] idSdeProfesores=new double[numFilas];
													double[] remProf=new double[numFilas];

													// Si la pesta�a seleccionada es la de profesores sacamos la informacion de la tabla correspondiente
													if(tbImparticion.getSelectedIndex() == 0) {
														//Sacar id's de los profesores
														for(int i=0;i<numFilas;i++) {
															idSdeProfesores[i]=(double)tableProfesoresAnadidos.getValueAt(i, 1);
														}

														//Sacar remuneraciones de los profesores
														for(int i=0;i<numFilas;i++) {
															remProf[i]=(double)tableProfesoresAnadidos.getValueAt(i, 2);
														}
													}
													// Si no, sacamos los profesores de la otra tabla (las remuneraciones son 0)
													else {
														idSdeProfesores = pnEmpresa.getProfesores();
														remProf = new double[idSdeProfesores.length];
													}

													List<Object> sesiones=new ArrayList();
													for(int i=0;i<tableSesiones.getRowCount();i++) {
														Object[] sesion=new Object[3];
														sesion[0]=tableSesiones.getValueAt(i, 0);
														sesion[1]=tableSesiones.getValueAt(i, 1);
														sesion[2]=tableSesiones.getValueAt(i, 2);
														sesiones.add(sesion);
													}

													boolean actualizado = Persistencia.planificarActividad(
															textNombreActividad.getText().toString(), textObjetivos.getText(),
															textContenidos.getText(),

															//Inscripcion desde
															insDesdeAno.getValue().toString(), 
															insDesdeMes.getValue().toString(),
															insDesdeDia.getValue().toString(), 
															insDesdeHora.getValue().toString(),
															insDesdeMinuto.getValue().toString(),

															//Inscripcion hasta
															insHastaAno.getValue().toString(),
															insHastaMes.getValue().toString(),
															insHastaDia.getValue().toString(),
															insHastaHora.getValue().toString(),
															insHastaMinuto.getValue().toString(), 

															//Imparticion desde (sesion 1)
															/*
														impDesdeAno.getValue().toString(),
														impDesdeMes.getValue().toString(),
														impDesdeDia.getValue().toString(),
														impDesdeHora.getValue().toString(),
														impDesdeMinuto.getValue().toString(),
															 */
															Integer.toString((impDesde.getYear()+1900)),
															Integer.toString((impDesde.getMonth()+1)),
															Integer.toString(impDesde.getDate()),
															Integer.toString(impDesde.getHours()),
															Integer.toString(impDesde.getMinutes()),

															//Imparticion hasta (sesion 2)
															/*
														impHastaAno.getValue().toString(),
														impHastaMes.getValue().toString(),
														impHastaDia.getValue().toString(),
														impHastaHora.getValue().toString(),
														impHastaMinuto.getValue().toString(),
															 */
															Integer.toString((impHasta.getYear()+1900)),
															Integer.toString((impHasta.getMonth()+1)),
															Integer.toString(impHasta.getDate()),
															Integer.toString(impHasta.getHours()),
															Integer.toString(impHasta.getMinutes()),

															(int) num_plazas.getValue(), 
															//textEspacio.getText(),
															"",
															(float) pnPrecios.getPrecioGeneral(),
															//Double.parseDouble(tableProfesores.getValueAt(tableProfesores.getSelectedRow(), 1).toString()),
															//Float.parseFloat(spinnerRemuneracion.getValue().toString()),
															//txtrIntroduzcaAContinuacin.getText(),
															//txtrProfesores.getText(),

															//(int)duracion1.getValue(),
															//(int)duracion2.getValue(),
															0,
															0,

															//textEspacio2.getText(),
															"",
															idSdeProfesores,
															remProf,
															sesiones
															);

													guardarPrecios();
													if(tbImparticion.getSelectedIndex() == 1)
														asignarEmpresa();
													if (actualizado) {
														JOptionPane.showMessageDialog(null, "Actividad insertada.",
																"Cambios actualizados con exito.", INFORMATION_MESSAGE);
														inicializar();
													} else {
														JOptionPane.showMessageDialog(null,
																"Ha habido un error, los cambios no se han guardado.",
																"Lo sentimos", WARNING_MESSAGE);
													}
												}
											}
										}
									}
								}
							}
						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Ya hay una actividad con ese nombre. Pruebe a introducir otro distinto.",
								"Lo sentimos", WARNING_MESSAGE);
						e.printStackTrace();
					}
				}
			});
			btnAceptar.setMnemonic('a');
			btnAceptar.setBounds(1162, 633, 89, 23);
		}
		return btnAceptar;
	}

	private JLabel getLblDa() {
		if (lblDa == null) {
			lblDa = new JLabel("D\u00EDa");
			lblDa.setBounds(170, 517, 46, 14);
		}
		return lblDa;
	}

	private JLabel getLblMs() {
		if (lblMs == null) {
			lblMs = new JLabel("M\u00E9s");
			lblMs.setBounds(220, 517, 46, 14);
		}
		return lblMs;
	}

	private JLabel getLblAo() {
		if (lblAo == null) {
			lblAo = new JLabel("A\u00F1o");
			lblAo.setBounds(276, 517, 46, 14);
		}
		return lblAo;
	}

	private JSpinner getInsDesdeDia() {
		if (insDesdeDia == null) {
			insDesdeDia = new JSpinner();
			insDesdeDia.setModel(new SpinnerNumberModel(1, 1, 31, 1));
			insDesdeDia.setBounds(170, 542, 43, 20);
		}
		return insDesdeDia;
	}

	private JSpinner getInsDesdeMes() {
		if (insDesdeMes == null) {
			insDesdeMes = new JSpinner();
			insDesdeMes.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			insDesdeMes.setBounds(223, 542, 43, 20);
		}
		return insDesdeMes;
	}

	private JSpinner getInsDesdeAno() {
		if (insDesdeAno == null) {
			insDesdeAno = new JSpinner();
			insDesdeAno.setModel(new SpinnerNumberModel(2020, 2019, 2030, 1));
			insDesdeAno.setBounds(276, 542, 70, 20);
		}
		return insDesdeAno;
	}

	private JSpinner getInsHastaDia() {
		if (insHastaDia == null) {
			insHastaDia = new JSpinner();
			insHastaDia.setModel(new SpinnerNumberModel(2, 1, 31, 1));
			insHastaDia.setBounds(805, 542, 43, 20);
		}
		return insHastaDia;
	}

	private JSpinner getInsHastaMes() {
		if (insHastaMes == null) {
			insHastaMes = new JSpinner();
			insHastaMes.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			insHastaMes.setBounds(858, 542, 43, 20);
		}
		return insHastaMes;
	}

	private JSpinner getInsHastaAno() {
		if (insHastaAno == null) {
			insHastaAno = new JSpinner();
			insHastaAno.setModel(new SpinnerNumberModel(2020, 2019, 2030, 1));
			insHastaAno.setBounds(911, 542, 70, 20);
		}
		return insHastaAno;
	}

	private JLabel getLblHora() {
		if (lblHora == null) {
			lblHora = new JLabel("Hora");
			lblHora.setBounds(356, 517, 46, 14);
		}
		return lblHora;
	}

	private JLabel getLblMinuto() {
		if (lblMinuto == null) {
			lblMinuto = new JLabel("Minuto");
			lblMinuto.setBounds(411, 517, 46, 14);
		}
		return lblMinuto;
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("Hora");
			label.setBounds(990, 517, 46, 14);
		}
		return label;
	}

	private JLabel getLabel_1() {
		if (label_1 == null) {
			label_1 = new JLabel("Minuto");
			label_1.setBounds(1046, 517, 46, 14);
		}
		return label_1;
	}

	private JSpinner getInsDesdeHora() {
		if (insDesdeHora == null) {
			insDesdeHora = new JSpinner();
			insDesdeHora.setModel(new SpinnerNumberModel(8, 8, 23, 1));
			insDesdeHora.setBounds(356, 542, 45, 20);
		}
		return insDesdeHora;
	}

	private JSpinner getInsDesdeMinuto() {
		if (insDesdeMinuto == null) {
			insDesdeMinuto = new JSpinner();
			insDesdeMinuto.setModel(new SpinnerNumberModel(0, 0, 59, 1));
			insDesdeMinuto.setBounds(412, 542, 45, 20);
		}
		return insDesdeMinuto;
	}

	private JSpinner getInsHastaHora() {
		if (insHastaHora == null) {
			insHastaHora = new JSpinner();
			insHastaHora.setModel(new SpinnerNumberModel(8, 8, 23, 1));
			insHastaHora.setBounds(991, 542, 45, 20);
		}
		return insHastaHora;
	}

	private JSpinner getInsHastaMinuto() {
		if (insHastaMinuto == null) {
			insHastaMinuto = new JSpinner();
			insHastaMinuto.setModel(new SpinnerNumberModel(0, 0, 59, 1));
			insHastaMinuto.setBounds(1047, 542, 45, 20);
		}
		return insHastaMinuto;
	}

	private JSpinner getSpinnerRemuneracion() {
		if (spinnerRemuneracion == null) {
			spinnerRemuneracion = new JSpinner();
			spinnerRemuneracion.setBounds(265, 39, 83, 20);
			spinnerRemuneracion.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		}
		return spinnerRemuneracion;
	}

	private JButton getBtnInscribirse() {
		if (btnInscribirse == null) {
			btnInscribirse = new JButton("Inscribirse en una actividad");
			btnInscribirse.setBounds(482, 91, 419, 29);
			btnInscribirse.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnInscribirse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setTitle("Lista de actividades");
					mostrarInscritas();

					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnListadoActividades");

					try
					{
						cbColectivo.actualizarColectivosActividad(0);

					}
					catch(Exception exp)
					{
						;
					}
				}
			});
		}
		return btnInscribirse;
	}

	private JPanel getPnListadoActividades() {
		if (pnListadoActividades == null) {
			pnListadoActividades = new JPanel();
			pnListadoActividades.setLayout(null);
			pnListadoActividades.add(getBtnNext());

			JLabel lblSeleccioneUnaActividad = new JLabel("Seleccione una actividad:");
			lblSeleccioneUnaActividad.setFont(new Font("Tahoma", Font.BOLD, 25));
			lblSeleccioneUnaActividad.setBounds(361, 16, 442, 20);
			pnListadoActividades.add(lblSeleccioneUnaActividad);
			pnListadoActividades.add(getSPaneAct());

			JPanel pnIncripcion = new JPanel();
			pnIncripcion.setBounds(139, 287, 459, 280);
			pnListadoActividades.add(pnIncripcion);
			pnIncripcion.setLayout(null);

			JLabel lblInscribase = new JLabel("Inscribase:");
			lblInscribase.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblInscribase.setBounds(138, 0, 195, 20);
			pnIncripcion.add(lblInscribase);

			JLabel lblNombre = new JLabel("Nombre:");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblNombre.setBounds(31, 57, 118, 20);
			pnIncripcion.add(lblNombre);

			JLabel lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblApellidos.setBounds(31, 93, 130, 20);
			pnIncripcion.add(lblApellidos);

			JLabel lblEmail = new JLabel("Email:");
			lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblEmail.setBounds(31, 129, 69, 20);
			pnIncripcion.add(lblEmail);

			txtNombre = new JTextField();
			txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
			txtNombre.setBounds(247, 54, 146, 26);
			pnIncripcion.add(txtNombre);
			txtNombre.setColumns(10);

			txtApellidos = new JTextField();
			txtApellidos.setFont(new Font("Tahoma", Font.PLAIN, 20));
			txtApellidos.setBounds(247, 90, 146, 26);
			pnIncripcion.add(txtApellidos);
			txtApellidos.setColumns(10);

			txtEmail = new JTextField();
			txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 20));
			txtEmail.setBounds(247, 126, 146, 26);
			pnIncripcion.add(txtEmail);
			txtEmail.setColumns(10);
			pnIncripcion.add(getLbColectivo());
			pnIncripcion.add(getCbColectivo());
			pnIncripcion.add(getBtnAgregarInscritoA());
			pnIncripcion.add(getBtnEliminarInscrito());
			pnListadoActividades.add(getBtnAtras());
			pnListadoActividades.add(getPnLote());

		}
		return pnListadoActividades;
	}

	private JButton getBtnNext() {
		if (btnFinalizarIns == null) {
			btnFinalizarIns = new JButton("Finalizar Inscripcion");
			btnFinalizarIns.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnFinalizarIns.setBounds(839, 611, 229, 29);
			btnFinalizarIns.setEnabled(false);
			btnFinalizarIns.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					generarReciboInscripcion();
					limpiarInscritos();
					tblActividades.clearSelection();
					tableLote.clearSelection();
					((DefaultTableModel) tableLote.getModel()).setRowCount(0);
					JOptionPane.showMessageDialog(null, "Inscripcion completada con exito", "Excelente",
							INFORMATION_MESSAGE);


					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");

				}

			});
		}
		return btnFinalizarIns;
	}

	public void generarReciboInscripcion() {
		for (ProfesionalDto pro : profesionaleslista) {
			generarReciboInscripcionAgregada(inscripcion, pro);
		}
	}

	public void generarReciboInscripcionAgregada(Inscripcion ins, ProfesionalDto pro) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("recibos/recibosInscripcionesRealizadas/" + pro.nombre + "_inscrita"), "utf-8"))) {
			writer.write("/////RECIBO INCRIPCION REALIZADA/////" + "\n");
			writer.write(pro.nombre + "\n");
			writer.write(ins.getActividad().getPrecio() + " eur " + "\n");
			writer.write(ins.getEstado() + " " + "\n");
			writer.close();

		} catch (IOException ex) {
			System.out.println("Problema al generar recibo de inscripcion");
		}

	}

	private JScrollPane getSPaneAct() {
		if (sPaneAct == null) {
			sPaneAct = new JScrollPane();
			sPaneAct.setBounds(139, 52, 940, 195);
			sPaneAct.setViewportView(getTblActividades());
		}
		return sPaneAct;
	}

	private JTable getTblActividades() {
		if (tblActividades == null) {
			tblActividades = new JTable();
			tblActividades.setFont(new Font("Tahoma", Font.PLAIN, 16));
			tblActividades.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(((DefaultTableModel) tableLote.getModel()).getRowCount()!=0)
					{
						resetInscritos();

					}

					btnAgregarInscritoA.setEnabled(true);
					if (tblActividades.getSelectedRow() != -1) {
						int row = tblActividades.getSelectedRow();
						ActividadDto act = listactividades.get(row);

						actins = new Actividad(act.nombre, act.objetivos, act.contenidos, act.espacio,
								act.inicioInscripcion, act.finInscripcion, act.inicioActividad, act.finActividad,
								act.numPlazas, act.profesor.id, act.id, act.precio, act.sueldoProfesor);

						cbColectivo.actualizarColectivosActividad(act.id);

						if (!actins.comprobarActividad()) {
							JOptionPane.showMessageDialog(null,
									"La actividad seleccionada "
											+ "no esta habilitada para inscribirse. Aforo completo",
											"Lo sentimos", WARNING_MESSAGE);

							parentcard.first(cardLayout);
							return;
						}
						crearInscripcion();
					}
				}
			});

			tblActividades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		}
		return tblActividades;
	}

	private void crearModeloAbiertasInscripcion(List<ActividadDto> actividades) {
		Object[] columnNames = { "Nombre", "Inicio Actividad","Inicio Inscripcion","Fin Inscripcion", "plazas", "Num Inscritos", "Estado", "Precios por colectivos" };
		modeloInscr = new DefaultTableModel(new Object[actividades.size()][7], columnNames);

		listactividades = actividades;

		tblActividades.setModel(modeloInscr);

		tblActividades.setDefaultRenderer(tblActividades.getColumnClass(6), new ListInACell());
		for (int i = 0; i < actividades.size(); i++) {
			int j = 0;
			ActividadDto act = actividades.get(i);
			modeloInscr.setValueAt(act.nombre, i, j++);
			modeloInscr.setValueAt(act.getFechainiact(), i, j++);
			modeloInscr.setValueAt(act.getFechainiins(), i, j++);
			modeloInscr.setValueAt(act.getFechafinins(), i, j++);
			modeloInscr.setValueAt(act.numPlazas, i, j++); 
			modeloInscr.setValueAt(Persistencia.getNumInsMul(act.id), i, j++);



			modeloInscr.setValueAt(act.estado.toString(), i, j++);

			try {
				modeloInscr.setValueAt(new DtoFactory().getPreciosColectivos(act.id), i, j++);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		tblActividades.setModel(modeloInscr);
	}

	private void mostrarInscritas() {
		try {
			List<ActividadDto> ins = new DtoFactory().getActividadesIns();
			crearModeloAbiertasInscripcion(ins);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(((DefaultTableModel) tableLote.getModel()).getRowCount()!=0)
					{
						resetInscritos();

					}
					else
					{
						limpiarInscritos();
					}
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");

				}
			});
			btnAtras.setBounds(158, 611, 115, 29);
		}
		return btnAtras;
	}

	public void resetInscritos()
	{

		limpiarInscritos();
		try {
			new DtoFactory().eliminarInscripcion(idInscripcion);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		tableLote.clearSelection();
		((DefaultTableModel) tableLote.getModel()).setRowCount(0);
	}








	private JButton getBtnCancelar_pnRespForm_Planif_Activ() {
		if (btnCancelar_pnRespForm_Planif_Activ == null) {
			btnCancelar_pnRespForm_Planif_Activ = new JButton("Cancelar");
			btnCancelar_pnRespForm_Planif_Activ.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");
				}
			});
			btnCancelar_pnRespForm_Planif_Activ.setBounds(1063, 633, 89, 23);
		}
		return btnCancelar_pnRespForm_Planif_Activ;
	}

	private JScrollPane getScrollPane_2() {
		if (scrollPane_2 == null) {
			scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(15, 37, 222, 108);
			scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			scrollPane_2.setViewportView(getTableProfesores());
		}
		return scrollPane_2;
	}

	private JTable getTableProfesores() {
		if (tableProfesores == null) {
			String[] nombreColumnas = { "Profesor", "id" };
			modeloTabla = new ModeloNoEditable(nombreColumnas, 0);
			tableProfesores = new JTable(modeloTabla);
		}
		return tableProfesores;
	}

	private JTextArea getTextObjetivos() {
		if (textObjetivos == null) {
			textObjetivos = new JTextArea();
		}
		return textObjetivos;
	}

	private JTextArea getTextContenidos() {
		if (textContenidos == null) {
			textContenidos = new JTextArea();
			textContenidos.setText("");
		}
		return textContenidos;
	}

	private JButton getBtnConsultarIngresosY() {
		if (btnConsultarIngresosY == null) {
			btnConsultarIngresosY = new JButton("Consultar ingresos y gastos");
			btnConsultarIngresosY.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnConsultarIngresosY.setBounds(10, 131, 419, 30);
			btnConsultarIngresosY.setMnemonic('o');
			btnConsultarIngresosY.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentanaBalance();
				}
			});
		}
		return btnConsultarIngresosY;
	}

	private void mostrarVentanaBalance() {
		VentanaBalanceActividades v = new VentanaBalanceActividades(this);
		v.setLocationRelativeTo(this);
		setVisible(false);
		v.setVisible(true);
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividades: ");
			lblActividades.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblActividades.setBounds(10, 54, 211, 25);
		}
		return lblActividades;
	}

	private JLabel getLblInscripciones() {
		if (lblInscripciones == null) {
			lblInscripciones = new JLabel("Inscripciones: ");
			lblInscripciones.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblInscripciones.setBounds(482, 55, 151, 23);
		}
		return lblInscripciones;
	}

	private JButton getBtnConsultarElEstado() {
		if (btnConsultarElEstado == null) {
			btnConsultarElEstado = new JButton("Consultar el estado de una actividad");
			btnConsultarElEstado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentanaActividad();
				}
			});
			btnConsultarElEstado.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnConsultarElEstado.setBounds(10, 223, 419, 29);
		}
		return btnConsultarElEstado;
	}

	private void mostrarVentanaActividad() {
		try {
			List<ActividadDto> a = new DtoFactory().getActividades();
			VentanaActividad frame = new VentanaActividad(a);
			frame.setLocationRelativeTo(this);
			frame.setVisible(true);
		} catch (IOException | SQLException e) {

			mostrarError();
			e.printStackTrace();
		}
	}

	private void mostrarError() {
		JOptionPane.showMessageDialog(this, "Se ha producido un error.", "Error", JOptionPane.ERROR_MESSAGE);
	}

	private JButton getBtnCerrarActividad() {
		if (btnCerrarActividad == null) {
			btnCerrarActividad = new JButton("Cerrar actividad");
			btnCerrarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentanaCerrarActividad();
				}
			});
			btnCerrarActividad.setMnemonic('p');
			btnCerrarActividad.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnCerrarActividad.setBounds(10, 177, 419, 30);
		}
		return btnCerrarActividad;
	}

	public static void mostrarVentanaCerrarActividad() {
		VentanaCerrarActividad frame = new VentanaCerrarActividad();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private JButton getBtnCancelarActividad() {
		if (btnCancelarActividad == null) {
			btnCancelarActividad = new JButton("Cancelar Actividad");
			btnCancelarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarACancelar();
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnCancelarAct");

				}
			});
			btnCancelarActividad.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnCancelarActividad.setBounds(10, 262, 419, 29);
		}
		return btnCancelarActividad;
	}

	private JPanel getPnCancelarActividades() {
		if (pnCancelarActividades == null) {
			pnCancelarActividades = new JPanel();
			pnCancelarActividades.setLayout(null);
			pnCancelarActividades.add(getBtnCancelarActividad_1());
			pnCancelarActividades.add(getLblSeleccioneUnaActividad_1());
			pnCancelarActividades.add(getScrollPane_3());
			pnCancelarActividades.add(getButtonAtraspCA());
			pnCancelarActividades.add(getLblSePuedenCancelar());
		}
		return pnCancelarActividades;
	}

	private JButton getBtnCancelarActividad_1() {
		if (btnCancelarActividad_1 == null) {
			btnCancelarActividad_1 = new JButton("Cancelar Actividad");
			btnCancelarActividad_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					try {
						cancelarActividad();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			});
			btnCancelarActividad_1.setEnabled(false);
			btnCancelarActividad_1.setBounds(799, 579, 196, 29);
		}
		return btnCancelarActividad_1;
	}

	private void cancelarActividad() throws SQLException {
		int res = new DtoFactory().cancelaactividad(canact);

		if (res == 0) {
			JOptionPane.showMessageDialog(null, "Problema base de datos actividad no cancelada.", "Lo sentimos",
					WARNING_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, "Actividad cancelada. Se devuelve el pago 100%.", "Bien", INFORMATION_MESSAGE);
			try {
				generarReciboCancelacionActividad(canact);
			} catch (IOException e) {
				// TODO Auto-generated catch block

				mostrarError();
				e.printStackTrace();
			}

			tableActividadesAcancelar.clearSelection();
			btnCancelarActividad_1.setEnabled(false);

			mostrarACancelar();

		}

	}

	private void generarReciboCancelacionActividad(ActividadDto actividad) throws IOException, SQLException {

		List<ProfesionalDto> profesionales = new DtoFactory().profesionalesInscritos(actividad.id);

		for (ProfesionalDto pro : profesionales) {
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
					"recibos/recibosActividadesCanceladas/" + actividad.id + "_cancelada_" + pro.id)));

			double cantidad_pagada = new DtoFactory().cantidadpagadaProfesionalActividad(actividad.id, pro.id);
			writer.write("/////RECIBO ACTIVIDAD CANCELADA/////" + "\n");
			writer.write("Se ha cancelado la actividad " + "\n" + actividad.nombre
					+ " en la que estaba inscrito a nombre: " + "\n" + pro.nombre + "\n");
			writer.write("Perdonen las molestias" + "\n" + "Se reembolsara el coste que es un TOTAL de: "
					+ cantidad_pagada + "\n");
			writer.close();

		}

		// generar un recibo a todos los inscritos en la actividad y devolver el dinero

	}

	private JLabel getLblSeleccioneUnaActividad_1() {
		if (lblSeleccioneUnaActividad_1 == null) {
			lblSeleccioneUnaActividad_1 = new JLabel("Seleccione una actividad para cancelar:");
			lblSeleccioneUnaActividad_1.setFont(new Font("Tahoma", Font.BOLD, 25));
			lblSeleccioneUnaActividad_1.setBounds(150, 16, 653, 49);
		}
		return lblSeleccioneUnaActividad_1;
	}

	private JScrollPane getScrollPane_3() {
		if (scrollPane_3 == null) {
			scrollPane_3 = new JScrollPane();
			scrollPane_3.setBounds(15, 99, 1231, 447);
			scrollPane_3.setViewportView(getTableActividadesAcancelar());
		}
		return scrollPane_3;
	}

	private JButton getButtonAtraspCA() {
		if (buttonAtraspCA == null) {
			buttonAtraspCA = new JButton("Atras");
			buttonAtraspCA.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					getBtnCancelarActividad_1().setEnabled(false);
					parentcard.first(cardLayout);
				}
			});
			buttonAtraspCA.setBounds(150, 579, 115, 29);
		}
		return buttonAtraspCA;
	}

	private void crearModeloActividadesACancelar(List<ActividadDto> actividades) {
		Object[] columnNames = { "Nombre", "Inicio Actividad","Fin actividad","Inicio Inscripcion","Fin inscripcion","Plazas","Numero de Inscritos", "Estado", "Precio por Colectivos"};


		modeloActCan = new DefaultTableModel(new Object[actividades.size()][4], columnNames);
		tableActividadesAcancelar.setModel(modeloActCan);
		tableActividadesAcancelar.setDefaultRenderer(tableActividadesAcancelar.getColumnClass(0), new ListInACell());

		listactcan = actividades;
		for (int i = 0; i < actividades.size(); i++) {
			int j = 0;
			ActividadDto act = actividades.get(i);
			modeloActCan.setValueAt(act.nombre, i, j++);
			modeloActCan.setValueAt(act.getFechainiact(), i, j++);
			modeloActCan.setValueAt(act.getFechafinact(), i, j++);

			modeloActCan.setValueAt(act.getFechainiins(), i, j++);
			modeloActCan.setValueAt(act.getFechafinins(), i, j++);

			modeloActCan.setValueAt(act.numPlazas, i, j++);
			try {
				modeloActCan.setValueAt(Persistencia.getNumInscritos(
						act.id), i, j++);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			modeloActCan.setValueAt(act.estado.toString(), i, j++);
			try {
				modeloActCan.setValueAt(new DtoFactory().getPreciosColectivos(act.id), i, j++);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		tableActividadesAcancelar.setModel(modeloActCan);
	}

	private void mostrarACancelar() {
		try {
			List<ActividadDto> actcan = new DtoFactory().getActividadesACancelar();
			crearModeloActividadesACancelar(actcan);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private JTable getTableActividadesAcancelar() {
		if (tableActividadesAcancelar == null) {
			tableActividadesAcancelar = new JTable();
			tableActividadesAcancelar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {

					btnCancelarActividad_1.setEnabled(true);
					if (tableActividadesAcancelar.getSelectedRow() != -1) {
						int row = tableActividadesAcancelar.getSelectedRow();
						canact = listactcan.get(row);

					}

				}
			});
		}
		return tableActividadesAcancelar;
	}

	private JPanel getPnLote() {
		if (pnLote == null) {
			pnLote = new JPanel();
			pnLote.setBounds(632, 287, 544, 280);
			pnLote.setLayout(null);
			pnLote.add(getLblInscripcionesEnLote());
			pnLote.add(getSPLote());
		}
		return pnLote;
	}

	private JLabel getLblInscripcionesEnLote() {
		if (lblInscripcionesEnLote == null) {
			lblInscripcionesEnLote = new JLabel("Lista Inscritos:");
			lblInscripcionesEnLote.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblInscripcionesEnLote.setBounds(106, 0, 354, 20);
		}
		return lblInscripcionesEnLote;
	}

	private JScrollPane getSPLote() {
		if (sPLote == null) {
			sPLote = new JScrollPane();
			sPLote.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
				}
			});
			sPLote.setBounds(0, 49, 524, 194);
			sPLote.setViewportView(getTableLote());
		}
		return sPLote;
	}

	private JTable getTableLote() {
		if (tableLote == null) {
			tableLote = new JTable();
			tableLote.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					btnEliminarInscrito.setEnabled(true);
					if (tableLote.getSelectedRow() != -1) {
						int row = tableLote.getSelectedRow();
						caninscrito = listins.get(row);

					}
				}
			});
		}
		return tableLote;
	}

	private JButton getBtnAgregarInscritoA() {
		if (btnAgregarInscritoA == null) {
			btnAgregarInscritoA = new JButton("Agregar Inscrito");
			btnAgregarInscritoA.setEnabled(false);
			btnAgregarInscritoA.setBounds(191, 206, 207, 29);
			btnAgregarInscritoA.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!actins.comprobarActividadLista(idInscripcion)) {
						JOptionPane.showMessageDialog(null,
								"La actividad seleccionada no admite mas inscritos."
										+ " Sin aforo", "Lo sentimos",
										WARNING_MESSAGE);

						return;


					}
					else {
						if(txtNombre.getText().isEmpty() || txtApellidos.getText().isEmpty() || txtEmail.getText().isEmpty())
						{
							JOptionPane.showMessageDialog(null, "Por favor rellene los campos", "Campos obligatorios", JOptionPane.WARNING_MESSAGE);
							return;

						}
						if(inscripcion == null || inscripcion.getActividad() == null)
							JOptionPane.showMessageDialog(null, "Por favor seleccione una actividad.", "Error", JOptionPane.ERROR_MESSAGE);
						else {
							agregarInscrito();
							limpiarInscritos();
							mostrarLote();

						}
					}

				}
			});
			btnAgregarInscritoA.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btnAgregarInscritoA;
	}
	private void limpiarInscritos() {
		txtNombre.setText(null);
		txtApellidos.setText(null);
		txtEmail.setText(null);


	}
	private void agregarInscrito() {
		Profesional profesional = null;
		int id_pro = 0;
		double precio = getPrecioColectivoActividad();
		if (precio == ColectivoDto.SIN_PRECIO_GENERAL) {
			JOptionPane.showMessageDialog(null,					
					"El colectivo seleccionado no es valido para esta actividad.",
					"Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		int colid = 1;
		try {
			colid= cbColectivo.getColectivo().id;
		}
		catch(NullPointerException e)
		{
			;
		}
		try {
			id_pro = Persistencia.getidProfesional();
			profesional = new Profesional(txtNombre.getText().toString(), txtApellidos.getText().toString(),
					txtEmail.getText().toString(), id_pro, colid);
			Persistencia.agregarProfesional(profesional);
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateIngresosEstimado"), precio, inscripcion.getActividad().getId_actividad());
		} catch (SQLException e1 ) {

			JOptionPane.showMessageDialog(null, "Problema al agregar profesional.", "Lo sentimos", WARNING_MESSAGE);
			e1.printStackTrace();
		}


		try {
			Persistencia.agregarListaInscrito(id_pro, idInscripcion, precio);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Problema al agregar inscrito.", "Lo sentimos", WARNING_MESSAGE);
			e.printStackTrace();
		}
	}

	private double getPrecioColectivoActividad() {
		int idColectivo = 0;
		try
		{
			idColectivo = cbColectivo.getColectivo().id;
		}
		catch(Exception e)
		{
			;
		}
		double precio = ColectivoDto.SIN_PRECIO_GENERAL;
		try {
			precio = new DtoFactory().getPrecioColectivoActividad(inscripcion.getActividad().getId_actividad(),
					idColectivo);
			return precio;
		} catch (SQLException |NullPointerException| IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return precio;
	}

	private void crearInscripcion() {

		System.out.print("inscripcion creada!!");
		Date date = new Date();

		inscripcion = new Inscripcion(date, 0, "recibida", actins, null);

		try {
			idInscripcion = Persistencia.getIdInscripcion();

			Persistencia.agregarInscripcion(inscripcion, idInscripcion);
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "Problema al agregar inscripcion.", "Lo sentimos", WARNING_MESSAGE);

			e1.printStackTrace();
		}


	}

	private void mostrarLote() {
		try {
			List<ProfesionalDto> lpro = new DtoFactory().getListaInscripciones(idInscripcion);
			profesionaleslista = lpro;





			crearModeloListaInscripciones(lpro);
			btnFinalizarIns.setEnabled(true);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void crearModeloListaInscripciones(List<ProfesionalDto> lins) {
		Object[] columnNames = { "Nombre", "Apellidos", "Colectivo", "Precio", "Fecha"};

		modeloListIncr = new DefaultTableModel(new Object[lins.size()][5], columnNames);
		listins = lins;
		for (int i = 0; i < lins.size(); i++) {
			int j = 0;
			ProfesionalDto pro = lins.get(i);
			modeloListIncr.setValueAt(pro.nombre, i, j++);
			modeloListIncr.setValueAt(pro.apellidos, i, j++);
			ColectivoDto col = obtenerColectivoProfesional(pro.id);
			String nombreCol = col != null ? col.nombre : "ERROR";
			modeloListIncr.setValueAt(nombreCol, i, j++);

			double precio = obtenerPrecioColectivoActividad(col);
			String strPrecio = precio != -1 ? precio + "" : "ERROR"; 
			modeloListIncr.setValueAt(strPrecio, i, j++);
			modeloListIncr.setValueAt(inscripcion.getFecha_inscripcion(), i, j++);
		}
		tableLote.setModel(modeloListIncr);
	}

	private double obtenerPrecioColectivoActividad(ColectivoDto col) {
		try {
			return new DtoFactory().getPrecioColectivoActividad(inscripcion.getActividad().getId_actividad(), col.id);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error con la base de datos");
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error con la base de datos");
			e.printStackTrace();
		}
		return -1;
	}

	private ColectivoDto obtenerColectivoProfesional(int id) {
		try {
			return new DtoFactory().getColectivoProfesional(id);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error con la base de datos");
			e.printStackTrace();
		}
		return null;
	}

	private PanelPrecios getPnPrecios() {
		if (pnPrecios == null) {
			pnPrecios = new PanelPrecios();
			pnPrecios.setBorder(new TitledBorder(null, "Precios", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnPrecios.setBounds(690, 36, 508, 143);
		}
		return pnPrecios;
	}

	private boolean comprobarPrecios() {
		Map<ColectivoDto, Double> colectivos = pnPrecios.comprobarValidez();
		return colectivos != null;
	}

	private void guardarPrecios() {
		Map<ColectivoDto, Double> colectivos = pnPrecios.comprobarValidez();
		for (ColectivoDto col : colectivos.keySet())
			try {
				new DtoFactory().savePriceColectivo(col, colectivos.get(col));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	private void asignarEmpresa() {
		DatosEmpresaDto empresa = pnEmpresa.getEmpresa();
		double remuneracion = pnEmpresa.getRemuneracion();
		if(empresa != null) {
			try {
				new DtoFactory().asignarEmpresa(empresa.id_empresa, remuneracion);
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Se ha producido un error con la base de datos.",
						"Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	private void monstrarVentanaProfesionals(int id) {
		try {
			List<ActividadDto> a = new DtoFactory().getActividades();
			ActividadDto act = new DtoFactory().getActividadDto(id);
			if (act != null) {
				VentanaProfesionales frame = new VentanaProfesionales(id, a);
				frame.setLocationRelativeTo(this);
				frame.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(this, "No existe ninguna actividad registrada con este ID.",
						"ID inexistente", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (IOException | SQLException e) {
			mostrarError();
			e.printStackTrace();
		}
	}


	private void monstrarVentanaProfesor(int id) {
		try {
			List<ActividadDto> a = new DtoFactory().getActividades();
			ActividadDto act = new DtoFactory().getActividadDto(id);
			if (act != null) {
				VentanaProfesores frame = new VentanaProfesores(id, a,null);
				frame.setLocationRelativeTo(this);
				frame.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(this, "No existe ninguna actividad registrada con este ID.",
						"ID inexistente", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (IOException | SQLException e) {
			mostrarError();
			e.printStackTrace();
		}
	}


	private JButton getBtnPagarIncripcion() {
		if (btnPagarIncripcion == null) {
			btnPagarIncripcion = new JButton("Pagar Inscripcion ");
			btnPagarIncripcion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					monstrarVentanaProfesionals(1);
				}
			});
			btnPagarIncripcion.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnPagarIncripcion.setBounds(482, 132, 419, 29);
		}
		return btnPagarIncripcion;
	}

	private JLabel getLbColectivo() {
		if (lbColectivo == null) {
			lbColectivo = new JLabel("Colectivo:");
			lbColectivo.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lbColectivo.setBounds(31, 165, 118, 20);
		}
		return lbColectivo;
	}

	private ComboBoxColectivos getCbColectivo() {
		if (cbColectivo == null) {
			cbColectivo = new ComboBoxColectivos();
			cbColectivo.setFont(new Font("Tahoma", Font.PLAIN, 20));
			cbColectivo.setSelectedIndex(0);
			cbColectivo.setBounds(247, 164, 146, 26);
		}
		return cbColectivo;
	}

	private class ListInACell extends DefaultTableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {

			JList<String> list = new JList<String>();
			if (value instanceof Map) {
				@SuppressWarnings("unchecked")
				Map<ColectivoDto, Double> map = (Map<ColectivoDto, Double>) value;
				String[] string = new String[map.size()];
				int i = 0;
				for(ColectivoDto col : map.keySet())
					string[i++] = col.nombre + ":	" + map.get(col);
				list.setListData(string);
				list.setVisibleRowCount(string.length);
				table.setRowHeight(row, 20 * (string.length) + 20);
				if(table.getSelectedRow() == row)
					list.setBackground(table.getSelectionBackground());
				else
					list.setBackground(table.getBackground());
			}else 
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			return list;
		}
	}

	private void generarReciboMultIns(InscripcionMultipleDto mulins) {
		List<ProfesionalDto> lpro= new ArrayList<ProfesionalDto>();
		try {
			lpro = new DtoFactory().getListaInscripciones(mulins.id);
		} catch (SQLException e) {
			mostrarError();
			e.printStackTrace();
		}

		for( ProfesionalDto pro : lpro) {
			generarReciboProfesional(pro,mulins);
		}



	}
	private void generarReciboProfesional(ProfesionalDto pro, InscripcionMultipleDto multins) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("recibos/recibosInscripcionesPagadas/" + pro.nombre + "_pagada"), "utf-8"))) {
			writer.write("/////RECIBO INCRIPCION PAGADA/////" + "\n");
			writer.write(pro.nombre + "\n");
			writer.write(multins.preciototal+ " eur " + "\n");
			writer.close();

		} catch (IOException ex) {
			System.out.println("Problema al generar recibo de inscripcion");
		}

	}


	private JPanel getPnPagarEmpresa() {
		if (pnPagarEmpresa == null) {
			pnPagarEmpresa = new JPanel();
			pnPagarEmpresa.setLayout(null);
			pnPagarEmpresa.add(getLblPagarAUna());
			pnPagarEmpresa.add(getBtnAgregarPago());
			pnPagarEmpresa.add(getSpinnerPagoEmpresa());
			pnPagarEmpresa.add(getBtnAtras_1());
			pnPagarEmpresa.add(getCmbActividades());
			pnPagarEmpresa.add(getLblSeleccionaActividad());
			pnPagarEmpresa.add(getLblFechaTransferencia());
			pnPagarEmpresa.add(getSpAno());
			pnPagarEmpresa.add(getSpDia());
			pnPagarEmpresa.add(getLblAno_emp());
			pnPagarEmpresa.add(getLblDia_emp());
			pnPagarEmpresa.add(getSpMes());
			pnPagarEmpresa.add(getLblMes());
			pnPagarEmpresa.add(getLblPagosRealizados());
			pnPagarEmpresa.add(getSPtransEmp());
			pnPagarEmpresa.add(getLblNumeroFactura());
			pnPagarEmpresa.add(getTxtIdFactnueva());
			pnPagarEmpresa.add(getBtnDevolverPago());
			pnPagarEmpresa.add(getPnTipoPagoEmpresa());
			pnPagarEmpresa.add(getLblNombre_2());
			pnPagarEmpresa.add(getLblRemuneracion());
			pnPagarEmpresa.add(getLblCif());
			pnPagarEmpresa.add(getLblFactura());
			pnPagarEmpresa.add(getLblCantidadFactura());
			pnPagarEmpresa.add(getLblFecha_1());
			pnPagarEmpresa.add(getTxtnombreEmp());
			pnPagarEmpresa.add(getTxtremuneracionemp());
			pnPagarEmpresa.add(getTxtnicifemp());
			pnPagarEmpresa.add(getLabel_2());
			pnPagarEmpresa.add(getSpDiafact());
			pnPagarEmpresa.add(getSpmesfact());
			pnPagarEmpresa.add(getSpAnofact());
			pnPagarEmpresa.add(getLabel_7());
			pnPagarEmpresa.add(getLabel_9());
			pnPagarEmpresa.add(getLblIdFactura());
			pnPagarEmpresa.add(getTxtidfactemp());
			pnPagarEmpresa.add(getTxtcantfactemp());
			pnPagarEmpresa.add(getTxtfechafactemp());
			pnPagarEmpresa.add(getBtnModificar());
			pnPagarEmpresa.add(getSpcantidadfact());
			pnPagarEmpresa.add(getLblFechaFactura());
			pnPagarEmpresa.add(getLblAgregarOModificar());
			pnPagarEmpresa.add(getLblCantidadPagada());
			pnPagarEmpresa.add(getTxtcantidadpagadaemp());
		}
		return pnPagarEmpresa;
	}
	private JLabel getLblPagarAUna() {
		if (lblPagarAUna == null) {
			lblPagarAUna = new JLabel("Pagar a una Empresa:");
			lblPagarAUna.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblPagarAUna.setBounds(325, 83, 317, 36);
		}
		return lblPagarAUna;
	}
	private JButton getBtnAgregarPago() {
		if (btnAgregarPago == null) {
			btnAgregarPago = new JButton("Agregar Pago");
			btnAgregarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int pago = (int) spinnerPagoEmpresa.getValue();
					String fact = txtIdFactnueva.getText();
					int ano = (int)spAno.getValue();
					int mes = (int) spMes.getValue();
					int dia = (int) spDia.getValue();

					pagarEmpresa(pago, ano , mes ,dia);
					try {
						int res = new DtoFactory().pagarEmpresa(emp,pago+ emp.pagado_empresa);
					} catch (SQLException e1) {
						mostrarError();
						e1.printStackTrace();
					}

					generarReciboPagoEmpresa(pago,emp);
					JOptionPane.showMessageDialog(null,
							"Pago de valor "+pago+" euros realizado a empresa con exito.",
							"Exito", INFORMATION_MESSAGE);

					mostrarPagosEmpresas(actividadcmb);
					mostrarTranferenciasEmpresa(emp);
				}
			});
			btnAgregarPago.setFont(new Font("Tahoma", Font.BOLD, 20));
			btnAgregarPago.setBounds(54, 304, 182, 29);
		}
		return btnAgregarPago;
	}
	private void generarReciboPagoEmpresa(int pago, EmpresaDto e) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("recibos/recibosEmpresaPago/" + e.nombre + "_pagada"), "utf-8"))) {
			writer.write("/////RECIBO PAGO  EMPRESA/////" + "\n");
			writer.write(e.nombre + "\n");
			writer.write(e.actividad.nombre + "\n");
			writer.write(" PAGADO " + "\n");

			writer.write(pago+ " eur " + "\n");
			writer.write(" FALTA POR PAGAR " + "\n");
			writer.write(e.remuneracion-pago+ " eur " + "\n");

			writer.close();

		} catch (IOException ex) {
			System.out.println("Problema al generar recibo de inscripcion");
		}
	}
	public void pagarEmpresa(int pago,  int ano, int mes, int dia) {
		try {
			int id_trans = new DtoFactory().getid_trans_emp(); 
			int res2 = new DtoFactory().tranferenciaEmpresa(emp,pago,ano,mes,dia, id_trans, pnTipoPagoEmpresa.getTipoPago(), actividadcmb.id);

		} catch (SQLException e) {
			mostrarError();
			e.printStackTrace();
		}

	}
	private JSpinner getSpinnerPagoEmpresa() {
		if (spinnerPagoEmpresa == null) {
			spinnerPagoEmpresa = new JSpinner();
			spinnerPagoEmpresa.setModel(new SpinnerNumberModel(new Integer(1), null, null, new Integer(1)));
			spinnerPagoEmpresa.setFont(new Font("Tahoma", Font.PLAIN, 20));
			spinnerPagoEmpresa.setBounds(267, 323, 81, 49);
		}
		return spinnerPagoEmpresa;
	}

	/*	private void crearModeloPagoEmpresas(List<EmpresaDto> emp) {
		Object[] columnNames = {  "Empresa","Remuneracion", "Cantidad Pagada" };
		modeloEmpr = new DefaultTableModel(new Object[emp.size()][2], columnNames);

		listempresa = emp;

		tpagarempresa.setModel(modeloEmpr);

		tpagarempresa.setDefaultRenderer(tpagarempresa.getColumnClass(2), new ListInACell());
		for (int i = 0; i < emp.size(); i++) {
			int j = 0;
			EmpresaDto e = emp.get(i);
			modeloEmpr.setValueAt(e.nombre, i, j++);
			modeloEmpr.setValueAt(e.remuneracion, i, j++);
			modeloEmpr.setValueAt(e.pagado_empresa, i, j++);



		}
		tpagarempresa.setModel(modeloEmpr);
	}*/

	private void mostrarPagosEmpresas(ActividadDto act) {
		try {
			emp = new DtoFactory().getPagoEmpresas(act);
			if (emp==null)
			{
				/*  JOptionPane.showMessageDialog(null,
							"Actividad sin contenido",
							"Aviso!", INFORMATION_MESSAGE);*/
			}
			else {
				try {
					txtnombreEmp.setText(emp.nombre);
					txtnicifemp.setText(emp.cif);
					txtcantidadpagadaemp.setText(emp.pagado_empresa+"");
					txtremuneracionemp.setText(emp.remuneracion+"");
					txtfechafactemp.setText(emp.factura_fecha.substring(0,10));
					if(emp.factura_cantidad != 0.0 || emp.factura_id!=0)
					{
						txtcantfactemp.setText(emp.factura_cantidad+"");				
						txtidfactemp.setText(emp.factura_id+"");
					}}
				catch(Exception ex) {
					;
				}
			}
			mostrarTranferenciasEmpresa(emp);



		} catch (SQLException e) {
			e.printStackTrace();
		}
	}




	private JLabel getLblEmpresas() {
		if (lblEmpresas == null) {
			lblEmpresas = new JLabel("Empresas:");
			lblEmpresas.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblEmpresas.setBounds(482, 311, 124, 21);
		}
		return lblEmpresas;
	}
	private JButton getBtnPagarEmpresas() {
		if (btnPagarEmpresas == null) {
			btnPagarEmpresas = new JButton("Pagar Empresas");
			btnPagarEmpresas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					crearModeloActividadesCMB();
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPagoEmpresa");

				}
			});
			btnPagarEmpresas.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnPagarEmpresas.setBounds(482, 340, 419, 27);
		}
		return btnPagarEmpresas;
	}
	private JButton getBtnAtras_1() {
		if (btnAtras_1 == null) {
			btnAtras_1 = new JButton("Atras");
			btnAtras_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");
					limpiartransferencias();

				}
			});
			btnAtras_1.setBounds(54, 611, 115, 29);
		}
		return btnAtras_1;
	}
	private void limpiartransferencias()
	{
		txtnombreEmp.setText("");
		txtcantfactemp.setText("");
		txtcantidadpagadaemp.setText("");
		txtfechafactemp.setText("");
		txtnicifemp.setText("");
		txtremuneracionemp.setText("");
		txtidfactemp.setText("");
		spcantidadfact.setValue(1);

		temptrans.clearSelection();
		((DefaultTableModel) temptrans.getModel()).setRowCount(0);

		txtIdFactnueva.setText("");
		spinnerPagoEmpresa.setValue(1);



	}
	private JButton getBtnEliminarInscrito() {
		if (btnEliminarInscrito == null) {
			btnEliminarInscrito = new JButton("Eliminar Inscrito");
			btnEliminarInscrito.setEnabled(false);
			btnEliminarInscrito.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					try {
						new DtoFactory().eliminarIncrito(caninscrito);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					mostrarLote();
					if(		((DefaultTableModel) tableLote.getModel()).getRowCount()==0) {
						btnFinalizarIns.setEnabled(false);

					}

				}
			});
			btnEliminarInscrito.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnEliminarInscrito.setBounds(191, 251, 207, 29);
		}
		return btnEliminarInscrito;
	}
	private JComboBox<ActividadDto> getCmbActividades() {
		if (cmbActividades == null) {
			cmbActividades = new JComboBox<ActividadDto>();
			cmbActividades.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					actividadcmb = (ActividadDto) modeloActividades.getSelectedItem();

					limpiartransferencias();


					mostrarPagosEmpresas(actividadcmb);

				}
			});
			cmbActividades.setFont(new Font("Tahoma", Font.PLAIN, 20));
			cmbActividades.setBounds(31, 31, 954, 36);
		}
		return cmbActividades;
	}
	private JLabel getLblSeleccionaActividad() {
		if (lblSeleccionaActividad == null) {
			lblSeleccionaActividad = new JLabel("Selecciona actividad:");
			lblSeleccionaActividad.setBounds(38, 0, 173, 20);
		}
		return lblSeleccionaActividad;
	}
	private void crearModeloActividadesCMB() {
		List<ActividadDto> listaactividadescmb = null;
		try {
			listaactividadescmb = new  DtoFactory().getActividades();
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ActividadDto[] listact = new ActividadDto[listaactividadescmb.size()];
		for(int i = 0; i < listact.length; i++) {
			listact[i] = listaactividadescmb.get(i);
		}
		modeloActividades = new DefaultComboBoxModel<ActividadDto>(listact);

		cmbActividades.setModel(modeloActividades);
		cmb_act_ins_mul.setModel(modeloActividades);

	}
	private JLabel getLblFechaTransferencia() {
		if (lblFechaTransferencia == null) {
			lblFechaTransferencia = new JLabel("Fecha Transferencia:");
			lblFechaTransferencia.setBounds(579, 323, 146, 20);
		}
		return lblFechaTransferencia;
	}
	private JSpinner getSpAno() {
		if (spAno == null) {
			spAno = new JSpinner();
			spAno.setModel(new SpinnerNumberModel(2019, 2019, 2020, 1));
			spAno.setBounds(750, 320, 69, 26);
		}
		return spAno;
	}
	private JSpinner getSpDia() {
		if (spDia == null) {
			spDia = new JSpinner();
			int day = getToday().get(Calendar.DATE);
			spDia.setModel(new SpinnerNumberModel(day, 1, 31, 1));
			spDia.setBounds(885, 320, 32, 26);
		}
		return spDia;
	}
	private Calendar getToday()
	{
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		//getTime() returns the current date in default time zone
		/* int day = calendar.get(Calendar.DATE);
	      //Note: +1 the month for current month
	      int month = calendar.get(Calendar.MONTH) + 1;
	      int year = calendar.get(Calendar.YEAR);
	      int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
	      int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
	      int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);*/
		return calendar;
	}
	private JLabel getLblAno_emp() {
		if (lblAno_emp == null) {
			lblAno_emp = new JLabel("A\u00F1o:");
			lblAno_emp.setBounds(770, 284, 45, 20);
		}
		return lblAno_emp;
	}
	private JLabel getLblDia_emp() {
		if (lblDia_emp == null) {
			lblDia_emp = new JLabel("Dia:");
			lblDia_emp.setBounds(885, 284, 32, 20);
		}
		return lblDia_emp;
	}
	private JSpinner getSpMes() {
		if (spMes == null) {
			spMes = new JSpinner();
			int mes = getToday().get(Calendar.MONTH)+1;
			spMes.setModel(new SpinnerNumberModel(mes, 1, 12, 1));
			spMes.setBounds(834, 320, 32, 26);
		}
		return spMes;
	}
	private JLabel getLblMes() {
		if (lblMes == null) {
			lblMes = new JLabel("Mes:");
			lblMes.setBounds(830, 284, 69, 20);
		}
		return lblMes;
	}
	private JLabel getLblPagosRealizados() {
		if (lblPagosRealizados == null) {
			lblPagosRealizados = new JLabel("Pagos Realizados:");
			lblPagosRealizados.setBounds(76, 388, 151, 20);
		}
		return lblPagosRealizados;
	}
	private JScrollPane getSPtransEmp() {
		if (sPtransEmp == null) {
			sPtransEmp = new JScrollPane();
			sPtransEmp.setBounds(76, 424, 858, 170);
			sPtransEmp.setViewportView(getTemptrans());
		}
		return sPtransEmp;
	}
	private JTable getTemptrans() {
		if (temptrans == null) {
			temptrans = new JTable();
		}
		return temptrans;
	}

	private void crearModeloTransferenciasEmpresa(List<TransferenciasEmpresaDto> trans) {
		Object[] columnNames = { "Fecha",  "Cantidad Pagada" ,"Movimiento", "Metodo de pago" };
		modeloTrans = new DefaultTableModel(new Object[trans.size()][4], columnNames);

		list_trans_emp = trans;

		temptrans.setModel(modeloTrans);

		for (int i = 0; i < trans.size(); i++) {
			int j = 0;
			TransferenciasEmpresaDto t = trans.get(i);
			modeloTrans.setValueAt(t.fecha_trans, i, j++);
			modeloTrans.setValueAt(t.cantidad_pagada, i, j++);
			modeloTrans.setValueAt(t.movimento, i, j++);

			modeloTrans.setValueAt(t.tipoPago, i, j++);
		}
		temptrans.setModel(modeloTrans);
	}

	private void mostrarTranferenciasEmpresa(EmpresaDto emp) {
		try {
			List<TransferenciasEmpresaDto> trans= new DtoFactory().getTransferenciasEmpresa(emp, actividadcmb.id);
			crearModeloTransferenciasEmpresa(trans);
		} catch (Exception e ) {
			;			
		}
	}
	private void mostrarTranferenciasMulIns() {
		try {
			List<TransferenciaProfesionalDto> trans= new DtoFactory().getTransferenciasMulIns(mulins);
			crearModeloTransferenciasProfesional(trans);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void crearModeloTransferenciasProfesional(List<TransferenciaProfesionalDto> trans) {
		Object[] columnNames = { "Fecha",  "Cantidad Pagada","Movimiento del pago", "Metodo de pago" };
		modeloTransProf = new DefaultTableModel(new Object[trans.size()][3], columnNames);

		list_trans_prof = trans;

		tTransferenciaInscripcion.setModel(modeloTransProf);

		tTransferenciaInscripcion.setDefaultRenderer(tTransferenciaInscripcion.getColumnClass(3), new ListInACell());
		for (int i = 0; i < trans.size(); i++) {
			int j = 0;
			TransferenciaProfesionalDto t = trans.get(i);
			modeloTransProf.setValueAt(t.fecha_transferencia, i, j++);
			modeloTransProf.setValueAt(t.cantidad_pagada, i, j++);
			modeloTransProf.setValueAt(t.movimiento, i, j++);
			modeloTransProf.setValueAt(t.tipoPago, i, j++);
		}
		tTransferenciaInscripcion.setModel(modeloTransProf);
	}
	private JLabel getLblNumeroFactura() {
		if (lblNumeroFactura == null) {
			lblNumeroFactura = new JLabel("Id. factura: ");
			lblNumeroFactura.setBounds(671, 186, 106, 20);
		}
		return lblNumeroFactura;
	}
	private JTextField getTxtIdFactnueva() {
		if (txtIdFactnueva == null) {
			txtIdFactnueva = new JTextField();
			txtIdFactnueva.setBounds(658, 222, 146, 26);
			txtIdFactnueva.setColumns(10);
		}
		return txtIdFactnueva;
	}
	private JButton getBtnDevolverPago() {
		if (btnDevolverPago == null) {
			btnDevolverPago = new JButton("Devolver Pago");
			btnDevolverPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					int pago = (int) spinnerPagoEmpresa.getValue();
					int ano = (int)spAno.getValue();
					int mes = (int) spMes.getValue();
					int dia = (int) spDia.getValue();


					pagarEmpresa(-pago,    ano , mes ,dia);
					try {
						int res = new DtoFactory().pagarEmpresa(emp,emp.pagado_empresa-pago);
					} catch (SQLException e1) {
 						e1.printStackTrace();
					}
					generarReciboPagoEmpresa(-pago,emp);

					JOptionPane.showMessageDialog(null,
							"Devolucion de valor "+pago+" euros realizado a COIIPA con exito.",
							"Exito", INFORMATION_MESSAGE);

					mostrarPagosEmpresas(actividadcmb);
					mostrarTranferenciasEmpresa(emp);
				}
			});
			btnDevolverPago.setFont(new Font("Tahoma", Font.BOLD, 20));
			btnDevolverPago.setBounds(54, 343, 182, 29);
		}
		return btnDevolverPago;
	}
	private JPanel getPnPagarInsMul() {
		if (pnPagarInsMul == null) {
			pnPagarInsMul = new JPanel();
			pnPagarInsMul.setLayout(null);
			pnPagarInsMul.add(getLblpagarInsMul());
			pnPagarInsMul.add(getSpInsMul());
			pnPagarInsMul.add(getBtn_add_ins_mul());
			pnPagarInsMul.add(getSpPrecio_insmul());
			pnPagarInsMul.add(getBtnAtras_insmul());
			pnPagarInsMul.add(getCmb_act_ins_mul());
			pnPagarInsMul.add(getLabel_3());
			pnPagarInsMul.add(getLabel_4());
			pnPagarInsMul.add(getSp_ano_ins_mul());
			pnPagarInsMul.add(getSp_dia_ins_mul());
			pnPagarInsMul.add(getLabel_5());
			pnPagarInsMul.add(getLabel_6());
			pnPagarInsMul.add(getSp_mes_ins_mul());
			pnPagarInsMul.add(getLabel_8());
			pnPagarInsMul.add(getLblpagosinsm());
			pnPagarInsMul.add(getSpTransferencias_ins_mul());
			pnPagarInsMul.add(getBtn_devolver_ins_mul());
			pnPagarInsMul.add(getPnTipoPagoMultiple());
		}
		return pnPagarInsMul;
	}
	private JLabel getLblpagarInsMul() {
		if (lblpagarInsMul == null) {
			lblpagarInsMul = new JLabel("Pagar Inscripcion Multiple:");
			lblpagarInsMul.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblpagarInsMul.setBounds(360, 106, 317, 36);
		}
		return lblpagarInsMul;
	}
	private JScrollPane getSpInsMul() {
		if (spInsMul == null) {
			spInsMul = new JScrollPane();
			spInsMul.setBounds(54, 164, 891, 89);
			spInsMul.setViewportView(getTMulIns());
		}
		return spInsMul;
	}
	private JButton getBtn_add_ins_mul() {
		if (btn_add_ins_mul == null) {
			btn_add_ins_mul = new JButton("Agregar Pago");
			btn_add_ins_mul.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int pago = (int) spPrecio_insmul.getValue();


					int ano = (int)sp_ano_ins_mul.getValue();
					int mes = (int) sp_mes_ins_mul.getValue();
					int dia = (int) sp_dia_ins_mul.getValue();


					String plaza="";

					double cantidadpagada = 0;
					try {
						cantidadpagada= new DtoFactory().getCantidadPagadaInsMul(mulins.id);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 if(mulins.cantidad_a_devolver!=0)
						{
							plaza = "sin plaza";
							JOptionPane.showMessageDialog(null,
									"Pago de valor "+pago+" euros realizado a COIIPA con exito. Sin plaza debido a cancelacion",
									"Exito", INFORMATION_MESSAGE);
						}
					else if(cantidadpagada+ pago >mulins.preciototal)
					{
						plaza="Con plaza";
						JOptionPane.showMessageDialog(null,
								"Pago superior al precio total. Se reserva plaza. Pago realizado de valor "+pago+" euros a COIIPA.",
								"Incidencia!", WARNING_MESSAGE);
					}
					 
					else if(cantidadpagada+ pago < mulins.preciototal  ){
						plaza = "Sin plaza";
						JOptionPane.showMessageDialog(null,
								"Pago inferior al precio total. No se reserva plaza. Pago realizado de valor "+pago+" euros a COIIPA.",
								"Incidencia!", WARNING_MESSAGE);
					}
					else
					{
						plaza="Con plaza";
						JOptionPane.showMessageDialog(null,
								"Pago de valor "+pago+" euros realizado a COIIPA con exito. Con plaza",
								"Exito", INFORMATION_MESSAGE);
					}
					
					pagarInsMult(pago, plaza,  ano , mes ,dia,  mulins);

					mostrarMultipleIns(actividadcmb.id);
					mostrarTranferenciasMulIns();					 
					generarReciboMultIns(mulins);

				}
			});
			btn_add_ins_mul.setFont(new Font("Tahoma", Font.BOLD, 20));
			btn_add_ins_mul.setEnabled(false);
			btn_add_ins_mul.setBounds(54, 288, 182, 29);
		}
		return btn_add_ins_mul;
	}
	private void pagarInsMult(int pago, String plaza, int ano, int mes, int dia, InscripcionMultipleDto mulins) {

		String estado = "incidencia";
		if(mulins.cantidad_pagada+ pago == mulins.preciototal)
		{
			estado = "cobrada";
		}
		if(mulins.cantidad_a_devolver!=0)
		{
			estado = "cancelada";
			try {
				int rescan = new DtoFactory().cambiarDevolver(mulins.id,mulins.cantidad_a_devolver+pago);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try { 

			int id_trans = new DtoFactory().getid_trans_prof(); 
			String tipo = pnTipoPagoMultiple.getTipoPago();

			int res2 = new DtoFactory().tranferenciaInsMul(mulins,pago,ano,mes,dia,00,id_trans, tipo,plaza);
			int res = new DtoFactory().pagarInsMultiple(mulins,pago,estado);

		} catch (SQLException e) {
			mostrarError();
			e.printStackTrace();
		}

	}
	private JSpinner getSpPrecio_insmul() {
		if (spPrecio_insmul == null) {
			spPrecio_insmul = new JSpinner();
			spPrecio_insmul.setFont(new Font("Tahoma", Font.PLAIN, 20));
			spPrecio_insmul.setBounds(260, 288, 81, 49);
		}
		return spPrecio_insmul;
	}
	private JButton getBtnAtras_insmul() {
		if (btnAtras_insmul == null) {
			btnAtras_insmul = new JButton("Atras");
			btnAtras_insmul.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");
					limpiarinsmul();
				}
			});
			btnAtras_insmul.setBounds(54, 611, 115, 29);
		}
		return btnAtras_insmul;
	}
	private void limpiarinsmul() {
		tTransferenciaInscripcion.clearSelection();
		((DefaultTableModel) tTransferenciaInscripcion.getModel()).setRowCount(0);




		tMulIns.clearSelection();
		((DefaultTableModel) tMulIns.getModel()).setRowCount(0);

	}
	private JComboBox<ActividadDto> getCmb_act_ins_mul() {
		if (cmb_act_ins_mul == null) {
			cmb_act_ins_mul = new JComboBox<ActividadDto>();
			cmb_act_ins_mul.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					actividadcmb = (ActividadDto) modeloActividades.getSelectedItem();
					mostrarMultipleIns(actividadcmb.id);
				}
			});
			cmb_act_ins_mul.setFont(new Font("Tahoma", Font.PLAIN, 20));
			cmb_act_ins_mul.setBounds(31, 31, 954, 36);
		}
		return cmb_act_ins_mul;
	}
	private void mostrarMultipleIns(int id) {
		try {
			List<InscripcionMultipleDto>  ins = new DtoFactory().getInsMul(id);
			crearModeloMultipleIns(ins);
		} catch (SQLException e) {
			e.printStackTrace(); 

		}
	}

	private void crearModeloMultipleIns(List<InscripcionMultipleDto> mulins) {
		Object[] columnNames = { "Encargado",   "Numero Inscritos", "Precio Total", "Cantidad Pagada" , "Plaza", "Cantidad a devolver (si es cancelada)"};
		modeloMulIns = new DefaultTableModel(new Object[mulins.size()][5], columnNames);

		listmulins = mulins;




		for (int i = 0; i < mulins.size(); i++) {
			int j = 0;
			InscripcionMultipleDto mul = mulins.get(i);
			modeloMulIns.setValueAt(mul.responsable, i, j++);
			modeloMulIns.setValueAt(mul.numins, i, j++);
			modeloMulIns.setValueAt(mul.preciototal, i, j++);
			modeloMulIns.setValueAt(mul.cantidad_pagada, i, j++);
			modeloMulIns.setValueAt(mul.plaza, i, j++);

			modeloMulIns.setValueAt(mul.cantidad_a_devolver, i, j++);



		}
		if(tMulIns==null)
		{
			tMulIns = new JTable();
		}
		tMulIns.setModel(modeloMulIns);

	}
	private JLabel getLabel_3() {
		if (label_3 == null) {
			label_3 = new JLabel("Selecciona actividad:");
			label_3.setBounds(38, 0, 173, 20);
		}
		return label_3;
	}
	private JLabel getLabel_4() {
		if (label_4 == null) {
			label_4 = new JLabel("Fecha Transferencia:");
			label_4.setBounds(633, 304, 146, 20);
		}
		return label_4;
	}
	private JSpinner getSp_ano_ins_mul() {
		if (sp_ano_ins_mul == null) {
			sp_ano_ins_mul = new JSpinner();
			sp_ano_ins_mul.setModel(new SpinnerNumberModel(2019, 2019, 2020, 1));
			sp_ano_ins_mul.setBounds(812, 301, 69, 26);
		}
		return sp_ano_ins_mul;
	}
	private JSpinner getSp_dia_ins_mul() {
		if (sp_dia_ins_mul == null) {
			sp_dia_ins_mul = new JSpinner();
			int day = getToday().get(Calendar.DATE);
			sp_dia_ins_mul.setModel(new SpinnerNumberModel(day, 1, 31, 1));
			sp_dia_ins_mul.setBounds(976, 301, 32, 26);
		}
		return sp_dia_ins_mul;
	}
	private JLabel getLabel_5() {
		if (label_5 == null) {
			label_5 = new JLabel("A\u00F1o:");
			label_5.setBounds(823, 265, 45, 20);
		}
		return label_5;
	}
	private JLabel getLabel_6() {
		if (label_6 == null) {
			label_6 = new JLabel("Dia:");
			label_6.setBounds(976, 265, 32, 20);
		}
		return label_6;
	}
	private JSpinner getSp_mes_ins_mul() {
		if (sp_mes_ins_mul == null) {
			sp_mes_ins_mul = new JSpinner();
			int mes = getToday().get(Calendar.MONTH)+1;
			sp_mes_ins_mul.setModel(new SpinnerNumberModel(mes, 1, 12, 1));
			sp_mes_ins_mul.setBounds(913, 301, 32, 26);
		}
		return sp_mes_ins_mul;
	}
	private JLabel getLabel_8() {
		if (label_8 == null) {
			label_8 = new JLabel("Mes:");
			label_8.setBounds(896, 265, 69, 20);
		}
		return label_8;
	}
	private JLabel getLblpagosinsm() {
		if (lblpagosinsm == null) {
			lblpagosinsm = new JLabel("Pagos Realizados:");
			lblpagosinsm.setBounds(76, 378, 151, 20);
		}
		return lblpagosinsm;
	}
	private JScrollPane getSpTransferencias_ins_mul() {
		if (spTransferencias_ins_mul == null) {
			spTransferencias_ins_mul = new JScrollPane();
			spTransferencias_ins_mul.setBounds(76, 424, 858, 170);
			spTransferencias_ins_mul.setViewportView(getTTransferenciaInscripcion());
		}
		return spTransferencias_ins_mul;
	}
	private JButton getBtn_devolver_ins_mul() {
		if (btn_devolver_ins_mul == null) {
			btn_devolver_ins_mul = new JButton("Devolver Pago");
			btn_devolver_ins_mul.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int pago = (int) spPrecio_insmul.getValue();

					int ano = (int)sp_ano_ins_mul.getValue();
					int mes = (int) sp_mes_ins_mul.getValue();
					int dia = (int) sp_dia_ins_mul.getValue();

					String plaza = "";


					double cantidadpagada = 0;
					try {
						cantidadpagada= new DtoFactory().getCantidadPagadaInsMul(mulins.id);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 
					if(mulins.cantidad_a_devolver!=0)
						{
							plaza = "sin plaza";
							JOptionPane.showMessageDialog(null,
									"Pago de valor "+pago+" euros realizado a COIIPA con exito. Sin plaza debido a cancelacion",
									"Exito", INFORMATION_MESSAGE);
						}
					else if(cantidadpagada-pago >mulins.preciototal)
					{
						plaza = "Con plaza";
						JOptionPane.showMessageDialog(null,
								"Devolucion inferior a la cantidad sobrante. Se reserva plaza. Devolucion realizada de valor "+pago+" euros a COIIPA.",
								"Incidencia!", WARNING_MESSAGE);
					}
					else if(cantidadpagada- pago < mulins.preciototal  ){



						plaza = "Sin plaza";

						JOptionPane.showMessageDialog(null,
								"Devolucion superior a la cantidad sobrante.Sin reserva de plaza. Devolucion realizada de valor "+pago+" euros a COIIPA.",
								"Incidencia!", WARNING_MESSAGE);
					}
					
					else
					{
						plaza = "Con plaza";
						JOptionPane.showMessageDialog(null,
								"Pago de valor "+pago+" euros realizado a COIIPA con exito. Se reserva plaza.",
								"Exito", INFORMATION_MESSAGE);
					}
					
					pagarInsMult( -pago,plaza, ano , mes ,dia,mulins);
					mostrarMultipleIns(actividadcmb.id);
					mostrarTranferenciasMulIns();					 
					generarReciboMultIns(mulins);

				}

			});
			btn_devolver_ins_mul.setEnabled(false);
			btn_devolver_ins_mul.setFont(new Font("Tahoma", Font.BOLD, 20));
			btn_devolver_ins_mul.setBounds(54, 333, 182, 29);
		}
		return btn_devolver_ins_mul;
	}
	private JButton getBtnPagarInscripcionMultiple() {
		if (btnPagarInscripcionMultiple == null) {
			btnPagarInscripcionMultiple = new JButton("Pagar Inscripcion Multiple");
			btnPagarInscripcionMultiple.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setTitle("Pagar Inscripcion Multiple");

					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnInsMul");
					crearModeloActividadesCMB();
				}
			});
			btnPagarInscripcionMultiple.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnPagarInscripcionMultiple.setBounds(482, 178, 419, 29);
		}
		return btnPagarInscripcionMultiple;
	}
	private JTable getTMulIns() {
		if (tMulIns == null) {
			tMulIns = new JTable();
			tMulIns.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (tMulIns.getSelectedRow() != -1) {
						btn_add_ins_mul.setEnabled(true);
						btn_devolver_ins_mul.setEnabled(true);
						int row = tMulIns.getSelectedRow();
						mulins = listmulins.get(row);

						mostrarTranferenciasMulIns();	

					}
				}
			});
		}
		return tMulIns;
	}
	private JTable getTTransferenciaInscripcion() {
		if (tTransferenciaInscripcion == null) {
			tTransferenciaInscripcion = new JTable();
		}
		return tTransferenciaInscripcion;
	}
	private JPanel getPnRegistrarProfesor() {
		if (pnRegistrarProfesor == null) {
			pnRegistrarProfesor = new JPanel();
			pnRegistrarProfesor.setLayout(null);
			pnRegistrarProfesor.add(getLblRegistrarProfesor());
			pnRegistrarProfesor.add(getLblNombre_1());
			pnRegistrarProfesor.add(getLblApellidos_1());
			pnRegistrarProfesor.add(getLblEmail_1());
			pnRegistrarProfesor.add(getLblTelefonoopcional());
			pnRegistrarProfesor.add(getLblDniopcional());
			pnRegistrarProfesor.add(getTxtNombre_pro());
			pnRegistrarProfesor.add(getTxtapellidos_pro());
			pnRegistrarProfesor.add(getTxtEmail_pro());
			pnRegistrarProfesor.add(getTxttlf_pro());
			pnRegistrarProfesor.add(getTxtdni_pro());
			pnRegistrarProfesor.add(getBtnAtras_pro());
			pnRegistrarProfesor.add(getBtnRegistrarProfesor());
		}
		return pnRegistrarProfesor;
	}
	private JLabel getLblRegistrarProfesor() {
		if (lblRegistrarProfesor == null) {
			lblRegistrarProfesor = new JLabel("Registrar Profesor:");
			lblRegistrarProfesor.setFont(new Font("Tahoma", Font.BOLD, 24));
			lblRegistrarProfesor.setBounds(316, 30, 320, 30);
		}
		return lblRegistrarProfesor;
	}
	private JLabel getLblNombre_1() {
		if (lblNombre_1 == null) {
			lblNombre_1 = new JLabel("Nombre:");
			lblNombre_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblNombre_1.setBounds(157, 145, 127, 20);
		}
		return lblNombre_1;
	}
	private JLabel getLblApellidos_1() {
		if (lblApellidos_1 == null) {
			lblApellidos_1 = new JLabel("Apellidos:");
			lblApellidos_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblApellidos_1.setBounds(157, 189, 127, 30);
		}
		return lblApellidos_1;
	}
	private JLabel getLblEmail_1() {
		if (lblEmail_1 == null) {
			lblEmail_1 = new JLabel("Email:");
			lblEmail_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblEmail_1.setBounds(157, 252, 88, 20);
		}
		return lblEmail_1;
	}
	private JLabel getLblTelefonoopcional() {
		if (lblTelefonoopcional == null) {
			lblTelefonoopcional = new JLabel("Telefono(opcional):");
			lblTelefonoopcional.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblTelefonoopcional.setBounds(157, 310, 175, 30);
		}
		return lblTelefonoopcional;
	}
	private JLabel getLblDniopcional() {
		if (lblDniopcional == null) {
			lblDniopcional = new JLabel("Dni(opcional):");
			lblDniopcional.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblDniopcional.setBounds(157, 380, 175, 30);
		}
		return lblDniopcional;
	}
	private JTextField getTxtNombre_pro() {
		if (txtNombre_pro == null) {
			txtNombre_pro = new JTextField();
			txtNombre_pro.setBounds(362, 144, 146, 26);
			txtNombre_pro.setColumns(10);
		}
		return txtNombre_pro;
	}
	private JTextField getTxtapellidos_pro() {
		if (txtapellidos_pro == null) {
			txtapellidos_pro = new JTextField();
			txtapellidos_pro.setBounds(362, 193, 146, 26);
			txtapellidos_pro.setColumns(10);
		}
		return txtapellidos_pro;
	}
	private JTextField getTxtEmail_pro() {
		if (txtEmail_pro == null) {
			txtEmail_pro = new JTextField();
			txtEmail_pro.setBounds(362, 251, 146, 26);
			txtEmail_pro.setColumns(10);
		}
		return txtEmail_pro;
	}
	private JTextField getTxttlf_pro() {
		if (txttlf_pro == null) {
			txttlf_pro = new JTextField();
			txttlf_pro.setBounds(362, 314, 146, 26);
			txttlf_pro.setColumns(10);
		}
		return txttlf_pro;
	}
	private JTextField getTxtdni_pro() {
		if (txtdni_pro == null) {
			txtdni_pro = new JTextField();
			txtdni_pro.setBounds(362, 384, 146, 26);
			txtdni_pro.setColumns(10);
		}
		return txtdni_pro;
	}
	private JButton getBtnAtras_pro() {
		if (btnAtras_pro == null) {
			btnAtras_pro = new JButton("Atras");
			btnAtras_pro.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					limpiarregistrarprofesor();
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");

				}
			});
			btnAtras_pro.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnAtras_pro.setBounds(27, 600, 115, 29);
		}
		return btnAtras_pro;
	}
	private void limpiarregistrarprofesor()
	{
		txtapellidos_pro.setText("");
		txtNombre_pro.setText("");
		txtdni_pro.setText("");
		txtEmail_pro.setText("");
		txttlf_pro.setText("");
	}
	private JButton getBtnRegistrarProfesor() {
		if (btnRegistrarProfesor == null) {
			btnRegistrarProfesor = new JButton("Registrar Profesor");
			btnRegistrarProfesor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String nombre = txtNombre_pro.getText();
					String apellidos = txtapellidos_pro.getText();
					String email = txtEmail_pro.getText();
					String tlf = txttlf_pro.getText();
					String dni = txtdni_pro.getText();
					if(nombre.isEmpty() || apellidos.isEmpty()|| email.isEmpty()) {
						JOptionPane.showMessageDialog(null,
								"Campos incompletos.",
								"Lo sentimos", WARNING_MESSAGE);
					}
					else {
						try {
							registrarprofesor(nombre,apellidos,email,tlf,dni);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						JOptionPane.showMessageDialog(null, "Profesor registrado.",
								"Cambios actualizados con exito.", INFORMATION_MESSAGE);
						limpiarregistrarprofesor();
					}
				}
			});
			btnRegistrarProfesor.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnRegistrarProfesor.setBounds(454, 600, 229, 29);
		}
		return btnRegistrarProfesor;
	}
	private void registrarprofesor(String nombre, String apellidos, String email, String tlf, String dni) throws SQLException {
		int id_pro = 0;

		id_pro = new DtoFactory().getIdprofesor();
		int res = new DtoFactory().registrarprofesor(id_pro,nombre,apellidos,email,tlf,dni);

	}
	private JPanel getPnRegistrarEmpresa() {
		if (pnRegistrarEmpresa == null) {
			pnRegistrarEmpresa = new JPanel();
			pnRegistrarEmpresa.setLayout(null);
			pnRegistrarEmpresa.add(getLblRegistrarEmpresa());
			pnRegistrarEmpresa.add(getLblNombreEmpresa());
			pnRegistrarEmpresa.add(getLblNombreDelRepresentante());
			pnRegistrarEmpresa.add(getLabel_12());
			pnRegistrarEmpresa.add(getLabel_13());
			pnRegistrarEmpresa.add(getLblCifopcional());
			pnRegistrarEmpresa.add(getTxtnombre_emp());
			pnRegistrarEmpresa.add(getTxtrepresentante_emp());
			pnRegistrarEmpresa.add(getTxtemail_emp());
			pnRegistrarEmpresa.add(getTxttlf_emp());
			pnRegistrarEmpresa.add(getTxtCIF_emp());
			pnRegistrarEmpresa.add(getButton());
			pnRegistrarEmpresa.add(getBtnRegistrarEmpresa());
		}
		return pnRegistrarEmpresa;
	}
	private JLabel getLblRegistrarEmpresa() {
		if (lblRegistrarEmpresa == null) {
			lblRegistrarEmpresa = new JLabel("Registrar Empresa");
			lblRegistrarEmpresa.setFont(new Font("Tahoma", Font.BOLD, 24));
			lblRegistrarEmpresa.setBounds(316, 30, 320, 30);
		}
		return lblRegistrarEmpresa;
	}
	private JLabel getLblNombreEmpresa() {
		if (lblNombreEmpresa == null) {
			lblNombreEmpresa = new JLabel("Nombre Empresa:");
			lblNombreEmpresa.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblNombreEmpresa.setBounds(157, 145, 175, 20);
		}
		return lblNombreEmpresa;
	}
	private JLabel getLblNombreDelRepresentante() {
		if (lblNombreDelRepresentante == null) {
			lblNombreDelRepresentante = new JLabel("Nombre Representante:");
			lblNombreDelRepresentante.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblNombreDelRepresentante.setBounds(157, 189, 213, 30);
		}
		return lblNombreDelRepresentante;
	}
	private JLabel getLabel_12() {
		if (label_12 == null) {
			label_12 = new JLabel("Email:");
			label_12.setFont(new Font("Tahoma", Font.PLAIN, 20));
			label_12.setBounds(157, 252, 88, 20);
		}
		return label_12;
	}
	private JLabel getLabel_13() {
		if (label_13 == null) {
			label_13 = new JLabel("Telefono(opcional):");
			label_13.setFont(new Font("Tahoma", Font.PLAIN, 20));
			label_13.setBounds(157, 310, 175, 30);
		}
		return label_13;
	}
	private JLabel getLblCifopcional() {
		if (lblCifopcional == null) {
			lblCifopcional = new JLabel("CIF(opcional):");
			lblCifopcional.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblCifopcional.setBounds(157, 380, 175, 30);
		}
		return lblCifopcional;
	}
	private JTextField getTxtnombre_emp() {
		if (txtnombre_emp == null) {
			txtnombre_emp = new JTextField();
			txtnombre_emp.setColumns(10);
			txtnombre_emp.setBounds(391, 144, 146, 26);
		}
		return txtnombre_emp;
	}
	private JTextField getTxtrepresentante_emp() {
		if (txtrepresentante_emp == null) {
			txtrepresentante_emp = new JTextField();
			txtrepresentante_emp.setColumns(10);
			txtrepresentante_emp.setBounds(391, 193, 146, 26);
		}
		return txtrepresentante_emp;
	}
	private JTextField getTxtemail_emp() {
		if (txtemail_emp == null) {
			txtemail_emp = new JTextField();
			txtemail_emp.setColumns(10);
			txtemail_emp.setBounds(391, 251, 146, 26);
		}
		return txtemail_emp;
	}
	private JTextField getTxttlf_emp() {
		if (txttlf_emp == null) {
			txttlf_emp = new JTextField();
			txttlf_emp.setColumns(10);
			txttlf_emp.setBounds(391, 314, 146, 26);
		}
		return txttlf_emp;
	}
	private JTextField getTxtCIF_emp() {
		if (txtCIF_emp == null) {
			txtCIF_emp = new JTextField();
			txtCIF_emp.setColumns(10);
			txtCIF_emp.setBounds(391, 384, 146, 26);
		}
		return txtCIF_emp;
	}
	private JButton getButton() {
		if (button == null) {
			button = new JButton("Atras");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					limpiarregistrarEmpresa();
					((CardLayout) cardLayout.getLayout()).show(cardLayout, "pnPrincipal");
				}
			});
			button.setFont(new Font("Tahoma", Font.PLAIN, 20));
			button.setBounds(27, 600, 115, 29);
		}
		return button;
	}
	private void limpiarregistrarEmpresa()
	{
		txtnombre_emp.setText("");
		txtrepresentante_emp.setText("");
		txtCIF_emp.setText("");
		txtemail_emp.setText("");
		txttlf_emp.setText("");
	}
	private JButton getBtnRegistrarEmpresa() {
		if (btnRegistrarEmpresa == null) {
			btnRegistrarEmpresa = new JButton("Registrar Empresa");
			btnRegistrarEmpresa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String nombre_emp = txtnombre_emp.getText();
					String repre = txtrepresentante_emp.getText();
					String email_emp = txtemail_emp.getText();
					String cif = txtCIF_emp.getText();
					String tlf_emp = txttlf_emp.getText();
					if(nombre_emp.isEmpty() || repre.isEmpty()||email_emp.isEmpty())
					{

						JOptionPane.showMessageDialog(null,
								"Campos sin completar.",
								"Lo sentimos", WARNING_MESSAGE);
					}
					else {
						registrarEmpresa(nombre_emp,repre,email_emp,tlf_emp,cif);
						JOptionPane.showMessageDialog(null, "Empresa registrada.",
								"Cambios actualizados con exito.", INFORMATION_MESSAGE);
						limpiarregistrarEmpresa();
					}

				}
			});
			btnRegistrarEmpresa.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btnRegistrarEmpresa.setBounds(454, 600, 229, 29);
		}
		return btnRegistrarEmpresa;
	}
	public void registrarEmpresa(String nombre_emp, String repre, String email_emp, String tlf_emp, String cif) {
		try {
			int id_emp = new DtoFactory().getId_emp();

			int res = new DtoFactory().registrarEmpresa(id_emp,nombre_emp,repre,email_emp,tlf_emp,cif);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private PanelTipoPago getPnTipoPagoEmpresa() {
		if (pnTipoPagoEmpresa == null) {
			pnTipoPagoEmpresa = new PanelTipoPago();
			pnTipoPagoEmpresa.setBounds(363, 298, 182, 83);
		}
		return pnTipoPagoEmpresa;
	}
	private PanelTipoPago getPnTipoPagoMultiple() {
		if (pnTipoPagoMultiple == null) {
			pnTipoPagoMultiple = new PanelTipoPago();
			pnTipoPagoMultiple.setBounds(378, 279, 182, 83);
		}
		return pnTipoPagoMultiple;
	}

	private JButton getBtnAadir() {
		if (btnAadir == null) {
			btnAadir = new JButton("A\u00F1adir");
			btnAadir.setBounds(265, 76, 89, 23);
			btnAadir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					boolean yaEsta=false;
					for(int i=0; i<modeloTablaProfesoresAnadidos.getRowCount();i++) {
						if(modeloTablaProfesoresAnadidos.getValueAt(i, 1)==tableProfesores.getValueAt(tableProfesores.getSelectedRow(), 1))
							yaEsta=true;
					}

					if(yaEsta) {
						JOptionPane.showMessageDialog(null, "El profesor ya esta a�adido",
								"Lo sentimos", WARNING_MESSAGE);
					}
					else {

						if(tableProfesores.getSelectedRow()==-1)
							JOptionPane.showMessageDialog(null, "No hay filas seleccionadas",
									"Lo sentimos", WARNING_MESSAGE);
						else {
							Object[] nuevaFila = new Object[3];// La fila tendra tantos objetos como columnas

							nuevaFila[0]=tableProfesores.getValueAt(tableProfesores.getSelectedRow(), 0);
							nuevaFila[1]=tableProfesores.getValueAt(tableProfesores.getSelectedRow(), 1);
							nuevaFila[2]=spinnerRemuneracion.getValue();

							modeloTablaProfesoresAnadidos.addRow(nuevaFila);
						}
					}
				}
			});
		}
		return btnAadir;
	}
	private JScrollPane getScrollPane_4() {
		if (scrollPane_4 == null) {
			scrollPane_4 = new JScrollPane();
			scrollPane_4.setBounds(392, 37, 222, 108);
			scrollPane_4.setViewportView(getTableProfesoresAnadidos());
		}
		return scrollPane_4;
	}
	private JLabel getLblProfesoresAadidos() {
		if (lblProfesoresAadidos == null) {
			lblProfesoresAadidos = new JLabel("Profesores a\u00F1adidos :");
			lblProfesoresAadidos.setBounds(401, 16, 161, 14);
		}
		return lblProfesoresAadidos;
	}
	private JTable getTableProfesoresAnadidos() {
		if (tableProfesoresAnadidos == null) {
			String[] nombreColumnas = { "Profesor", "id", "Remuneracion" };
			modeloTablaProfesoresAnadidos = new ModeloNoEditable(nombreColumnas, 0);
			tableProfesoresAnadidos = new JTable(modeloTablaProfesoresAnadidos);
		}
		return tableProfesoresAnadidos;
	}

	private JLabel getLblNombre_2() {
		if (lblNombre_2 == null) {
			lblNombre_2 = new JLabel("Nombre:");
			lblNombre_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblNombre_2.setBounds(54, 129, 72, 20);
		}
		return lblNombre_2;
	}
	private JLabel getLblRemuneracion() {
		if (lblRemuneracion == null) {
			lblRemuneracion = new JLabel("Remuneracion:");
			lblRemuneracion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblRemuneracion.setBounds(302, 129, 160, 20);
		}
		return lblRemuneracion;
	}
	private JLabel getLblCif() {
		if (lblCif == null) {
			lblCif = new JLabel("NIF/CIF:");
			lblCif.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblCif.setBounds(579, 129, 69, 20);
		}
		return lblCif;
	}
	private JLabel getLblFactura() {
		if (lblFactura == null) {
			lblFactura = new JLabel("Factura:");
			lblFactura.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblFactura.setBounds(54, 165, 72, 20);
		}
		return lblFactura;
	}
	private JLabel getLblCantidadFactura() {
		if (lblCantidadFactura == null) {
			lblCantidadFactura = new JLabel("cantidad factura:");
			lblCantidadFactura.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblCantidadFactura.setBounds(115, 221, 146, 20);
		}
		return lblCantidadFactura;
	}
	private JLabel getLblFecha_1() {
		if (lblFecha_1 == null) {
			lblFecha_1 = new JLabel("fecha:");
			lblFecha_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblFecha_1.setBounds(119, 252, 62, 20);
		}
		return lblFecha_1;
	}
	private JTextField getTxtnombreEmp() {
		if (txtnombreEmp == null) {
			txtnombreEmp = new JTextField();
			txtnombreEmp.setEditable(false);
			txtnombreEmp.setBounds(141, 127, 146, 26);
			txtnombreEmp.setColumns(10);
		}
		return txtnombreEmp;
	}
	private JTextField getTxtremuneracionemp() {
		if (txtremuneracionemp == null) {
			txtremuneracionemp = new JTextField();
			txtremuneracionemp.setEditable(false);
			txtremuneracionemp.setBounds(477, 127, 92, 26);
			txtremuneracionemp.setColumns(10);
		}
		return txtremuneracionemp;
	}
	private JTextField getTxtnicifemp() {
		if (txtnicifemp == null) {
			txtnicifemp = new JTextField();
			txtnicifemp.setEditable(false);
			txtnicifemp.setBounds(658, 127, 146, 26);
			txtnicifemp.setColumns(10);
		}
		return txtnicifemp;
	}
	private JLabel getLabel_2() {
		if (label_2 == null) {
			label_2 = new JLabel("Dia:");
			label_2.setBounds(1089, 186, 32, 20);
		}
		return label_2;
	}
	private JSpinner getSpDiafact() {
		if (spDiafact == null) {
			spDiafact = new JSpinner();
			int day = getToday().get(Calendar.DATE);
			spDiafact.setModel(new SpinnerNumberModel(day, 1, 31, 1));
			spDiafact.setBounds(1076, 222, 32, 26);
		}
		return spDiafact;
	}
	private JSpinner getSpmesfact() {
		if (spmesfact == null) {
			spmesfact = new JSpinner();
			int mes = getToday().get(Calendar.MONTH)+1;
			spmesfact.setModel(new SpinnerNumberModel(mes, 1, 12, 1));
			spmesfact.setBounds(1029, 222, 32, 26);
		}
		return spmesfact;
	}
	private JSpinner getSpAnofact() {
		if (spAnofact == null) {
			spAnofact = new JSpinner();
			spAnofact.setModel(new SpinnerNumberModel(2019, 2019, 2020, 1));

			spAnofact.setBounds(945, 222, 69, 26);
		}
		return spAnofact;
	}
	private JLabel getLabel_7() {
		if (label_7 == null) {
			label_7 = new JLabel("A\u00F1o:");
			label_7.setBounds(945, 186, 45, 20);
		}
		return label_7;
	}
	private JLabel getLabel_9() {
		if (label_9 == null) {
			label_9 = new JLabel("Mes:");
			label_9.setBounds(1029, 186, 69, 20);
		}
		return label_9;
	}
	private JLabel getLblIdFactura() {
		if (lblIdFactura == null) {
			lblIdFactura = new JLabel("id factura:");
			lblIdFactura.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblIdFactura.setBounds(141, 185, 96, 20);
		}
		return lblIdFactura;
	}
	private JTextField getTxtidfactemp() {
		if (txtidfactemp == null) {
			txtidfactemp = new JTextField();
			txtidfactemp.setEditable(false);
			txtidfactemp.setBounds(268, 182, 146, 26);
			txtidfactemp.setColumns(10);
		}
		return txtidfactemp;
	}
	private JTextField getTxtcantfactemp() {
		if (txtcantfactemp == null) {
			txtcantfactemp = new JTextField();
			txtcantfactemp.setEditable(false);
			txtcantfactemp.setBounds(267, 219, 146, 26);
			txtcantfactemp.setColumns(10);
		}
		return txtcantfactemp;
	}
	private JTextField getTxtfechafactemp() {
		if (txtfechafactemp == null) {
			txtfechafactemp = new JTextField();
			txtfechafactemp.setEditable(false);
			txtfechafactemp.setBounds(268, 250, 146, 26);
			txtfechafactemp.setColumns(10);
		}
		return txtfechafactemp;
	}
	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(txtIdFactnueva.getText().isEmpty())
					{
						JOptionPane.showMessageDialog(null, "El id de la factura es un campo obligatorio.",
								"Lo sentimos", WARNING_MESSAGE);
					}
					else {
						int idfact = 0;
						try {
							idfact = Integer.parseInt(txtIdFactnueva.getText());
						}
						catch(Exception ex){
							JOptionPane.showMessageDialog(null, "id de factura debe ser un numero "
									,
									"Pruebe otra vez", WARNING_MESSAGE);
							return;
						}
						int anofact = (int) spAnofact.getValue();
						int diafact = (int) spDiafact.getValue();
						int mesfact = (int) spmesfact.getValue();
						int cantidad = (int)spcantidadfact.getValue();

						if(cantidad != emp.remuneracion)
						{
							JOptionPane.showMessageDialog(null, "Cantidad de factura diferente "
									+ "a la remuneracion acordada. Se debera modificar",
									"Incidencia", WARNING_MESSAGE);
						}

						if(txtidfactemp.getText().isEmpty())
						{
							agregarFactura(idfact,anofact,mesfact,diafact,cantidad);
							JOptionPane.showMessageDialog(null, "Factura agregada correctamente",
									"Exito", INFORMATION_MESSAGE);
						}
						else
						{
							modificarFactura(idfact,anofact,mesfact,diafact,cantidad);
							JOptionPane.showMessageDialog(null, "Factura modificada correctamente",
									"Exito", INFORMATION_MESSAGE);
						}
						mostrarPagosEmpresas(actividadcmb);
						limpiarFacturas();

					}
				}




			});
			btnModificar.setBounds(430, 218, 115, 29);
		}
		return btnModificar;
	}
	private void limpiarFacturas()
	{
		txtIdFactnueva.setText("");
		spcantidadfact.setValue(1); 
	}
	private void modificarFactura(int idfact, int anofact, int mesfact, int diafact, int cantidad) {
		try {
			int res = new DtoFactory().modificarFactura(idfact,
					anofact, mesfact, diafact, cantidad, emp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void agregarFactura(int idfact, int anofact, int mesfact, int diafact, int cantidad) {
		// TODO Auto-generated method stub
		try {
			int res = new DtoFactory().agregarFactura(idfact,
					anofact, mesfact, diafact, cantidad,actividadcmb,emp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private JSpinner getSpcantidadfact() {
		if (spcantidadfact == null) {
			spcantidadfact = new JSpinner();
			spcantidadfact.setModel(new SpinnerNumberModel(new Integer(1), null, null, new Integer(1)));
			spcantidadfact.setFont(new Font("Tahoma", Font.PLAIN, 20));
			spcantidadfact.setBounds(560, 219, 69, 36);
		}
		return spcantidadfact;
	}
	private JLabel getLblFechaFactura() {
		if (lblFechaFactura == null) {
			lblFechaFactura = new JLabel("Fecha Factura:");
			lblFechaFactura.setBounds(830, 225, 118, 20);
		}
		return lblFechaFactura;
	}
	private JLabel getLblAgregarOModificar() {
		if (lblAgregarOModificar == null) {
			lblAgregarOModificar = new JLabel("agregar/modificar:");
			lblAgregarOModificar.setBounds(429, 180, 136, 32);
		}
		return lblAgregarOModificar;
	}
	private JLabel getLblCantidadPagada() {
		if (lblCantidadPagada == null) {
			lblCantidadPagada = new JLabel("Cantidad Pagada:");
			lblCantidadPagada.setBounds(848, 130, 136, 20);
		}
		return lblCantidadPagada;
	}
	private JTextField getTxtcantidadpagadaemp() {
		if (txtcantidadpagadaemp == null) {
			txtcantidadpagadaemp = new JTextField();
			txtcantidadpagadaemp.setEditable(false);
			txtcantidadpagadaemp.setBounds(999, 130, 146, 26);
			txtcantidadpagadaemp.setColumns(10);
		}
		return txtcantidadpagadaemp;
	}
	private JButton getBtnQuitar() {
		if (btnQuitar == null) {
			btnQuitar = new JButton("Quitar");
			btnQuitar.setBounds(265, 104, 89, 23);
			btnQuitar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(tableProfesoresAnadidos.getSelectedRow()==-1)
						JOptionPane.showMessageDialog(null, "No hay ninguna fila seleccionada",
								"Lo sentimos", WARNING_MESSAGE);
					else
						modeloTablaProfesoresAnadidos.removeRow(tableProfesoresAnadidos.getSelectedRow());
				}
			});
		}
		return btnQuitar;
	}
	private JLabel getLblSePuedenCancelar() {
		if (lblSePuedenCancelar == null) {
			lblSePuedenCancelar = new JLabel("Se cancela una actividad en cualquier momento si no esta cerrada o cancelada. Se devuelve el 100% a los inscritos.");
			lblSePuedenCancelar.setBounds(81, 71, 1019, 20);
		}
		return lblSePuedenCancelar;
	}
	private JTabbedPane getTbImparticion() {
		if (tbImparticion == null) {
			tbImparticion = new JTabbedPane(JTabbedPane.TOP);
			tbImparticion.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					JTabbedPane panel = (JTabbedPane) e.getSource();
					if(panel.getSelectedIndex() == 1)
						pnEmpresa.actualizar();
				}
			});
			tbImparticion.setBounds(42, 241, 635, 185);
			tbImparticion.addTab("Profesores", null, getPnProfesores(), null);
			tbImparticion.addTab("Empresa", null, getPnEmpresa(), null);
		}
		return tbImparticion;
	}
	private JPanel getPnProfesores() {
		if (pnProfesores == null) {
			pnProfesores = new JPanel();
			pnProfesores.setLayout(null);
			pnProfesores.add(getBtnQuitar());
			pnProfesores.add(getLblProfesoresAadidos());
			pnProfesores.add(getScrollPane_4());
			pnProfesores.add(getBtnAadir());
			pnProfesores.add(getSpinnerRemuneracion());
			pnProfesores.add(getLblRemuneracin());
			pnProfesores.add(getScrollPane_2());
			pnProfesores.add(getLblIdDelProfesor());
		}
		return pnProfesores;
	}
	private PanelImparticionEmpresa getPnEmpresa() {
		if (pnEmpresa == null) {
			pnEmpresa = new PanelImparticionEmpresa();
		}
		return pnEmpresa;
	}
	private JLabel getLbImparticion() {
		if (lbImparticion == null) {
			lbImparticion = new JLabel("Imparticion:");
			lbImparticion.setBounds(10, 216, 135, 20);
		}
		return lbImparticion;
	}
	private JLabel getLblSesiones() {
		if (lblSesiones == null) {
			lblSesiones = new JLabel("Sesiones:");
			lblSesiones.setBounds(689, 219, 194, 14);
		}
		return lblSesiones;
	}
	private JLabel getLblEspacio_1() {
		if (lblEspacio_1 == null) {
			lblEspacio_1 = new JLabel("Espacio:");
			lblEspacio_1.setBounds(689, 247, 94, 14);
		}
		return lblEspacio_1;
	}
	private JLabel getLblDia() {
		if (lblDia == null) {
			lblDia = new JLabel("Dia: ");
			lblDia.setBounds(753, 272, 70, 14);
		}
		return lblDia;
	}
	private JTextField getTextEspacioSesion() {
		if (textEspacioSesion == null) {
			textEspacioSesion = new JTextField();
			textEspacioSesion.setBounds(753, 244, 339, 20);
			textEspacioSesion.setColumns(10);
		}
		return textEspacioSesion;
	}
	private JSpinner getDiaSesion() {
		if (diaSesion == null) {
			diaSesion = new JSpinner();
			diaSesion.setModel(new SpinnerNumberModel(3, 1, 31, 1));
			diaSesion.setBounds(753, 297, 43, 20);
		}
		return diaSesion;
	}
	private JSpinner getMesSesion() {
		if (mesSesion == null) {
			mesSesion = new JSpinner();
			mesSesion.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			mesSesion.setBounds(805, 297, 43, 20);
		}
		return mesSesion;
	}
	private JLabel getLblMes_1() {
		if (lblMes_1 == null) {
			lblMes_1 = new JLabel("Mes");
			lblMes_1.setBounds(805, 272, 86, 14);
		}
		return lblMes_1;
	}
	private JLabel getLblFecha_2() {
		if (lblFecha_2 == null) {
			lblFecha_2 = new JLabel("Fecha");
			lblFecha_2.setBounds(689, 300, 83, 14);
		}
		return lblFecha_2;
	}
	private JLabel getLblAo_1() {
		if (lblAo_1 == null) {
			lblAo_1 = new JLabel("A\u00F1o");
			lblAo_1.setBounds(858, 272, 89, 14);
		}
		return lblAo_1;
	}
	private JSpinner getAnoSesion() {
		if (anoSesion == null) {
			anoSesion = new JSpinner();
			anoSesion.setModel(new SpinnerNumberModel(2020, 2019, 2100, 1));
			anoSesion.setBounds(858, 297, 70, 20);
		}
		return anoSesion;
	}
	private JButton getBtnAadirSesion() {
		if (btnAadirSesion == null) {
			btnAadirSesion = new JButton("A\u00F1adir sesion");
			btnAadirSesion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					//Primero comprobar la fecha, no puede ser anterior que la de fin de inscripcion
					String fechaSesionStr=diaSesion.getValue()+"/"+mesSesion.getValue()+"/"+anoSesion.getValue()+" "+horaSesion.getValue()+":"+minutoSesion.getValue();
					Date fechaSesion=Dates.fromString(fechaSesionStr.replace(" ", "/"));

					SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					Date insHasta=new Date();
					try {
						insHasta = objSDF.parse(Integer.parseInt(insHastaAno.getValue().toString()) + "-"
								+ Integer.parseInt(insHastaMes.getValue().toString()) + "-"
								+ Integer.parseInt(insHastaDia.getValue().toString()) + " "
								+ Integer.parseInt(insHastaHora.getValue().toString()) + ":"
								+ Integer.parseInt(insHastaMinuto.getValue().toString()));
					} catch (NumberFormatException | ParseException e) {
						e.printStackTrace();
					}

					if(fechaSesion.compareTo(insHasta)<=0)
						JOptionPane.showMessageDialog(null, "La fecha de una sesion no puede ser anterior a la fecha de fin de inscripcion.",
								"Lo sentimos", WARNING_MESSAGE);
					else {
						Object[] nuevaFila = new Object[3];
						nuevaFila[0]=textEspacioSesion.getText();
						nuevaFila[1]=diaSesion.getValue()+"/"+mesSesion.getValue()+"/"+anoSesion.getValue()+" "+horaSesion.getValue()+":"+minutoSesion.getValue();
						nuevaFila[2]=duracionSesion.getValue();
						modeloTablaSesiones.addRow(nuevaFila);
					}
				}
			});
			btnAadirSesion.setBounds(1101, 294, 150, 23);
		}
		return btnAadirSesion;
	}
	private JScrollPane getScrollPane_5() {
		if (scrollPane_5 == null) {
			scrollPane_5 = new JScrollPane();
			scrollPane_5.setBounds(689, 325, 562, 101);
			scrollPane_5.setViewportView(getTableSesiones());
		}
		return scrollPane_5;
	}
	private JTable getTableSesiones() {
		if (tableSesiones == null) {
			String[] nombreColumnas = { "Espacio", "Fecha y hora", "Duracion" };
			modeloTablaSesiones=new ModeloNoEditable(nombreColumnas, 0);
			tableSesiones = new JTable(modeloTablaSesiones);
		}
		return tableSesiones;
	}
	private JLabel getLabel_10() {
		if (label_10 == null) {
			label_10 = new JLabel("Hora");
			label_10.setBounds(938, 272, 63, 14);
		}
		return label_10;
	}
	private JLabel getLabel_11() {
		if (label_11 == null) {
			label_11 = new JLabel("Minuto");
			label_11.setBounds(990, 272, 63, 14);
		}
		return label_11;
	}
	private JSpinner getHoraSesion() {
		if (horaSesion == null) {
			horaSesion = new JSpinner();
			horaSesion.setModel(new SpinnerNumberModel(9, 0, 23, 1));
			horaSesion.setBounds(938, 297, 45, 20);
		}
		return horaSesion;
	}
	private JSpinner getMinutoSesion() {
		if (minutoSesion == null) {
			minutoSesion = new JSpinner();
			minutoSesion.setModel(new SpinnerNumberModel(0, 0, 59, 1));
			minutoSesion.setBounds(990, 297, 45, 20);
		}
		return minutoSesion;
	}
	private JLabel getLblDuracion_1() {
		if (lblDuracion_1 == null) {
			lblDuracion_1 = new JLabel("Duracion");
			lblDuracion_1.setBounds(1046, 272, 83, 14);
		}
		return lblDuracion_1;
	}
	private JSpinner getDuracionSesion() {
		if (duracionSesion == null) {
			duracionSesion = new JSpinner();
			duracionSesion.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			duracionSesion.setBounds(1046, 297, 45, 20);
		}
		return duracionSesion;
	}
	private JButton getBtnBorrarSesion() {
		if (btnBorrarSesion == null) {
			btnBorrarSesion = new JButton("Borrar sesion");
			btnBorrarSesion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(tableSesiones.getSelectedRow()==-1)
						JOptionPane.showMessageDialog(null, "No hay ninguna fila seleccionada",
								"Lo sentimos", WARNING_MESSAGE);
					else
						modeloTablaSesiones.removeRow(tableSesiones.getSelectedRow());
				}
			});
			btnBorrarSesion.setBounds(1101, 268, 150, 23);
		}
		return btnBorrarSesion;
	}
	private JPanel getPnRetrasar() {
		if (pnRetrasar == null) {
			pnRetrasar = new JPanel();
			pnRetrasar.setLayout(null);

			JLabel lblRetrasarActividad = new JLabel("Retrasar Actividad:");
			lblRetrasarActividad.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblRetrasarActividad.setBounds(155, 24, 521, 20);
			pnRetrasar.add(lblRetrasarActividad);
			pnRetrasar.add(getSPretrasar());
			pnRetrasar.add(getSpDiaRet());
			pnRetrasar.add(getLabel_14());
			pnRetrasar.add(getSpAnoRet());
			pnRetrasar.add(getLabel_15());
			pnRetrasar.add(getLabel_16());
			pnRetrasar.add(getSpMesRet());
			pnRetrasar.add(getLabel_17());
			pnRetrasar.add(getBtnRetrasarActividad());
			pnRetrasar.add(getBtnAtras_ret());
		}
		return pnRetrasar;
	}
	private JScrollPane getSPretrasar() {
		if (sPretrasar == null) {
			sPretrasar = new JScrollPane();
			sPretrasar.setBounds(32, 136, 850, 238);
			sPretrasar.setViewportView(getTretrasar());
		}
		return sPretrasar;
	}
	private JTable getTretrasar() {
		if (tretrasar == null) {
			tretrasar = new JTable();
			tretrasar.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent arg0) {

					btnRetrasarActividad.setEnabled(true);
					if (tretrasar.getSelectedRow() != -1) {
						int row = tretrasar.getSelectedRow();
						retact = listactret.get(row);

					}

				}
			});
		}
		return tretrasar;
	}


	private void crearModeloActividadesARetrasar(List<ActividadDto> actividades) {
		Object[] columnNames = { "Nombre", "Inicio Actividad","Fin actividad","Inicio Inscripcion","Fin inscripcion","Plazas","Numero de Inscritos", "Estado", "Precio por Colectivos"};


		modeloActRet = new DefaultTableModel(new Object[actividades.size()][4], columnNames);
		tretrasar.setModel(modeloActRet);
		tretrasar.setDefaultRenderer(tretrasar.getColumnClass(0), new ListInACell());

		listactret = actividades;
		for (int i = 0; i < actividades.size(); i++) {
			int j = 0;
			ActividadDto act = actividades.get(i);
			modeloActRet.setValueAt(act.nombre, i, j++);
			modeloActRet.setValueAt(act.getFechainiact(), i, j++);
			modeloActRet.setValueAt(act.getFechafinact(), i, j++);

			modeloActRet.setValueAt(act.getFechainiins(), i, j++);
			modeloActRet.setValueAt(act.getFechafinins(), i, j++);

			modeloActRet.setValueAt(act.numPlazas, i, j++);
			try {
				modeloActRet.setValueAt(Persistencia.getNumInscritos(
						act.id), i, j++);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			modeloActRet.setValueAt(act.estado.toString(), i, j++);
			try {
				modeloActRet.setValueAt(new DtoFactory().getPreciosColectivos(act.id), i, j++);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		tretrasar.setModel(modeloActRet);
	}

	private void mostrarARetrasar() {
		try {
			List<ActividadDto> actret = new DtoFactory().getActividadesACancelar();
			crearModeloActividadesARetrasar(actret);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private JSpinner getSpDiaRet() {
		if (spDiaRet == null) {
			spDiaRet = new JSpinner();
			int day = getToday().get(Calendar.DATE);
			spDiaRet.setModel(new SpinnerNumberModel(day, 1, 31, 1));
			spDiaRet.setBounds(685, 479, 32, 26);
		}
		return spDiaRet;
	}
	private JLabel getLabel_14() {
		if (label_14 == null) {
			label_14 = new JLabel("Nueva Fecha de Inicio:");
			label_14.setBounds(398, 485, 151, 14);
		}
		return label_14;
	}
	private JSpinner getSpAnoRet() {
		if (spAnoRet == null) {
			spAnoRet = new JSpinner();
			spAnoRet.setModel(new SpinnerNumberModel(2019, 2019, 2100, 1));

			spAnoRet.setBounds(550, 479, 69, 26);
		}
		return spAnoRet;
	}
	private JLabel getLabel_15() {
		if (label_15 == null) {
			label_15 = new JLabel("A\u00F1o:");
			label_15.setBounds(570, 443, 45, 20);
		}
		return label_15;
	}
	private JLabel getLabel_16() {
		if (label_16 == null) {
			label_16 = new JLabel("Mes:");
			label_16.setBounds(630, 443, 69, 20);
		}
		return label_16;
	}
	private JSpinner getSpMesRet() {
		if (spMesRet == null) {
			spMesRet = new JSpinner();

			int mes = getToday().get(Calendar.MONTH)+1;
			spMesRet.setModel(new SpinnerNumberModel(mes, 1, 12, 1));
			spMesRet.setBounds(634, 479, 32, 26);
		}
		return spMesRet;
	}
	private JLabel getLabel_17() {
		if (label_17 == null) {
			label_17 = new JLabel("Dia:");
			label_17.setBounds(685, 443, 32, 20);
		}
		return label_17;
	}
	private JButton getBtnRetrasarActividad() {
		if (btnRetrasarActividad == null) {
			btnRetrasarActividad = new JButton("Retrasar Actividad");
			btnRetrasarActividad.setEnabled(false);
			btnRetrasarActividad.addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent e) {
					int ano = (int) spAnoRet.getValue();
					int mes = (int) spMesRet.getValue();
					int dia = (int) spDiaRet.getValue();

					Timestamp ts = new Timestamp(ano, mes, dia, 00, 00, 00, 0);
					Date d2 = new Date(ts.getTime());
					Date d1 = new Date(retact.getTimeStampiniact().getTime());

					if( d1.after(d2))
					{
						JOptionPane.showMessageDialog(null, "Fecha a modificar antes que la indicada. Cambiela", "Cambie fecha", INFORMATION_MESSAGE);
						return;
					}
					try {
						retrasarActividad(ano,mes,dia);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			btnRetrasarActividad.setFont(new Font("Tahoma", Font.BOLD, 14));
			btnRetrasarActividad.setBounds(183, 481, 175, 23);
		}
		return btnRetrasarActividad;
	}
	public void retrasarActividad(int ano, int mes, int dia) throws SQLException {
		int res = new DtoFactory().retrasaractividad(retact, ano, mes ,dia);

		if (res == 0) {
			JOptionPane.showMessageDialog(null, "Problema base de datos actividad no retrasada.", "Lo sentimos",
					WARNING_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, "Actividad retrasada. Se devuelve el pago  a quien lo desea a 100%.", "Bien", INFORMATION_MESSAGE);
			tretrasar.clearSelection();
			btnRetrasarActividad.setEnabled(false);

			mostrarARetrasar();

		}
	}
	private JButton getBtnAtras_ret() {
		if (btnAtras_ret == null) {
			btnAtras_ret = new JButton("Atras");
			btnAtras_ret.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					getBtnRetrasarActividad().setEnabled(false);
					parentcard.first(cardLayout);
				}
			});
			btnAtras_ret.setBounds(40, 620, 89, 23);
		}
		return btnAtras_ret;
	}
	private JButton getBotonPagarProfesores() {
		if (botonPagarProfesores == null) {
			botonPagarProfesores = new JButton("Pagar Profesores");
			botonPagarProfesores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					monstrarVentanaProfesor(1);
				}
			});
			botonPagarProfesores.setFont(new Font("Tahoma", Font.PLAIN, 20));
			botonPagarProfesores.setBounds(482, 230, 419, 29);
		}
		return botonPagarProfesores;
	}
	private JLabel getLabel_18() {
		if (label_18 == null) {
			label_18 = new JLabel("D\u00EDa");
			label_18.setBounds(805, 517, 46, 14);
		}
		return label_18;
	}
	private JLabel getLabel_19() {
		if (label_19 == null) {
			label_19 = new JLabel("M\u00E9s");
			label_19.setBounds(858, 517, 46, 14);
		}
		return label_19;
	}
	private JLabel getLabel_20() {
		if (label_20 == null) {
			label_20 = new JLabel("A\u00F1o");
			label_20.setBounds(911, 517, 46, 14);
		}
		return label_20;
	}
}
