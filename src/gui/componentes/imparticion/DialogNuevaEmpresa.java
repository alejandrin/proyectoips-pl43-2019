package gui.componentes.imparticion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import util.DtoFactory;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class DialogNuevaEmpresa extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txNombre;
	private JTextField txRepresentante;
	private JTextField txEmail;
	private JTextField txTelefono;
	private JTextField txCIF;
	private PanelImparticionEmpresa ventanaAnterior;

	public DialogNuevaEmpresa(PanelImparticionEmpresa panelImparticionEmpresa) {
		ventanaAnterior = panelImparticionEmpresa;
		
		setTitle("Registrar nueva empresa");
		setBounds(100, 100, 391, 261);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txCIF = new JTextField();
			txCIF.setBounds(183, 134, 157, 26);
			contentPanel.add(txCIF);
			txCIF.setColumns(10);
		}
		{
			txTelefono = new JTextField();
			txTelefono.setBounds(183, 102, 157, 26);
			contentPanel.add(txTelefono);
			txTelefono.setColumns(10);
		}
		{
			txEmail = new JTextField();
			txEmail.setBounds(183, 70, 157, 26);
			contentPanel.add(txEmail);
			txEmail.setColumns(10);
		}
		{
			txRepresentante = new JTextField();
			txRepresentante.setBounds(183, 38, 157, 26);
			contentPanel.add(txRepresentante);
			txRepresentante.setColumns(10);
		}
		{
			txNombre = new JTextField();
			txNombre.setBounds(183, 6, 157, 26);
			contentPanel.add(txNombre);
			txNombre.setColumns(10);
		}
		{
			JLabel lbCif = new JLabel("CIF (opcional):");
			lbCif.setBounds(15, 134, 157, 26);
			contentPanel.add(lbCif);
		}
		{
			JLabel lblTelefonoopcional = new JLabel("Telefono (opcional):");
			lblTelefonoopcional.setBounds(15, 102, 157, 26);
			contentPanel.add(lblTelefonoopcional);
		}
		{
			JLabel lbEmail = new JLabel("Email:");
			lbEmail.setBounds(15, 70, 157, 26);
			contentPanel.add(lbEmail);
		}
		{
			JLabel lbRepresentante = new JLabel("Representante");
			lbRepresentante.setBounds(15, 38, 157, 26);
			contentPanel.add(lbRepresentante);
		}
		{
			JLabel lbNombre = new JLabel("Nombre:");
			lbNombre.setBounds(15, 6, 157, 26);
			contentPanel.add(lbNombre);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String nombre_emp = txNombre.getText();
						String repre = txRepresentante.getText();
						String email_emp = txEmail.getText();
						String cif = txCIF.getText();
						String tlf_emp = txTelefono.getText();
						if(nombre_emp.isEmpty() || repre.isEmpty()||email_emp.isEmpty())
						{

							JOptionPane.showMessageDialog(null,
									"Campos sin completar.",
									"Lo sentimos", JOptionPane.WARNING_MESSAGE);
						}
						else {
							registrarEmpresa(nombre_emp,repre,email_emp,tlf_emp,cif);
							JOptionPane.showMessageDialog(null, "Empresa registrada.",
									"Cambios actualizados con exito.", JOptionPane.INFORMATION_MESSAGE);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	public void registrarEmpresa(String nombre_emp, String repre, String email_emp, String tlf_emp, String cif) {
		try {
			int id_emp = new DtoFactory().getId_emp();

			new DtoFactory().registrarEmpresa(id_emp,nombre_emp,repre,email_emp,tlf_emp,cif);
			
			ventanaAnterior.aņadirEmpresa(id_emp);
			
			this.dispose();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error en la base de datos.",
					"Error", JOptionPane.ERROR_MESSAGE);
		}

	}

}
