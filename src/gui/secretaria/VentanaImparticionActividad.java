package gui.secretaria;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import logica.dto.DatosEmpresaDto;
import logica.dto.ProfesorDto;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class VentanaImparticionActividad extends JDialog {

	private static final long serialVersionUID = 1L;

	private List<ProfesorDto> profesores;
	private DatosEmpresaDto empresa;
	
	private JPanel pnPrincipal;
	private JPanel pnEmpresa;
	private JPanel pnNombre;
	private JPanel pnRepresentante;
	private JPanel pnCIF;
	private JLabel lbNombre;
	private JTextField txNombre;
	private JLabel lbRepresentante;
	private JTextField txRepresentante;
	private JLabel lbCIF;
	private JTextField txCIF;
	private JPanel pnEmail;
	private JPanel pnTelefono;
	private JLabel lbEmail;
	private JTextField txEmail;
	private JLabel lblTelfono;
	private JTextField txTelefono;
	private JScrollPane scrProfesores;
	private JTable tbProfesores;

	
	/**
	 * Create the frame.
	 */
	public VentanaImparticionActividad(List<ProfesorDto> prof, DatosEmpresaDto empresa) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaImparticionActividad.class.getResource("/img/logo-COIIPA.png")));
		setModal(true);
		profesores = prof;
		this.empresa = empresa;
		
		setTitle("Datos de imparticion");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 638, 453);
		pnPrincipal = new JPanel();
		pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnPrincipal);
		if(empresa != null)
			pnPrincipal.add(getPnEmpresa(), BorderLayout.NORTH);
		pnPrincipal.setLayout(new GridLayout(2, 1, 0, 0));
		pnPrincipal.add(getScrProfesores());
		
		aņadirProfesores();
	}

	private void aņadirProfesores() {
		String[] columns = new String[] {"Nombre", "Apellidos", "Email", "Telefono", "DNI"};
		
		DefaultTableModel modeloProfesores = new DefaultTableModel(new Object[profesores.size()][columns.length],
				columns);
		
		for(int i=0; i<profesores.size(); i++) {
			ProfesorDto profe = profesores.get(i);
			int j = 0;
			
			modeloProfesores.setValueAt(profe.nombre, i, j++);
			modeloProfesores.setValueAt(profe.apellidos, i, j++);
			modeloProfesores.setValueAt(profe.email, i, j++);
			
			String tel = profe.telefono == null ? "Desconocido" : profe.telefono;
			modeloProfesores.setValueAt(tel, i, j++);
			
			String dni = profe.dni == null ? "Desconocido" : profe.dni;
			modeloProfesores.setValueAt(dni, i, j++);
		}
		
		tbProfesores.setModel(modeloProfesores);
	}
	
	private JPanel getPnEmpresa() {
		if (pnEmpresa == null) {
			pnEmpresa = new JPanel();
			pnEmpresa.setBorder(new TitledBorder(null, "Empresa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnEmpresa.add(getPnNombre());
			pnEmpresa.add(getPnRepresentante());
			pnEmpresa.add(getPnTelefono());
			pnEmpresa.add(getPnEmail());
			pnEmpresa.add(getPnCIF());
		}
		return pnEmpresa;
	}
	private JPanel getPnNombre() {
		if (pnNombre == null) {
			pnNombre = new JPanel();
			pnNombre.add(getLbNombre());
			pnNombre.add(getTxNombre());
		}
		return pnNombre;
	}
	private JPanel getPnRepresentante() {
		if (pnRepresentante == null) {
			pnRepresentante = new JPanel();
			pnRepresentante.add(getLbRepresentante());
			pnRepresentante.add(getTxRepresentante());
		}
		return pnRepresentante;
	}
	private JPanel getPnCIF() {
		if (pnCIF == null) {
			pnCIF = new JPanel();
			pnCIF.add(getLbCIF());
			pnCIF.add(getTxCIF());
		}
		return pnCIF;
	}
	private JLabel getLbNombre() {
		if (lbNombre == null) {
			lbNombre = new JLabel("Nombre: ");
			lbNombre.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbNombre;
	}
	private JTextField getTxNombre() {
		if (txNombre == null) {
			txNombre = new JTextField(empresa.nombre);
			txNombre.setEditable(false);
			txNombre.setHorizontalAlignment(SwingConstants.CENTER);
			txNombre.setColumns(10);
		}
		return txNombre;
	}
	private JLabel getLbRepresentante() {
		if (lbRepresentante == null) {
			lbRepresentante = new JLabel("Representante:");
			lbRepresentante.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbRepresentante;
	}
	private JTextField getTxRepresentante() {
		if (txRepresentante == null) {
			txRepresentante = new JTextField(empresa.representante);
			txRepresentante.setEditable(false);
			txRepresentante.setHorizontalAlignment(SwingConstants.CENTER);
			txRepresentante.setColumns(10);
		}
		return txRepresentante;
	}
	private JLabel getLbCIF() {
		if (lbCIF == null) {
			lbCIF = new JLabel("CIF:");
			lbCIF.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbCIF;
	}
	private JTextField getTxCIF() {
		if (txCIF == null) {
			txCIF = new JTextField();
			if(empresa.cif != null)
				txCIF.setText(empresa.cif);
			txCIF.setEditable(false);
			txCIF.setHorizontalAlignment(SwingConstants.CENTER);
			txCIF.setColumns(10);
		}
		return txCIF;
	}
	private JPanel getPnEmail() {
		if (pnEmail == null) {
			pnEmail = new JPanel();
			pnEmail.add(getLbEmail());
			pnEmail.add(getTxEmail());
		}
		return pnEmail;
	}
	private JPanel getPnTelefono() {
		if (pnTelefono == null) {
			pnTelefono = new JPanel();
			pnTelefono.add(getLblTelfono());
			pnTelefono.add(getTxTelefono());
		}
		return pnTelefono;
	}
	private JLabel getLbEmail() {
		if (lbEmail == null) {
			lbEmail = new JLabel("E-mail");
		}
		return lbEmail;
	}
	private JTextField getTxEmail() {
		if (txEmail == null) {
			txEmail = new JTextField(empresa.email);
			txEmail.setHorizontalAlignment(SwingConstants.CENTER);
			txEmail.setEditable(false);
			txEmail.setColumns(15);
		}
		return txEmail;
	}
	private JLabel getLblTelfono() {
		if (lblTelfono == null) {
			lblTelfono = new JLabel("Tel\u00E9fono: ");
		}
		return lblTelfono;
	}
	private JTextField getTxTelefono() {
		if (txTelefono == null) {
			txTelefono = new JTextField();
			if(empresa.telefono != null)
				txTelefono.setText(empresa.telefono);
			txTelefono.setHorizontalAlignment(SwingConstants.CENTER);
			txTelefono.setEditable(false);
			txTelefono.setColumns(10);
		}
		return txTelefono;
	}
	private JScrollPane getScrProfesores() {
		if (scrProfesores == null) {
			scrProfesores = new JScrollPane();
			scrProfesores.setViewportView(getTbProfesores());
		}
		return scrProfesores;
	}
	private JTable getTbProfesores() {
		if (tbProfesores == null) {
			tbProfesores = new JTable();
		}
		return tbProfesores;
	}
}
