package gui.secretaria;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import java.awt.GridLayout;
import java.awt.font.TextAttribute;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import logica.EstadoInscripcion;
import logica.dto.ActividadDto;
import logica.dto.ColectivoDto;
import logica.dto.DatosEmpresaDto;
import logica.dto.InscripcionDto;
import logica.dto.ProfesorDto;
import util.DtoFactory;

import javax.swing.JTextField;
import java.awt.Font;

import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.BoxLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.FlowLayout;

public class VentanaActividad extends JFrame {

	private static final long serialVersionUID = 1L;

	private CustomRenderer cr;
	private ActividadDto actividad;
	private TableModel modelo;
	private DefaultComboBoxModel<ActividadDto> modeloActividades;
	private List<ActividadDto> actividades = new ArrayList<ActividadDto>();

	private JPanel pnActividad;
	private JPanel pnSuperior;
	private JPanel pnDatosBasicos;
	private JPanel pnEstado;
	private JPanel pnDatos;
	private JLabel lbEstado;
	private JLabel lblLugar;
	private JLabel lblProfesor;
	private JLabel lblSueldo;
	private JPanel pnFechas;
	private JPanel pnInscripcion;
	private JPanel pnCurso;
	private JPanel pnInicioInscripcion;
	private JLabel lbInicioInscripcion;
	private JTextField txInicioInscripcion;
	private JPanel pnFinInscripcion;
	private JLabel lbFinInscripcion;
	private JPanel pnInicioCurso;
	private JLabel lbFechaCurso;
	private JTextField txFechaCurso;
	private JPanel pnFinCurso;
	private JTextField txFinInscripcion;
	private JLabel lbHoraCurso;
	private JTextField txHora;
	private JTable tbInscripciones;
	private JPanel pnLugar;
	private JTextField txLugar;
	private JPanel pnProfesor;
	private JPanel pnSueldo;
	private JTextField txSueldo;
	private JScrollPane scrTable;
	private JButton btImparticion;
	private JPanel pnPrincipal;
	private JPanel pnSeleccion;
	private JLabel lblActividad;
	private JComboBox<ActividadDto> cbActividad;
	private JPanel pnInscritos;
	private JPanel pnInfoInscritos;
	private JLabel lbNInscritos;
	private JTextField txNInscritos;
	private JLabel lbPlazasDisponibles;
	private JTextField txPlazas;
	private JPanel panel;
	private JPanel pnPrecios;
	private Map<ColectivoDto, Double> precios;
	private JScrollPane scrPrecios;
	private JPanel pnCentro;
	private JButton btnCancelarInscripcion;

	private int inscritos = 0;
	private List<InscripcionDto> listinscripciones;
	private InscripcionDto canins;
	private JPanel pnAtras;
	private JButton btnAtras;
	private JLabel lblSinPlaza;
	private JTextField txSinPlaza;

	/**
	 * Create the frame.
	 */
	public VentanaActividad(List<ActividadDto> actividades) {
		cr = new CustomRenderer();
		this.setExtendedState(MAXIMIZED_BOTH);

		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaActividad.class.getResource("/img/logo-COIIPA.png")));
		setForeground(UIManager.getColor("FormattedTextField.selectionBackground"));
		this.actividades = actividades;
		crearModeloActividades();

		setTitle("Datos de la actividad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1104, 684);
		pnPrincipal = getPnPrincipal();
		actividad = actividades.get(0);
		mostrarDatosActividad();
	}

	private void crearModeloActividades() {
		ActividadDto[] a = new ActividadDto[actividades.size()];
		modeloActividades = new DefaultComboBoxModel<ActividadDto>(actividades.toArray(a));

	}

	private void añadirFilas(List<InscripcionDto> inscripciones) {
		inscritos = 0;
		for (int i = 0; i < inscripciones.size(); i++) {
			int j = 0;
			InscripcionDto ins = inscripciones.get(i);
			modelo.setValueAt(ins.profesional.nombre, i, j++);
			modelo.setValueAt(ins.fechaInscripcion.toString(), i, j++);
			modelo.setValueAt(ins.cantidadPagada, i, j++);
			modelo.setValueAt(ins.precio, i, j++);
			if (ins.estado.equals(EstadoInscripcion.INCIDENCIA))
				modelo.setValueAt((ins.cantidadPagada - ins.precio) + "", i, j++);
			else
				modelo.setValueAt("No hay incidencia.", i, j++);
			modelo.setValueAt(ins.estado.toString(), i, j++);
			modelo.setValueAt(ins.plaza, i, j++);
			if ("Con plaza".equalsIgnoreCase(ins.plaza))
				inscritos++;
			modelo.setValueAt(ins.cantidad_a_devolver, i, j++);

		}
	}

	private JPanel getPnActividad() {
		if (pnActividad == null) {
			pnActividad = new JPanel();
			pnActividad.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnActividad.setLayout(new BorderLayout(0, 0));
			pnActividad.add(getPnSuperior(), BorderLayout.EAST);
			pnActividad.add(getPnInscritos(), BorderLayout.CENTER);
			pnActividad.add(getPnAtras(), BorderLayout.SOUTH);
			// pnActividad.add(getPnBoton(), BorderLayout.SOUTH);
		}
		return pnActividad;
	}

	private JPanel getPnSuperior() {
		if (pnSuperior == null) {
			pnSuperior = new JPanel();
			pnSuperior.setLayout(new BorderLayout(0, 0));
			pnSuperior.add(getPnDatosBasicos(), BorderLayout.NORTH);
			pnSuperior.add(getPnCentro(), BorderLayout.CENTER);
		}
		return pnSuperior;
	}

	private JPanel getPnDatosBasicos() {
		if (pnDatosBasicos == null) {
			pnDatosBasicos = new JPanel();
			pnDatosBasicos.setLayout(new BorderLayout(0, 0));
			pnDatosBasicos.add(getPnDatos(), BorderLayout.CENTER);
		}
		return pnDatosBasicos;
	}

	private JPanel getPnEstado() {
		if (pnEstado == null) {
			pnEstado = new JPanel();
			pnEstado.setLayout(new BorderLayout(10, 0));
			pnEstado.add(getLbEstado(), BorderLayout.NORTH);
		}
		return pnEstado;
	}

	private JPanel getPnDatos() {
		if (pnDatos == null) {
			pnDatos = new JPanel();
			pnDatos.setLayout(new GridLayout(2, 2, 10, 10));
			pnDatos.add(getPnLugar());
			pnDatos.add(getPnProfesor());
			pnDatos.add(getPanel());
			pnDatos.add(getPnSueldo());
		}
		return pnDatos;
	}

	private JLabel getLbEstado() {
		if (lbEstado == null) {
			lbEstado = new JLabel();
			lbEstado.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbEstado;
	}

	private JLabel getLblLugar() {
		if (lblLugar == null) {
			lblLugar = new JLabel("Lugar:");
			lblLugar.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblLugar;
	}

	private JLabel getLblProfesor() {
		if (lblProfesor == null) {
			lblProfesor = new JLabel("Imparticion:");
			lblProfesor.setDisplayedMnemonic('P');
			lblProfesor.setLabelFor(getBtProfesor());
			lblProfesor.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblProfesor;
	}

	private JLabel getLblSueldo() {
		if (lblSueldo == null) {
			lblSueldo = new JLabel("Remuneraci\u00F3n:");
			lblSueldo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblSueldo;
	}

	private JPanel getPnFechas() {
		if (pnFechas == null) {
			pnFechas = new JPanel();
			pnFechas.setBorder(new TitledBorder(null, "Fechas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnFechas.setLayout(new GridLayout(2, 1, 0, 0));
			pnFechas.add(getPnInscripcion());
			pnFechas.add(getPnCurso());
		}
		return pnFechas;
	}

	private JPanel getPnInscripcion() {
		if (pnInscripcion == null) {
			pnInscripcion = new JPanel();
			pnInscripcion.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Inscripci\u00F3n",
					TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnInscripcion.add(getPnInicioInscripcion());
			pnInscripcion.add(getPnFinInscripcion());
		}
		return pnInscripcion;
	}

	private JPanel getPnCurso() {
		if (pnCurso == null) {
			pnCurso = new JPanel();
			pnCurso.setBorder(new TitledBorder(null, "Curso", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnCurso.add(getPnInicioCurso());
			pnCurso.add(getPnFinCurso());
		}
		return pnCurso;
	}

	private JPanel getPnInicioInscripcion() {
		if (pnInicioInscripcion == null) {
			pnInicioInscripcion = new JPanel();
			pnInicioInscripcion.setLayout(new GridLayout(2, 1, 5, 5));
			pnInicioInscripcion.add(getLbInicioInscripcion());
			pnInicioInscripcion.add(getTxInicioInscripcion());
		}
		return pnInicioInscripcion;
	}

	private JLabel getLbInicioInscripcion() {
		if (lbInicioInscripcion == null) {
			lbInicioInscripcion = new JLabel("Inicio");
			lbInicioInscripcion.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbInicioInscripcion;
	}

	private JTextField getTxInicioInscripcion() {
		if (txInicioInscripcion == null) {
			txInicioInscripcion = new JTextField();
			txInicioInscripcion.setEditable(false);
			txInicioInscripcion.setHorizontalAlignment(SwingConstants.CENTER);
			txInicioInscripcion.setColumns(14);
		}
		return txInicioInscripcion;
	}

	private JPanel getPnFinInscripcion() {
		if (pnFinInscripcion == null) {
			pnFinInscripcion = new JPanel();
			pnFinInscripcion.setLayout(new GridLayout(2, 1, 5, 5));
			pnFinInscripcion.add(getLbFinInscripcion());
			pnFinInscripcion.add(getTxFinInscripcion());
		}
		return pnFinInscripcion;
	}

	private JLabel getLbFinInscripcion() {
		if (lbFinInscripcion == null) {
			lbFinInscripcion = new JLabel("Fin");
			lbFinInscripcion.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbFinInscripcion;
	}

	private JPanel getPnInicioCurso() {
		if (pnInicioCurso == null) {
			pnInicioCurso = new JPanel();
			pnInicioCurso.setLayout(new GridLayout(2, 1, 5, 5));
			pnInicioCurso.add(getLbFechaCurso());
			pnInicioCurso.add(getTxFechaCurso());
		}
		return pnInicioCurso;
	}

	private JLabel getLbFechaCurso() {
		if (lbFechaCurso == null) {
			lbFechaCurso = new JLabel("Fecha");
			lbFechaCurso.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbFechaCurso;
	}

	private JTextField getTxFechaCurso() {
		if (txFechaCurso == null) {
			txFechaCurso = new JTextField();
			txFechaCurso.setEditable(false);
			txFechaCurso.setHorizontalAlignment(SwingConstants.CENTER);
			txFechaCurso.setColumns(10);
		}
		return txFechaCurso;
	}

	private JPanel getPnFinCurso() {
		if (pnFinCurso == null) {
			pnFinCurso = new JPanel();
			pnFinCurso.setLayout(new GridLayout(2, 1, 5, 5));
			pnFinCurso.add(getLbHoraCurso());
			pnFinCurso.add(getTxHora());
		}
		return pnFinCurso;
	}

	private JTextField getTxFinInscripcion() {
		if (txFinInscripcion == null) {
			txFinInscripcion = new JTextField();
			txFinInscripcion.setEditable(false);
			txFinInscripcion.setHorizontalAlignment(SwingConstants.CENTER);
			txFinInscripcion.setColumns(14);
		}
		return txFinInscripcion;
	}

	private JLabel getLbHoraCurso() {
		if (lbHoraCurso == null) {
			lbHoraCurso = new JLabel("Hora");
			lbHoraCurso.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbHoraCurso;
	}

	private JTextField getTxHora() {
		if (txHora == null) {
			txHora = new JTextField();
			txHora.setEditable(false);
			txHora.setHorizontalAlignment(SwingConstants.CENTER);
			txHora.setColumns(10);
		}
		return txHora;
	}

	private JTable getTbInscripciones() {
		if (tbInscripciones == null) {
			tbInscripciones = new JTable();
			tbInscripciones.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					btnCancelarInscripcion.setEnabled(true);
					if (tbInscripciones.getSelectedRow() != -1) {
						int row = tbInscripciones.getSelectedRow();
						canins = listinscripciones.get(row);

					}
				}
			});
		}
		return tbInscripciones;
	}

	private JPanel getPnLugar() {
		if (pnLugar == null) {
			pnLugar = new JPanel();
			pnLugar.add(getLblLugar());
			pnLugar.add(getTxLugar());
		}
		return pnLugar;
	}

	private JTextField getTxLugar() {
		if (txLugar == null) {
			txLugar = new JTextField();
			txLugar.setHorizontalAlignment(SwingConstants.CENTER);
			txLugar.setEditable(false);
			txLugar.setColumns(10);
		}
		return txLugar;
	}

	private JPanel getPnProfesor() {
		if (pnProfesor == null) {
			pnProfesor = new JPanel();
			pnProfesor.add(getLblProfesor());
			pnProfesor.add(getBtProfesor());
		}
		return pnProfesor;
	}

	private JButton getBtProfesor() {
		if (btImparticion == null) {
			btImparticion = new JButton();
			btImparticion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentanaImparticion();
				}
			});
			btImparticion.setForeground(Color.BLUE.darker());
			@SuppressWarnings("unchecked")
			Map<TextAttribute, Object> map = (Map<TextAttribute, Object>) btImparticion.getFont().getAttributes();
			map.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
			Font newFont = new Font(map);
			btImparticion.setFont(newFont);
			btImparticion.setOpaque(false);
			btImparticion.setHorizontalTextPosition(SwingConstants.CENTER);
			btImparticion.setContentAreaFilled(false);
			btImparticion.setBorderPainted(false);

		}
		return btImparticion;
	}

	private void mostrarVentanaImparticion() {
		try {
			List<ProfesorDto> prof = new DtoFactory().getProfesoresByActividad(actividad.id);
			DatosEmpresaDto empresa = new DtoFactory().getEmpresaByActividad(actividad.id);
			VentanaImparticionActividad vp = new VentanaImparticionActividad(prof, empresa);
			vp.setLocationRelativeTo(this);
			vp.setVisible(true);
		} catch (Exception e1) {
			e1.printStackTrace();
			mostrarError();
		}
	}

	private void mostrarError() {
		JOptionPane.showMessageDialog(this, "Se ha producido un error", "Error", JOptionPane.ERROR_MESSAGE);
	}

	private JPanel getPnSueldo() {
		if (pnSueldo == null) {
			pnSueldo = new JPanel();
			pnSueldo.add(getLblSueldo());
			pnSueldo.add(getTxSueldo());
		}
		return pnSueldo;
	}

	private JTextField getTxSueldo() {
		if (txSueldo == null) {
			txSueldo = new JTextField();
			txSueldo.setHorizontalAlignment(SwingConstants.CENTER);
			txSueldo.setEditable(false);
			txSueldo.setColumns(6);
		}
		return txSueldo;
	}

	private JScrollPane getScrTable() {
		if (scrTable == null) {
			scrTable = new JScrollPane();
			scrTable.addMouseListener(new MouseAdapter() {

			});
			scrTable.setViewportView(getTbInscripciones());
		}
		return scrTable;
	}

	private JPanel getPnPrincipal() {
		if (pnPrincipal == null) {
			pnPrincipal = new JPanel();
			pnPrincipal.setLayout(new BorderLayout(0, 0));
			pnPrincipal.add(getPnActividad());
			setContentPane(pnPrincipal);
			pnPrincipal.add(getPnSeleccion(), BorderLayout.NORTH);
		}
		return pnPrincipal;
	}

	private void rellenarCampos(List<InscripcionDto> inscripciones) {
		txSueldo.setText(actividad.sueldoProfesor + " €");

		String imparticion = obtenerImparticion();

		btImparticion.setText(imparticion);
		txLugar.setText(actividad.espacio);

		txHora.setText(actividad.inicioActividad.toString().substring(11, 16) + " - "
				+ actividad.finActividad.toString().substring(11, 16));
		txFinInscripcion.setText(actividad.finInscripcion.toString());
		txFechaCurso.setText(actividad.inicioActividad.toString().substring(0, 10));
		txInicioInscripcion.setText(actividad.inicioInscripcion.toString());

		lbEstado.setText(actividad.estado.toString());

		txSinPlaza.setText(inscripciones.size() - inscritos + "");
		txNInscritos.setText(inscritos + "");
		txPlazas.setText(actividad.numPlazas - inscritos + "");

		pnPrecios.removeAll();
		for (ColectivoDto col : precios.keySet()) {
			JPanel panel = new JPanel();
			JTextField precio = new JTextField(precios.get(col) + " €");
			precio.setEditable(false);
			panel.add(new JLabel(col.nombre));
			panel.add(precio);
			pnPrecios.add(panel);
			scrPrecios.setViewportView(pnPrecios);
		}
	}

	private String obtenerImparticion() {
		try {
			DatosEmpresaDto empresa = new DtoFactory().getEmpresaByActividad(actividad.id);
			if (empresa != null)
				return empresa.nombre;
			else
				return "Profesores";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	@SuppressWarnings("serial")
	private void mostrarDatosActividad() {
		try {
			List<InscripcionDto> inscripciones = new DtoFactory().getInscripciones(actividad.id);
			Object[] columnNames = new Object[] { "Nombre Profesional", "Fecha de inscripción", "Cantidad Pagada",
					"Precio", "Detalle incidencia", "Estado", "Plaza", "Cantidad a devolver (cancelacion)" };
			modelo = new DefaultTableModel(new Object[inscripciones.size()][7], columnNames) {
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			añadirFilas(inscripciones);
			listinscripciones = inscripciones;
			tbInscripciones.setModel(modelo);
			tbInscripciones.setDefaultRenderer(tbInscripciones.getColumnClass(0), cr);
			precios = new DtoFactory().getPreciosColectivos(actividad.id);
			rellenarCampos(inscripciones);
		} catch (Exception e) {
			e.printStackTrace();
			mostrarError();
		}
	}

	private JPanel getPnSeleccion() {
		if (pnSeleccion == null) {
			pnSeleccion = new JPanel();
			pnSeleccion.add(getLblActividad());
			pnSeleccion.add(getCbActividad());
			pnSeleccion.add(getPnEstado());
		}
		return pnSeleccion;
	}

	private JLabel getLblActividad() {
		if (lblActividad == null) {
			lblActividad = new JLabel("Actividad:");
			lblActividad.setDisplayedMnemonic('A');
			lblActividad.setLabelFor(getCbActividad());
		}
		return lblActividad;
	}

	private JComboBox<ActividadDto> getCbActividad() {
		if (cbActividad == null) {
			cbActividad = new JComboBox<ActividadDto>();
			cbActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					actividad = (ActividadDto) modeloActividades.getSelectedItem();
					mostrarDatosActividad();
				}
			});
			cbActividad.setMaximumRowCount(100);
			cbActividad.setModel(modeloActividades);
		}
		return cbActividad;
	}

	@SuppressWarnings("serial")
	private class CustomRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			DefaultTableModel tm = (DefaultTableModel) table.getModel();

			if (column == 4) {
				String incidencia = (String) tm.getValueAt(row, column);
				try {
					double i = Double.parseDouble(incidencia);
					if (i < 0)
						setForeground(Color.red);
					else if (i > 0)
						setForeground(Color.green);
					else
						setForeground(Color.black);
				} catch (NumberFormatException e) {
					setForeground(Color.black);
				}
			} else {
				setForeground(Color.black);
			}

			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		}
	}

	private JPanel getPnInscritos() {
		if (pnInscritos == null) {
			pnInscritos = new JPanel();
			pnInscritos.setLayout(new BorderLayout(0, 0));
			pnInscritos.add(getPnInfoInscritos(), BorderLayout.NORTH);
			pnInscritos.add(getScrTable());
		}
		return pnInscritos;
	}

	private JPanel getPnInfoInscritos() {
		if (pnInfoInscritos == null) {
			pnInfoInscritos = new JPanel();
			pnInfoInscritos.add(getLblSinPlaza());
			pnInfoInscritos.add(getTxSinPlaza());
			pnInfoInscritos.add(getLbNInscritos());
			pnInfoInscritos.add(getTxNInscritos());
			pnInfoInscritos.add(getLbPlazasDisponibles());
			pnInfoInscritos.add(getTxPlazas());
		}
		return pnInfoInscritos;
	}

	private JLabel getLbNInscritos() {
		if (lbNInscritos == null) {
			lbNInscritos = new JLabel("N\u00BA inscritos (con plaza):");
		}
		return lbNInscritos;
	}

	private JTextField getTxNInscritos() {
		if (txNInscritos == null) {
			txNInscritos = new JTextField();
			txNInscritos.setEditable(false);
			txNInscritos.setColumns(5);
		}
		return txNInscritos;
	}

	private JLabel getLbPlazasDisponibles() {
		if (lbPlazasDisponibles == null) {
			lbPlazasDisponibles = new JLabel("Plazas disponibles:");
		}
		return lbPlazasDisponibles;
	}

	private JTextField getTxPlazas() {
		if (txPlazas == null) {
			txPlazas = new JTextField();
			txPlazas.setEditable(false);
			txPlazas.setColumns(5);
		}
		return txPlazas;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getBtnCancelarInscripcion());
		}
		return panel;
	}

	private JPanel getPnPrecios() {
		if (pnPrecios == null) {
			pnPrecios = new JPanel();
			pnPrecios.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Precios",
					TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnPrecios.setLayout(new BoxLayout(pnPrecios, BoxLayout.Y_AXIS));
		}
		return pnPrecios;
	}

	private JScrollPane getScrPrecios() {
		if (scrPrecios == null) {
			scrPrecios = new JScrollPane();
			scrPrecios.setViewportView(getPnPrecios());
		}
		return scrPrecios;
	}

	private JPanel getPnCentro() {
		if (pnCentro == null) {
			pnCentro = new JPanel();
			pnCentro.setLayout(new BoxLayout(pnCentro, BoxLayout.Y_AXIS));
			pnCentro.add(getPnFechas());
			pnCentro.add(getScrPrecios());
		}
		return pnCentro;
	}

	private JButton getBtnCancelarInscripcion() {
		if (btnCancelarInscripcion == null) {
			btnCancelarInscripcion = new JButton("Cancelar Inscripcion");
			btnCancelarInscripcion.setEnabled(false);
			btnCancelarInscripcion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					try {
						cancelarinscripcion();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					btnCancelarInscripcion.setEnabled(false);
					mostrarDatosActividad();
				}
			});
		}
		return btnCancelarInscripcion;
	}

	public void cancelarinscripcion() throws SQLException {

		int res = new DtoFactory().cancelaInscripcion(canins);
		if (res != 1) {
			JOptionPane.showMessageDialog(null, "Problema base de datos inscripcion no cancelada.", "Lo sentimos", 2);
		} else {
			generarReciboCancelacion(canins);

		}

	}

	public void generarReciboCancelacion(InscripcionDto ins) {
		Date inicioimparticion = null;
		try {
			inicioimparticion = new DtoFactory().fechaInicioImparticion(canins.actividad.id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// lo que se devuelve::: averguar si lo tengo que escribir en bbdd en cantidad
		// pagada
		//bug! escribir en bbdd cantidad a devolver de cancelar
		double cantidadDevuelta = ins.cantidadDevuelta(inicioimparticion, actividad.estado.toString());
		try {
			int res = new DtoFactory().agregarcantidadadevolver(cantidadDevuelta,
					ins);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
				"recibos/recibosInscripcionesCanceladas/" + ins.profesional.nombre + "_inscripcion_cancelada.txt")))) {
			writer.write("/////RECIBO INCRIPCION CANCELADA/////" + "\n");
			writer.write(ins.profesional.nombre + " " + "\n");

			writer.write("cantidad devuelta: " + cantidadDevuelta + "\n");

			// writer.write( ins.getEstado()+ " "+"\n");
			writer.close();

		} catch (IOException ex) {
			System.out.println("Problema al generar recibo de inscripcion");
		}

	}

	private JPanel getPnAtras() {
		if (pnAtras == null) {
			pnAtras = new JPanel();
			pnAtras.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnAtras.add(getBtnAtras());
		}
		return pnAtras;
	}

	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.setVerticalAlignment(SwingConstants.BOTTOM);
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnAtras.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return btnAtras;
	}
	private JLabel getLblSinPlaza() {
		if (lblSinPlaza == null) {
			lblSinPlaza = new JLabel("Sin plaza:");
		}
		return lblSinPlaza;
	}
	private JTextField getTxSinPlaza() {
		if (txSinPlaza == null) {
			txSinPlaza = new JTextField();
			txSinPlaza.setEditable(false);
			txSinPlaza.setColumns(5);
		}
		return txSinPlaza;
	}
}
