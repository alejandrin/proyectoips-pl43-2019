package logica;

public class Profesional {
	String nombre, apellido, email,tfl;
	int dni, id_profesional, id_colectivo;
	public Profesional(String nombre, String apellido, String email,  int id_profesional) {
 		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		
		this.id_profesional = id_profesional;
	}
	
	public Profesional(String nombre, String apellido, String email,  int id_profesional, int id_colectivo) {
 		this(nombre, apellido, email, id_profesional);
 		this.id_colectivo = id_colectivo;
	}
	
	public int getIdColectivo() {
		return id_colectivo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTfl() {
		return tfl;
	}
	public void setTfl(String tfl) {
		this.tfl = tfl;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public int getId_profesional() {
		return id_profesional;
	}
	public void setId_profesional(int id_profesional) {
		this.id_profesional = id_profesional;
	}

}
