package logica;

import java.util.Date;

public class TransferenciaProfesional {
	int id, id_inscripcion, id_actividad;
	public int id_profesional;
	double cantidad_pagada_por_COIIPA, cantidad_pagada_por_profesional;
	Date fecha_transferencia;
	private String tipoPago;
	
	
	public TransferenciaProfesional(int id, int id_profesional,int id_inscripcion,int id_actividad,
			double cantidad_pagada_por_COIIPA, double cantidad_pagada_por_profesional,Date fecha_transferencia)
	{
		this.id = id;
		this.id_inscripcion = id_inscripcion;
		this.id_profesional = id_profesional;
		this.id_actividad = id_actividad;
		this.cantidad_pagada_por_COIIPA = cantidad_pagada_por_COIIPA;
		this.cantidad_pagada_por_profesional = cantidad_pagada_por_profesional;
		this.fecha_transferencia = fecha_transferencia;
	}
	
	public TransferenciaProfesional(int id, int id_profesional,int id_inscripcion,int id_actividad,
			double cantidad_pagada_por_COIIPA, double cantidad_pagada_por_profesional,Date fecha_transferencia, String tipoPago)
	{
		this(id, id_profesional, id_inscripcion, id_actividad, cantidad_pagada_por_COIIPA,  cantidad_pagada_por_profesional, fecha_transferencia);
		this.tipoPago = tipoPago;
	}


	public Date getFecha_transferencia() {
		return fecha_transferencia;
	}




	public void setFecha_transferencia(Date fecha_transferencia) {
		this.fecha_transferencia = fecha_transferencia;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getid_inscripcion() {
		return id_inscripcion;
	}


	public void setid_inscripcion(int id_inscripcion) {
		this.id_inscripcion = id_inscripcion;
	}


	public int getId_actividad() {
		return id_actividad;
	}


	public void setId_actividad(int id_actividad) {
		this.id_actividad = id_actividad;
	}


	public double getCantidad_pagada_por_COIIPA() {
		return cantidad_pagada_por_COIIPA;
	}


	public void setCantidad_pagada_por_COIIPA(double cantidad_pagada_por_COIIPA) {
		this.cantidad_pagada_por_COIIPA = cantidad_pagada_por_COIIPA;
	}


	public double getCantidad_pagada_por_profesional() {
		return cantidad_pagada_por_profesional;
	}


	public void setCantidad_pagada_por_profesional(double cantidad_pagada_por_profesional) {
		this.cantidad_pagada_por_profesional = cantidad_pagada_por_profesional;
	}

	public String getTipoPago() {
		return tipoPago;
	}
	
	
}
