package logica;

public enum EstadoActividad {
	PLANIFICADA("PLANIFICADA"),
	PERIODO_INSCRIPCION("EN PERIODO DE INSCRIPCIÓN"),
	INSCRIPCION_CERRADA("INSCRIPCIÓN CERRADA"),
	CERRADA("CERRADA"),
	CANCELADA("CANCELADA"),
	RETRASADA("RETRASADA");
	
	private String value;

    EstadoActividad(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }

    public static EstadoActividad getEnum(String value) {
        for(EstadoActividad v : values())
            if(v.getValue().equalsIgnoreCase(value)) return v;
        throw new IllegalArgumentException();
    }
}
