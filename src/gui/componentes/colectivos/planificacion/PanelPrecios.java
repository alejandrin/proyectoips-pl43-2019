package gui.componentes.colectivos.planificacion;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JOptionPane;
import logica.dto.ColectivoDto;
import util.DtoFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import java.sql.SQLException;

public class PanelPrecios extends JPanel {

	private static final long serialVersionUID = 1L;
	private JPanel pnNuevo;
	private JButton btnNuevoPrecio;
	private JScrollPane scrPrecios;
	private JPanel pnPrecios;
	private List<ColectivoDto> colectivos;
	private double precioGeneral = -1;

	
	public PanelPrecios() {
		setLayout(new BorderLayout(0, 0));
		add(getPnNuevo(), BorderLayout.EAST);
		add(getScrPrecios(), BorderLayout.CENTER);
		try {
			colectivos = new DtoFactory().getColectivos();
			crearNuevoPanel();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private JPanel getPnNuevo() {
		if (pnNuevo == null) {
			pnNuevo = new JPanel();
			pnNuevo.add(getBtnNuevoPrecio());
		}
		return pnNuevo;
	}

	private JButton getBtnNuevoPrecio() {
		if (btnNuevoPrecio == null) {
			btnNuevoPrecio = new JButton("Nuevo precio");
			btnNuevoPrecio.setMnemonic('N');
			btnNuevoPrecio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					crearNuevoPanel();
				}
			});
		}
		return btnNuevoPrecio;
	}

	private void crearNuevoPanel() {
		pnPrecios.add(new PanelPrecioColectivo(colectivos, this));
		scrPrecios.setViewportView(pnPrecios);
	}

	private JScrollPane getScrPrecios() {
		if (scrPrecios == null) {
			scrPrecios = new JScrollPane();
			scrPrecios.setViewportView(getPnPrecios());
		}
		return scrPrecios;
	}

	private JPanel getPnPrecios() {
		if (pnPrecios == null) {
			pnPrecios = new JPanel();
			pnPrecios.setSize(new Dimension(10, 10));
			pnPrecios.setLayout(new BoxLayout(pnPrecios, BoxLayout.PAGE_AXIS));
		}
		return pnPrecios;
	}

	public void actualizarColectivos() {
		try {
			colectivos = new DtoFactory().getColectivos();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Component c : pnPrecios.getComponents())
			((PanelPrecioColectivo) c).actualizarModelo(colectivos);
	}

	public Map<ColectivoDto, Double> comprobarValidez() {		
		Map<ColectivoDto, Double> colectivosSeleccionados = new HashMap<ColectivoDto, Double>();
		try {
			for (Component c : pnPrecios.getComponents()) {
				PanelPrecioColectivo panel = (PanelPrecioColectivo) c;
				if(!panel.comprobarValidez()) {
					JOptionPane.showMessageDialog(this, "A�ade un precio al colectivo seleccionado " + panel.getColectivoSeleccionado());
					return null;
				}
				if(colectivosSeleccionados.put(panel.getColectivoSeleccionado(), panel.getPrecio()) != null)
					throw new IllegalArgumentException();
				if(panel.getColectivoSeleccionado().id == ColectivoDto.ID_GENERAL)
					precioGeneral = panel.getPrecio();
			}

			return colectivosSeleccionados;

		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(this, "No se pueden poner dos precios a un mismo colectivo");
			return null;
		}
	}

	void quitarPanelPrecio(PanelPrecioColectivo panelPrecioColectivo) {
		if(pnPrecios.getComponents().length == 1)
			JOptionPane.showMessageDialog(null, "Tiene que haber al menos un precio.");
		else {
			pnPrecios.remove(panelPrecioColectivo);
			scrPrecios.setViewportView(pnPrecios);
		}
	}
	
	public double getPrecioGeneral() {
		return precioGeneral;
	}
}
