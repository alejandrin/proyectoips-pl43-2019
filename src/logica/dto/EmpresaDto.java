package logica.dto;

public class EmpresaDto {
	public String nombre;
	public int id_empresa;
	public double pagado_empresa;
	public double remuneracion;
	
	public ActividadDto actividad;
	public String cif;
	
	public double factura_cantidad=0.0;
	public String factura_fecha="";
	public int factura_id=0;
	
	public String getFactura_fecha() {
		return factura_fecha ;
	}
	 
	
	

}
