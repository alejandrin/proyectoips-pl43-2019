package logica.dto;

public class IncidenciaDto {
	public String nombreActividad;
	public double precioActividad;
	public double cantidadPagada;
	public String nombreProfesional;
	public String apellidoProfesional;
}
