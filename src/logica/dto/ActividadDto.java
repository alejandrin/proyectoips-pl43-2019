package logica.dto;


import java.sql.Timestamp;

import logica.EstadoActividad;

public class ActividadDto {

	public int id;
	public String nombre;
	public String objetivos;
	public String contenidos;
	public Timestamp inicioInscripcion;
	public Timestamp finInscripcion;
	public Timestamp inicioActividad;
	public Timestamp finActividad;
	public int numPlazas;
	public EstadoActividad estado;
	public double ingresosReales;
	public double gastosReales;
	public double ingresosEstimados;
	public double gastosEstimados;
	public ProfesorDto profesor;
	public double sueldoProfesor;
	public double precio;
	public String espacio;
	public int id_empresa;
	
	public String toString() {
		String string = "Nombre: " + nombre + ". N� plazas: " + numPlazas + ". Precio general: " ;
		string += precio == ColectivoDto.SIN_PRECIO_GENERAL ? "sin precio general" : precio + " �";
		return string;
	}

	public String getFechainiact() {
		// TODO Auto-generated method stub
		return inicioActividad.toString().substring(0, 10);
	}
	public String getFechainiins() {
		// TODO Auto-generated method stub
		return inicioInscripcion.toString().substring(0, 10);
	}
	public String getFechafinact() {
		// TODO Auto-generated method stub
		return finActividad.toString().substring(0, 10);
	}
	public String getFechafinins() {
		// TODO Auto-generated method stub
		return finInscripcion.toString().substring(0, 10);
	}
	@SuppressWarnings("deprecation")
	public Timestamp getTimeStampiniact()
	{
		int ano = Integer.parseInt(inicioActividad.toString().substring(0, 4));
		int mes = Integer.parseInt(inicioActividad.toString().substring(5, 7));
		int dia = Integer.parseInt(inicioActividad.toString().substring(8, 10));
		
		System.out.println(ano+" "+mes+ " "+ dia);

		return new Timestamp(ano, mes, dia, 00, 00, 00, 0);
	}
}
