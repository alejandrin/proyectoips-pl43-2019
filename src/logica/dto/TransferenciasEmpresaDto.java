package logica.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class TransferenciasEmpresaDto {
	public int id_trans_emp;
	public String fact;
	public String nif;
	public Date fecha_trans;
	public EmpresaDto empresa;
	public int pagadoporCOIIPA;
	public int pagadoporEmpresa;
	public String tipoPago;
	public int cantidad_pagada;
	public String movimento;
	
	public String getfecha_trans()
	{
		return fecha_trans.toString().substring(0,10);
	}
}
