package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.ProfesorDto;

public class ParserProfesor {

	public ProfesorDto parseProfesor(ResultSet rs) throws SQLException {
		ProfesorDto prof = null;
		
		if(rs.next()) {
			prof = new ProfesorDto();
			
			prof.apellidos = rs.getString("apellidos");
			prof.dni = rs.getString("dni");
			prof.email = rs.getString("email");
			prof.id = rs.getInt("id_profesor");
			prof.nombre = rs.getString("nombre");
			prof.telefono = rs.getString("telefono");
		}
		return prof;
	}

	public List<ProfesorDto> parseProfesores(ResultSet rs) throws SQLException {
		List<ProfesorDto> profesores = new ArrayList<ProfesorDto>();
		ProfesorDto profesor = parseProfesor(rs);
		while(profesor != null) {
			profesores.add(profesor);
			profesor = parseProfesor(rs);
		}
		
		return profesores;
	}
	
}
