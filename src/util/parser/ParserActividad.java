package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.EstadoActividad;
import logica.dto.ActividadDto;
import logica.dto.ProfesorDto;

public class ParserActividad {

	public List<ActividadDto> parseActividades(ResultSet rs) throws SQLException{
		List<ActividadDto> acts = new ArrayList<ActividadDto>();
		while(rs.next())
			acts.add(parseActividad(rs));
		return acts;
	}
	public List<ActividadDto> parseActividadesIns(ResultSet rs) throws SQLException{
		List<ActividadDto> acts = new ArrayList<ActividadDto>();
		while(rs.next())
			acts.add(parseActividadIns(rs));
		return acts;
	}
	
	public ActividadDto parseActividad(ResultSet rs) throws SQLException {
		ActividadDto act = new ActividadDto();
		ProfesorDto prof = new ProfesorDto();
		if(!rs.isBeforeFirst() || rs.next()) {
			//prof.id = rs.getInt("id_profesor");
			//prof.nombre = rs.getString("nombre_prof");
			//prof.apellidos = rs.getString("apellidos");
			
			act.profesor = prof;
			act.nombre = rs.getString("nombre");
			act.objetivos = rs.getString("objetivos");
			act.contenidos = rs.getString("contenidos");
			act.inicioInscripcion = rs.getTimestamp("fecha_abre_inscripcion");
			act.finInscripcion = rs.getTimestamp("fecha_cierra_inscripcion");
			act.inicioActividad = rs.getTimestamp("fecha_inicio_imparticion");
			act.finActividad = rs.getTimestamp("fecha_fin_imparticion");
			act.numPlazas = rs.getInt("num_plazas");
			act.estado = EstadoActividad.getEnum(rs.getString("estado").toUpperCase()); 
			act.ingresosReales = rs.getDouble("ingresos_confirmados");
			act.ingresosEstimados = rs.getDouble("ingresos_estimados");
			act.gastosReales = rs.getDouble("gastos_confirmados");
			act.gastosEstimados = rs.getDouble("gastos_estimados");

			//act.sueldoProfesor = rs.getDouble("remuneracion");
			act.precio = rs.getDouble("precio");
			act.espacio = rs.getString("espacio");
			act.id = rs.getInt("id_actividad");
		}
		return act;
	}

	public ActividadDto parseActividadIns(ResultSet rs) throws SQLException {
		ActividadDto act = new ActividadDto();
		ProfesorDto prof = new ProfesorDto();
		if(!rs.isBeforeFirst() || rs.next()) {
 
			 
			
			act.profesor = prof;
			act.nombre = rs.getString("nombre");
			act.objetivos = rs.getString("objetivos");
			act.contenidos = rs.getString("contenidos");
			act.inicioInscripcion = rs.getTimestamp("fecha_abre_inscripcion");
			act.finInscripcion = rs.getTimestamp("fecha_cierra_inscripcion");
			act.inicioActividad = rs.getTimestamp("fecha_inicio_imparticion");
			act.finActividad = rs.getTimestamp("fecha_fin_imparticion");
			act.numPlazas = rs.getInt("num_plazas");
			act.estado = EstadoActividad.getEnum(rs.getString("estado").toUpperCase()); 
			act.ingresosReales = rs.getDouble("ingresos_confirmados");
			act.ingresosEstimados = rs.getDouble("ingresos_estimados");
			act.gastosReales = rs.getDouble("gastos_confirmados");
			act.gastosEstimados = rs.getDouble("gastos_estimados");
 			act.precio = rs.getDouble("precio");
			act.espacio = rs.getString("espacio");
			act.id = rs.getInt("id_actividad");
		}
		return act;
	}
	
}