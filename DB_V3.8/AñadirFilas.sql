--##################################################################################################
--######################################## cantidad a devolver cancelada  ###########################
 --##################################################################################################
ALTER TABLE lista_inscritos add column cantidad_a_devolver decimal(10,4)
--##################################################################################################
--######################################## factura creada para empresas  ###########################
 --##################################################################################################
ALTER TABLE lista_inscritos add column plaza varchar(30)
ALTER TABLE transferencias_profesional drop column plaza
ALTER TABLE transferencias_profesional drop column cantidad_pagada_por_profesional
ALTER TABLE transferencias_profesional drop column factid

ALTER TABLE transferencias_empresa add column id_actividad decimal(10,2)

ALTER TABLE transferencias_empresa drop column nif
ALTER TABLE transferencias_empresa drop column facturaid
ALTER TABLE transferencias_empresa drop column cantidad_pagada_por_empresa
--##################################################################################################
--######################################## cambios v2.4 para historia 1: ###########################
--############ a�adir varios profesores con dos listas, y remuneraciones diferentes ################
--##################################################################################################
ALTER TABLE actividadesProfesores add column remuneracion decimal(10,2)
ALTER TABLE actividades drop column remuneracion_profesor
alter table actividades drop constraint fk_actividades_profesores
ALTER TABLE actividades drop column ID_PROFESOR

--##################################################################################################
--######################################## empresas registrar ##################
--##################################################################################################
ALTER TABLE empresas add column representante varchar(100)
ALTER TABLE empresas add column email varchar(100)
ALTER TABLE empresas add column telefono varchar(100)
ALTER TABLE empresas add column cif varchar(100)

--##################################################################################################
--######################################## transferencias_profesional inscripcion multiple ##################
--##################################################################################################
ALTER TABLE transferencias_profesional add column facturaid varchar(100)

--##################################################################################################
--######################################## cambios V18 para historia 1 2019/11/18 ##################
--##################################################################################################
ALTER TABLE actividades drop column horarios
ALTER TABLE actividades drop column profesores
ALTER TABLE actividades add column duracionsesion1
ALTER TABLE actividades add column duracionsesion2
ALTER TABLE actividades add column espacio2
commit;



--##################################################################################################
--######################################## a�adir columnas empresas y remuneracion en actividades ###################
--##################################################################################################
ALTER TABLE actividades add column id_empresa varchar(100)
ALTER TABLE actividades add column remuneracion_empresa decimal(10,2)
ALTER TABLE actividades add column pagado_empresa decimal(10,2)
ADD constraint fk_actividad_empresa foreign key ( id_empresa) references empresas(id_empresa);

--##################################################################################################
--######################################## a�adir empresas y profesoresDeEmpresas ###################
--##################################################################################################

insert into EMPRESAS values (1,'Amazon');
insert into EMPRESAS values (2,'Oracle');
insert into EMPRESAS values (3,'Huawei');
insert into EMPRESAS values (4,'Amenesty International');

insert into profesores_empresas values(1,6);
insert into profesores_empresas values(4,6);
insert into profesores_empresas values(1,5);

--##################################################################################################
--######################################## problema guille discrepancia de filas ###################
--##################################################################################################
ALTER TABLE transferencias_profesional add column fecha_transferencia date
ALTER TABLE transferencias_profesor add column fecha_transferencia date

--##################################################################################################
--######################################## Historia 1 y 2 sprint 2 ###################
--##################################################################################################
ALTER TABLE ACTIVIDADES ADD COLUMN horarios varchar(5000)
ALTER TABLE ACTIVIDADES ADD COLUMN profesores varchar(5000)

--##################################################################################################
--######################################## Astrid modificacion transferencias_profesional ###################
--##################################################################################################
ALTER TABLE transferencias_profesional
ADD column id_profesional decimal(10,2);
ALTER TABLE transferencias_profesional
ADD constraint fk_transferencias_profesionales foreign key ( id_profesional) references profesional(id_profesional);

ALTER TABLE inscripcion
DROP column cantidad_pagada;
ALTER TABLE lista_inscritos
ADD column cantidad_pagada;

 
ALTER TABLE inscripcion
DROP column estado;

ALTER TABLE lista_inscritos
ADD column estado varchar(100) not null;

ALTER TABLE lista_inscritos
ADD constraint ck_inscripcion check (estado in ('recibida', 'cobrada', 'cancelada', 'incidencia'))
--##################################################################################################
--######################################## Astrid foreign key inscripcion profesional ###################
--##################################################################################################
ALTER TABLE transferencias_profesional  
DROP CONSTRAINT fk_transferencias_profesional;

ALTER TABLE INSCRIPCION  
DROP CONSTRAINT pk_inscripcion;

ALTER TABLE transferencias_profesional  
DROP column id_profesional;

ALTER TABLE INSCRIPCION add column id_inscripcion number;
ALTER TABLE inscripcion  
DROP  constraint fk_inscripcion  ;
ALTER TABLE inscripcion  
DROP  id_profesional  ;

ALTER TABLE INSCRIPCION
ADD constraint pk_inscripcion primary key(id_inscripcion);

ALTER TABLE transferencias_profesional
ADD column id_inscripcion number;

ALTER TABLE transferencias_profesional
ADD constraint fk_transferencias_profesional foreign key (id_inscripcion) references inscripcion(id_inscripcion);

--##################################################################################################
--######################################## problema guille discrepancia de filas ###################
--##################################################################################################
ALTER TABLE transferencias_profesional add column fecha_transferencia date
ALTER TABLE transferencias_profesor add column fecha_transferencia date
ALTER TABLE facturas_profesores add column fecha_factura date


--##################################################################################################
--######################################## CONSTRAINT ##############################################
--##################################################################################################
ALTER TABLE actividades  
DROP CONSTRAINT ck_actividades;

ALTER TABLE actividades
ADD constraint ck_actividades check (estado in ('planificada', 'inscripci�n cerrada', 'en periodo de inscripci�n', 'cerrada', 'cancelada'));

ALTER TABLE inscripcion
DROP CONSTRAINT ck_inscripcion;

ALTER TABLE inscripcion
ADD CONSTRAINT ck_inscripcion check (estado in('incidencia','recibida', 'cobrada', 'cancelada'));
--##################################################################################################
--######################################## PROFESORES ##############################################
--##################################################################################################
insert into profesores values (1,'Nestor','Garc�a Fern�ndez','nestor@uniovi.es','985555555','88888888A');
insert into profesores values (2,'Oliverio','Fernando Guti�rrez','oliverio@uniovi.es','985555556','88888889A');
insert into profesores values (3,'Chechu','Alonso Rodriguez','chechu@uniovi.es','985555557','88888890A');
insert into profesores values (4,'Charo','�lvarez L�pez','charo@uniovi.es','985555558','88888891A');
insert into profesores values (5,'Andres Enrique','L�pez Castro','andresenrique@uniovi.es','985555559','88888892A');
insert into profesores values (6,'Redondo','Jose Manuel Redondo','redondo@uniovi.es','985555560','88888893A');
--##################################################################################################
--######################################## PROFESIONALES ###########################################
--##################################################################################################
insert into PROFESIONALES values (12, 'Jose', 'Garcia', 'mini@uniovi.es', '444555666', '77777776B');
insert into PROFESIONALES values (4, 'Sergio', 'Martinez Mu�iz', 'sergio@uniovi.es', '444555666', '77777775B');
insert into PROFESIONALES values (5, 'Oscar', 'Lopez Castro', 'oscar@uniovi.es', '444555666', '77777779B');
insert into PROFESIONALES values (6, 'Robertin', 'Gonzalez Masada', 'rober@uniovi.es', '444555666', '77777780B');
insert into PROFESIONALES values (7, 'Josu', 'Garcia Barrios', 'josu@uniovi.es', '444555666', '77777781B');
insert into PROFESIONALES values (8, 'Tiano', 'Garcia Gonzalez', 'tiano@uniovi.es', '444555666', '77777782B');
insert into PROFESIONALES values (9, 'Abdelmonin', 'Aboufaras', 'monin@uniovi.es', '444555666', '77777783B');
insert into PROFESIONALES values (10, 'Yacin', 'Aboufaras', 'yacin@uniovi.es', '444555666', '77777784B');
insert into PROFESIONALES values (11, 'Pablo', 'Otero Leon', 'pabline@uniovi.es', '444555666', '77777785B');
--##################################################################################################
--######################################## ACTIVIDADES #############################################
--##################################################################################################
insert into actividades values (1,'Concurso hacking','Entrar en la c�rcel','Metasploit | Penetration Testing Software',
	TO_DATE('2019/10/01 10:00:00','YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/10 10:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/11 10:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/11 14:30:00', 'YYYY/MM/DD HH:MI:SS'),
	30,
	'cancelada',
	'AS-01',
	50,
	null, null, null, null,
	1,
	350
);
insert into actividades values (2,'Charla de Amnist�a Internacional',
	'Pasar un buen rato',
	'Estimados alumnos y profesores, tendr� lugar la charla/taller de Amnist�a Internacional titulada "Por un Selfie Bueno" (va adjunto pdf explicativo). Tendr� una duraci�n m�xima de una hora.',
	TO_DATE('2019/12/01 09:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/12/10 23:59:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/12/11 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/12/11 16:00:00', 'YYYY/MM/DD HH:MI:SS'),
	40,
	'planificada',
	'AS-02',
	10,
	null, null, null, null,
	2,
	250
);
insert into actividades values (3,'Energy-Efficient Fuzzy-Based DASH Streaming Algorithgm.',
	'Hacer un uso de los recursos eficiente',
	'With the expansive demand for video streaming over mobile networks, it became necessary to adopt schemes that balance the need for high video quality with the available network resources when streaming or downloading the video. Several approaches were proposed in the literature including the use of Dynamic Adaptive Streaming over HTTP (DASH). In this work, we consider an approach through which we place sufficient emphasis on the constrained battery resources in mobile devices when making decisions on the quality (or bitrate) of the video to be requested. This is done through the use of a fuzzy logic controller that enhances over that proposed for the Fuzzy-based DASH (FDASH) scheme. Simulation results show that our proposed approach conserves more energy than its predecessor while maintaining similar levels of video quality and avoiding playback interruptions.',
	TO_DATE('2019/10/01 09:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/10 23:59:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/02/11 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/02/11 16:00:00', 'YYYY/MM/DD HH:MI:SS'),
	70,
	'inscripci�n cerrada',
	'AS-02',
	8,
	null, null, null, null,
	3,
	150
);
insert into actividades values (4,'Professional Development: How to write an effective cover letter',
	'Mejora profesional',
	'Buenos d�as. Seg�n nos informan los representantes del programa UO&IMFAHE 2019-2020, se recuerda que tendr� lugar la segunda sesi�n del Quarter Course sobre Career Development, sobre How to write an effective cover letter. Incluyen tambi�n en su mensaje algunos requerimientos importantes con vistas a obtener la certificaci�n que se deriva de la participaci�n en los cursos.',
	TO_DATE('2019/10/14 09:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/02/10 23:59:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/02/11 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/02/11 16:00:00', 'YYYY/MM/DD HH:MI:SS'),
	15,
	'en periodo de inscripci�n',
	'AS-02',
	20,
	null, null, null, null,
	4,
	100
);
insert into actividades values (5,'CyberLeague',
	'Inscr�bete y participa en la ciberliga y arranca tu carrera en ciberseguridad',
	'Estimados alumnos, nos avisan desde la Guardia Civil que es posible apuntarse aunque no se tenga grupo o no est� completo y que luego se crear�n los grupos hasta el mismo d�a del concurso, que va a haber total flexibilidad para eso. Ya hay apuntados alumnos de Derecho, Marketing etc para formar equipos multidisciplinares, por lo tanto no ser� ning�n problema incluir en cada grupo alumnos de esas especialidades. Si quieres participar y no tienes grupo, inscr�bete igualmente y te buscamos o completamos el grupo. Un saludo',
	TO_DATE('2019/10/01 09:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/10 23:59:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/11 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/11 20:00:00', 'YYYY/MM/DD HH:MI:SS'),
	100,
	'cerrada',
	'AS-02',
	0,
	null, null, null, null,
	5,
	0
);
insert into actividades values (6,'Convocatoria movilidad SICUE',
	'Se ha publicado la convocatoria de movilidad SICUE para el curso 19-20',
	'Plazo de presentaci�n: 13 de febrero al 13 de marzo de 2019. Para el curso 2019-2020 la movilidad SICUE se solicitar� por parte de los estudiantes por medios electr�nicos (accediendo con su Usuario y Clave)',
	TO_DATE('2019/10/01 09:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/10 23:59:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/11 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2019/10/11 20:00:00', 'YYYY/MM/DD HH:MI:SS'),
	50,
	'cerrada',
	'EII AS-01',
	20,
	null, null, null, null,
	1,
	400
);

-- //////// Nuevas actividades para hacer pruebas //////////////////

insert into actividades values (10,'Capgemini',
	'Esperando sea de vuestro inter�s, adjuntamos oferta de la empresa Capgemini: ',
	'Capgemini Asturias se encuentra en expansi�n y para ello buscamos a gente con vocaci�n por el sector IT y continuar su formaci�n en el �mbito de la inform�tica. Si tienes pasi�n por la inform�tica, te gusta estar en continuo aprendizaje y participar en proyectos internacionales, te estamos esperando. Te daremos la oportunidad de entrar a formar parte de un proyecto de transformaci�n estrat�gica y cultural donde el �xito empieza en sus equipos. �Qu� buscamos? - Personas que est�n cursando sus estudios de Grado de Ingenier�a Inform�tica y cuenten con un componente cient�fico, tecnol�gico y anal�tico. - Posibilidad de convenio de pr�cticas - Conocimientos familiarizados con diferentes lenguajes de programaci�n. �Qu� te podemos ofrecer? - Pr�cticas remuneradas - Formaci�n en un entorno real - Posibilidad de contrataci�n - Un ambiente de trabajo colaborativo y motivador. ',
	TO_DATE('2020/05/01 09:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/06/01 23:59:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/06/02 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	TO_DATE('2020/06/04 15:00:00', 'YYYY/MM/DD HH:MI:SS'),
	50,
	'cerrada',
	'EII AS-01',
	20,
	null, null, null, null,--gastyos y cobros estimados
	1,--id empresa
	100,--remuneracion empresa
	0,--pagado empresa
	2,--duracion sesion 1
	2,--duracion sesion 2
	'EII AS-01'
);
insert into ACTIVIDADESPROFESORES values(3,10,50)
insert into ACTIVIDADESPROFESORES values(4,10,50)
insert into ACTIVIDADESPROFESORES values(5,10,50)
insert into ACTIVIDADESPROFESORES values(3,9,50)
insert into ACTIVIDADESPROFESORES values(4,9,70)
insert into ACTIVIDADESPROFESORES values(1,8,10)
insert into ACTIVIDADESPROFESORES values(2,8,20)
insert into ACTIVIDADESPROFESORES values(3,7,30)
insert into ACTIVIDADESPROFESORES values(4,7,40)
insert into ACTIVIDADESPROFESORES values(5,6,50)
insert into ACTIVIDADESPROFESORES values(6,6,60)
insert into ACTIVIDADESPROFESORES values(7,5,70)
insert into ACTIVIDADESPROFESORES values(1,5,80)
insert into ACTIVIDADESPROFESORES values(2,5,90)
insert into ACTIVIDADESPROFESORES values(3,4,100)
insert into ACTIVIDADESPROFESORES values(4,4,110)
insert into ACTIVIDADESPROFESORES values(5,4,120)
insert into ACTIVIDADESPROFESORES values(6,3,130)
insert into ACTIVIDADESPROFESORES values(7,3,140)
insert into ACTIVIDADESPROFESORES values(1,3,150)
insert into ACTIVIDADESPROFESORES values(2,2,160)
insert into ACTIVIDADESPROFESORES values(3,2,170)
insert into ACTIVIDADESPROFESORES values(4,2,180)
insert into ACTIVIDADESPROFESORES values(5,1,190)
insert into ACTIVIDADESPROFESORES values(6,1,200)
insert into ACTIVIDADESPROFESORES values(7,1,210)

update actividades set estado='planificada' where id_actividad=10
--##################################################################################################
--######################################## INSCRIPCIONES ###########################################
--##################################################################################################
insert into INSCRIPCION values (5, 2, to_date('2019/10/20', 'YYYY/MM/DD'),10 ,'incidencia');
insert into INSCRIPCION values (5, 3, to_date('2019/10/20', 'YYYY/MM/DD'),5 ,'incidencia');
--##################################################################################################
--######################################## BORRAR DNI Y TELEFONO PROFESIONAL #######################
--##################################################################################################
alter table PROFESIONALES drop column DNI;
alter table PROFESIONALES drop column TELEFONO;

-- Guardar los cambios
commit;