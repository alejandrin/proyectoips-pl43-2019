package logica;

public enum EstadoFactura {
	RECIBIDA, COBRADA, CANCELADA, INCIDENCIA;
}
