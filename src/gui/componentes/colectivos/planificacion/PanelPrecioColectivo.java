package gui.componentes.colectivos.planificacion;

import javax.swing.JPanel;

import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import logica.dto.ColectivoDto;
import util.DtoFactory;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import javax.swing.JButton;
import java.awt.FlowLayout;

public class PanelPrecioColectivo extends JPanel {

	private static final long serialVersionUID = 1L;

	private ComboBoxColectivos cbColectivo;
	private JTextField txPrecio;
	private List<ColectivoDto> colectivos;
	private PanelPrecios panelAnterior;
	private JButton btnQuitar;
	private JPanel pnColectivo;
	private FiltroTextoPrecio ftp;

	/**
	 * Create the panel.
	 */
	public PanelPrecioColectivo(List<ColectivoDto> colectivos, PanelPrecios pp) {
		ftp = new FiltroTextoPrecio();
		this.colectivos = colectivos;
		panelAnterior = pp;
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		add(getPnColectivo());
		add(getBtnQuitar());

		cbColectivo.crearModelo();
	}

	private JComboBox<ColectivoDto> getCbColectivo() {
		if (cbColectivo == null) {
			cbColectivo = new ComboBoxColectivos(colectivos);
		}
		return cbColectivo;
	}

	private JTextField getTxPrecio() {
		if (txPrecio == null) {
			txPrecio = new JTextField();
			txPrecio.addKeyListener(ftp);
			txPrecio.setColumns(5);
		}
		return txPrecio;
	}

	public double getPrecio() {
		return Double.parseDouble(txPrecio.getText());
	}

	public ColectivoDto getColectivoSeleccionado() {
		return (ColectivoDto) cbColectivo.getSelectedItem();
	}

	private JButton getBtnQuitar() {
		if (btnQuitar == null) {
			btnQuitar = new JButton("Quitar");
			btnQuitar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ordenarQuitar();
				}
			});
		}
		return btnQuitar;
	}

	private void ordenarQuitar() {
		panelAnterior.quitarPanelPrecio(this);
	}

	private JPanel getPnColectivo() {
		if (pnColectivo == null) {
			pnColectivo = new JPanel();
			pnColectivo.add(getCbColectivo());
			pnColectivo.add(getTxPrecio());
		}
		return pnColectivo;
	}

	boolean comprobarValidez() {
		return !txPrecio.getText().isEmpty();
	}
	
	void actualizarModelo(List<ColectivoDto> colectivos) {
		this.colectivos = colectivos;
		cbColectivo.actualizarModelo(colectivos);
	}

	private class FiltroTextoPrecio extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent ke) {
			Character c = ke.getKeyChar();
			if (!Character.isDigit(c) && c != '.') {
				ke.consume();
				System.out.println("No se escribe " + c.toString());
			}
		}
	}

	private class ComboBoxColectivos extends JComboBox<ColectivoDto> {
		private static final long serialVersionUID = 1L;
		private List<ColectivoDto> colectivos;
		private DefaultComboBoxModel<ColectivoDto> modelo;

		public ComboBoxColectivos(List<ColectivoDto> colectivos) {
			this.colectivos = colectivos;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ColectivoDto col = (ColectivoDto) getSelectedItem();
					if (col.id == -1) {
						crearNuevoColectivo();
					}
				}
			});
		}

		private void crearNuevoColectivo() {
			String nombre = JOptionPane.showInputDialog(this, "Introduce nombre de nuevo colectivo:");
			nombre = nombre.substring(0, 1).toUpperCase().concat(nombre.substring(1, nombre.length()).toLowerCase());
			int id = colectivos.get(colectivos.size() - 1).id + 1;
			ColectivoDto nuevoColectivo = new ColectivoDto(id, nombre);
			try {
				if (DtoFactory.getColectivoByName(nuevoColectivo.nombre) != null)
					throw new IllegalArgumentException("Ya existe un colectivo con este nombre");
				if (!DtoFactory.saveColectivo(nuevoColectivo))
					throw new SQLException();
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,
						"Se ha producido un error. No se ha podido crear el nuevo colectivo.");
			} catch (IllegalArgumentException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}

			panelAnterior.actualizarColectivos();
		}

		private void actualizarModelo(List<ColectivoDto> colectivos) {
			this.colectivos = colectivos;
			crearModelo();
			setSelectedIndex(modelo.getSize() - 2);
		}

		private void crearModelo() {
			ColectivoDto[] array = (ColectivoDto[]) colectivos.toArray(new ColectivoDto[colectivos.size() + 1]);

			ColectivoDto otro = new ColectivoDto(-1, "Otro...");
			array[array.length - 1] = otro;

			int select = getSelectedIndex();

			modelo = new DefaultComboBoxModel<ColectivoDto>(array);
			setModel(modelo);
			setSelectedIndex(0);

			if (select != -1)
				setSelectedIndex(select);
		}
	}
}
