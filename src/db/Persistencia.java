package db;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import connection.ConnectionUtil;
import logica.Actividad;
import logica.EstadoActividad;
import logica.FacturaProfesor;
import logica.Inscripcion;
import logica.Profesional;
import logica.TransferenciaProfesional;
import logica.TransferenciaProfesor;
import logica.dto.ActividadDto;
import logica.dto.FacturaProfesorDto;
import logica.dto.IncidenciaDto;
import logica.dto.InscripcionDto;
import logica.dto.ProfesorDto;
import util.Dates;
import util.parser.Conf;

@SuppressWarnings("unused")
public class Persistencia {
	private static String CADENA_CONEXION = "jdbc:hsqldb:hsql://localhost/";
	private static String USER = "SA";
	private static String PASS = "";

	private static Connection getConnection() throws SQLException {
		if (DriverManager.getDriver(CADENA_CONEXION) == null)
			DriverManager.registerDriver(new org.hsqldb.jdbc.JDBCDriver());// JDBCDriver es la que est� en el jar
		return DriverManager.getConnection(CADENA_CONEXION, USER, PASS);
	}

	//////////////// RESPONSABLE DE FORMACI�N ////////////////

	/**
	 * 
	 * @param nombre
	 * @param objetivos
	 * @param contenidos
	 * @param fecha_abre_inscripcion
	 * @param fecha_cierra_inscripcion
	 * @param fecha_inicio_imparticion
	 * @param fecha_fin_imparticion
	 * @param num_plazas
	 * @param sesiones 
	 * @param estado
	 * @param idProfesor
	 * @param remuneracion_profesor
	 * @param horarios 
	 * @param profesores 
	 * @throws SQLException
	 */
	public static boolean planificarActividad(String nombre, String objetivos, String contenidos,
			String fecha_abre_inscripcion_year, String fecha_abre_inscripcion_month, String fecha_abre_inscripcion_day,	String fecha_abre_inscripcion_hour, String fecha_abre_inscripcion_minute,
			String fecha_cierra_inscripcion_year, String fecha_cierra_inscripcion_month,String fecha_cierra_inscripcion_day, String fecha_cierra_inscripcion_hour,String fecha_cierra_inscripcion_minute, 
			String fecha_inicio_imparticion_year, String fecha_inicio_imparticion_month, String fecha_inicio_imparticion_day, String fecha_inicio_imparticion_hour, String fecha_inicio_imparticion_minute,
			String fecha_fin_imparticion_year, String fecha_fin_imparticion_month, String fecha_fin_imparticion_day,String fecha_fin_imparticion_hour, String fecha_fin_imparticion_minute, 
			int num_plazas, String espacio, float precio,
			//double idProfesor, 
			//float remuneracion_profesor,
			//String horarios, String profesores
			int duracion1, int duracion2, String espacio2,
			double[] idSdeProfesores, double[] remProf, List<Object> sesiones) 
					throws SQLException {

		//Se calcula el estado en el que est� la actividad
		String estado="planificada";
		Date now=new Date();
		Date abreInscripcion=Dates.fromString(fecha_abre_inscripcion_day+"/"+fecha_abre_inscripcion_month+"/"+fecha_abre_inscripcion_year);
		Date cierraInscripcion=Dates.fromString(fecha_cierra_inscripcion_day+"/"+fecha_cierra_inscripcion_month+"/"+fecha_cierra_inscripcion_year);
		Date inicioImparticion=Dates.fromString(fecha_inicio_imparticion_day+"/"+fecha_inicio_imparticion_month+"/"+fecha_inicio_imparticion_year);

		if(Dates.isBefore(abreInscripcion, now))
			estado="en periodo de inscripci�n";


		//Se inserta la actividad en la bd
		String planificarActividad = "insert into actividades values " + "(?,'" + nombre + "','" + objetivos + "','"
				+ contenidos + "', " +

				"TO_DATE('" + fecha_abre_inscripcion_year + "/" + fecha_abre_inscripcion_month + "/"
				+ fecha_abre_inscripcion_day + " " + fecha_abre_inscripcion_hour + ":" + fecha_abre_inscripcion_minute
				+ ":00','YYYY/MM/DD HH:MI:SS')," +

				"TO_DATE('" + fecha_cierra_inscripcion_year + "/" + fecha_cierra_inscripcion_month + "/"
				+ fecha_cierra_inscripcion_day + " " + fecha_cierra_inscripcion_hour + ":"
				+ fecha_cierra_inscripcion_minute + ":00','YYYY/MM/DD HH:MI:SS')," +

				"TO_DATE('" + fecha_inicio_imparticion_year + "/" + fecha_inicio_imparticion_month + "/"
				+ fecha_inicio_imparticion_day + " " + fecha_inicio_imparticion_hour + ":"
				+ fecha_inicio_imparticion_minute + ":00','YYYY/MM/DD HH:MI:SS')," +

				"TO_DATE('" + fecha_fin_imparticion_year + "/" + fecha_fin_imparticion_month + "/"
				+ fecha_fin_imparticion_day + " " + fecha_fin_imparticion_hour + ":" + fecha_fin_imparticion_minute
				+ ":00','YYYY/MM/DD HH:MI:SS')," +

				+num_plazas + ",'" + estado + "','" + espacio + "'," + precio + ",0,0,0,"
				//+remuneracion_profesor+","
				+ 
				//idProfesor +
				"0"+
				//"," + remuneracion_profesor + 
				",null, null, null, "+duracion1 +","+ duracion2+",'"+espacio2+"')";


		ResultSet rs = ConnectionUtil.executeQuery("select max(id_actividad) from actividades", new Object[] {} );
		rs.next();
		int id_act = rs.getInt(1) + 1;
		int res = ConnectionUtil.executeUpdate(planificarActividad, id_act);

		//A�adir profesores
		for(int i=0;i<idSdeProfesores.length;i++) {
			//ConnectionUtil.executeUpdate("insert into ACTIVIDADESPROFESORES values("+idSdeProfesores[i]+","+id_act+")", new Object[] {idSdeProfesores[i]+"",id_act+""});
			ConnectionUtil.executeUpdate("insert into ACTIVIDADESPROFESORES values(?,?,?)", new Object[] {idSdeProfesores[i]+"",id_act+"",remProf[i]});
			ConnectionUtil.executeUpdate(Conf.getInstance().getProperty("SQL_UpdateGastosEstimados"), remProf[i], id_act);
		}

		//A�adir sesiones
		for(Object o:sesiones) {
			String espacioSesion=(String) ((Object[])o)[0];
			String fechaSesion=(String) ((Object[])o)[1];
			int duracionSesion=(int) ((Object[])o)[2];
			String query="insert into SESIONES values( " + id_act + " ,'" + espacioSesion + "', TO_DATE('" + fechaSesion + ":00', 'DD/MM/YYYY HH:MI:SS')," + duracionSesion + " )";
			ConnectionUtil.executeUpdate(query, new Object[] {});
		}
		
		boolean actualizado = false;
		if (res == 1) {
			System.out.println("Actividad creada.");
			actualizado = true;
		} 
		return actualizado;
	}

	public static ArrayList<ArrayList<Object>> getListaProfesores() throws SQLException{
		String query="select nombre, apellidos,id_profesor from profesores";
		ResultSet rs=ConnectionUtil.executeQuery(query, new Object[] {});
		ArrayList<ArrayList<Object>> datos=new ArrayList<ArrayList<Object>>();
		while(rs.next()) {
			ArrayList<Object>profesor=new ArrayList<Object>();
			profesor.add(rs.getString(1)+" "+rs.getString(2));
			profesor.add(rs.getDouble(3));
			datos.add(profesor);
		}
		rs.close();
		return datos;
	}

	///////////////////////// CERRAR ACTIVIDAD //////////////////////////7

	public static int getNumInscripciones(int id_actividad) {
		ResultSet rs;
		int id_act=0;
		try {
			rs = ConnectionUtil.executeQuery("select count(*) from INSCRIPCION where ID_ACTIVIDAD=?", id_actividad );
			rs.next();
			id_act = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id_act;
	}
	
	public static int getNumInsMul(int id_actividad) {
		ResultSet rs;
		int id_act=0;
		try {
			rs = ConnectionUtil.executeQuery("select count(l.id_profesional) from lista_inscritos l, inscripcion i "
					+ "where i.ID_ACTIVIDAD=? and i.id_inscripcion = l.id_inscripcion", id_actividad );
			rs.next();
			id_act = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id_act;
	}

	/// PROFESIONAL///
	public static JList<Actividad> getListaActividades() throws SQLException {
		JList<Actividad> list = new JList<Actividad>();

		Connection con = getConnection();
		Statement st = con.createStatement();
		DefaultListModel<Actividad> listModel = new DefaultListModel<Actividad>();

		String query = "select * from actividades";
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) {
			Actividad act;
			String nombre, objetivos, contenidos, espacio;

			Date fecha_abre_inscripcion, fecha_cierra_inscripcion, fecha_inicio_imparticion, fecha_fin_imparticion;

			int num_plazas, id_profesor, id_actividad;
			float precio, remuneracion_profesor;

			nombre = (String) rs.getObject("nombre");
			objetivos = (String) rs.getObject("objetivos");
			contenidos = (String) rs.getObject("contenidos");
			espacio = (String) rs.getObject("espacio");

			fecha_abre_inscripcion = (Date) rs.getObject("fecha_abre_inscripcion");
			fecha_cierra_inscripcion = (Date) rs.getObject("fecha_cierra_inscripcion");
			fecha_inicio_imparticion = (Date) rs.getObject("fecha_inicio_imparticion");
			fecha_fin_imparticion = (Date) rs.getObject("fecha_fin_imparticion");

			num_plazas = ((BigDecimal) rs.getObject("num_plazas")).intValue(); 
			id_profesor = ((BigDecimal) rs.getObject("id_profesor")).intValue(); 
			id_actividad = ((BigDecimal) rs.getObject("id_actividad")).intValue(); 
			precio = ((BigDecimal) rs.getObject("precio")).floatValue();
			remuneracion_profesor = ((BigDecimal) rs.getObject("remuneracion_profesor")).floatValue();




			act = new Actividad( nombre,objetivos,contenidos, espacio, fecha_abre_inscripcion, fecha_cierra_inscripcion, 
					fecha_inicio_imparticion, fecha_fin_imparticion, num_plazas, id_profesor, id_actividad, precio, remuneracion_profesor);

			listModel.addElement(act);
		}
		st.close();
		con.close();
		list.setModel(listModel);

		return list;

	}


	public static int getidProfesional() throws SQLException
	{
		Connection con = getConnection();

		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select max(id_profesional) from profesionales");
		rs.next();
		int id_pro = rs.getInt(1) + 1;
		st.close();
		con.close();
		return id_pro;
	} 
	public static boolean agregarProfesional(Profesional profesional) throws SQLException {
		String agregarProfesional = "insert into profesionales values " + 
				"( '" + profesional.getId_profesional()+ "','"+
				profesional.getNombre()+  "','" +
				profesional.getApellido()+ "','" +
				profesional.getEmail()+ "','" + profesional.getIdColectivo()
				///colectivo
				+ "')";

		; 

		Connection con = getConnection();
		Statement pst = con.createStatement( );
		int up=pst.executeUpdate(agregarProfesional);

		boolean actualizado = false;
		if (up == 1) {
			System.out.println("Profesional creado.");
			actualizado = true;
		}
		pst.close();
		con.close();
		return actualizado;

	}

	@SuppressWarnings("deprecation")
	public static boolean agregarTransferenciaProfesional(TransferenciaProfesional trans) throws SQLException
	{

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(trans.getFecha_transferencia());
		int year = calendar.get(Calendar.YEAR);
		//Add one to month {0 - 11}
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		String agregarTransferencia = "insert into transferencias_profesional values " + 
				"( '" + trans.getId()+ "','"+
				trans.getId_actividad()+"','" +
				trans.getCantidad_pagada_por_COIIPA() + "'," +
				"TO_DATE('" +year + "/" 
				+ month + "/"
				+ day+ " " +
				trans.getFecha_transferencia().getHours() + ":" 
				+ trans.getFecha_transferencia().getMinutes()
				+ ":00','YYYY/MM/DD HH:MI:SS')"+   ",'" +
				trans.getid_inscripcion()+  "','" + trans.id_profesional + "', '" +
				trans.getTipoPago() + "' )" ;

		Connection con = getConnection();
		Statement pst = con.createStatement( );
		int up=pst.executeUpdate(agregarTransferencia);

		boolean actualizado = false;
		if (up == 1) {
			System.out.println("TransferenciaProfesional creada.");
			actualizado = true;
		}
		pst.close();
		con.close();
		return actualizado;
	}
	
	
	public static boolean agregarTransferenciaProfesor(TransferenciaProfesor trans) throws SQLException {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(trans.getFecha_transferencia());
		int year = calendar.get(Calendar.YEAR);
		//Add one to month {0 - 11}
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		String agregarTransferencia = "insert into transferencias_profesor values " + 
				"( '" + trans.getId()+ "','"+
				trans.getId_actividad()+"','" +
				trans.id_profesor + "', '" +
				trans.getCantidad_pagada_por_COIIPA() + "','" +
				trans.getCantidad_pagada_por_profesional() +"'," +
				"TO_DATE('" +year + "/" 
				+ month + "/"
				+ day+ " " +
				trans.getFecha_transferencia().getHours() + ":" 
				+ trans.getFecha_transferencia().getMinutes()
				+ ":00','YYYY/MM/DD HH:MI:SS')"+   ",'" + 
				trans.getTipoPago() + "' )" ;

		Connection con = getConnection();
		Statement pst = con.createStatement( );
		int up=pst.executeUpdate(agregarTransferencia);

		boolean actualizado = false;
		if (up == 1) {
			System.out.println("TransferenciaProfesional creada.");
			actualizado = true;
		}
		pst.close();
		con.close();
		return actualizado;
		
	}
	
	public static boolean agregarFacturaProfesor(FacturaProfesor f) throws SQLException {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(f.getFecha());
		int year = calendar.get(Calendar.YEAR);
		//Add one to month {0 - 11}
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		String agregarTransferencia = "insert into facturas_profesores values " + 
				"( '" + f.getId()+ "','"+
				f.getId_profesor()+"','" +
				f.getPrecio_a_pagar() + "', '" +
				f.getPrecio_pagado() + "','" +
				f.getEstado() + "','" +
				f.getId_actividad() + "'," +
				"TO_DATE('" +year + "/" 
				+ month + "/"
				+ day+ " " +
				f.getFecha().getHours() + ":" 
				+ f.getFecha().getMinutes()
				+ ":00','YYYY/MM/DD HH:MI:SS')" + ")" ;

		Connection con = getConnection();
		Statement pst = con.createStatement( );
		int up=pst.executeUpdate(agregarTransferencia);

		boolean actualizado = false;
		if (up == 1) {
			System.out.println("TransferenciaProfesional creada.");
			actualizado = true;
		}
		pst.close();
		con.close();
		return actualizado;
		
	}

	@SuppressWarnings("deprecation")
	public static boolean agregarInscripcion(Inscripcion inscripcion, int idInscripcion) throws SQLException {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(inscripcion.fecha_inscripcion);
		int year = calendar.get(Calendar.YEAR);
		//Add one to month {0 - 11}
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);


		String agregarInscripcion = "insert into inscripcion values " 
				+ "('" +
				inscripcion.getActividad().getId_actividad()+  "'," +


					"TO_DATE('" +year + "/" 
					+ month + "/"
					+ day+ " " +
					inscripcion.fecha_inscripcion.getHours() + ":" 
					+ inscripcion.fecha_inscripcion.getMinutes()
					+ ":00','YYYY/MM/DD HH:MI:SS')" 
					+ ",'"	+ idInscripcion+ "' )" ;

		Connection con = getConnection();
		PreparedStatement pst = con.prepareStatement(agregarInscripcion);
		int up = pst.executeUpdate() ;
		boolean actualizado = false;
		if (up==1) {
			System.out.println("Inscripcion agregada.");
			actualizado = true;
		}
		pst.close();
		con.close();
		return actualizado;

	}

	public static int getNumInscritos(int idact) throws SQLException {
		Connection con = getConnection();
		Statement st = con.createStatement();
 		ResultSet rs = st.executeQuery("select count("+idact+") from inscripcion WHERE id_actividad="+idact);
		rs.next();
		int numins = rs.getInt(1);
		st.close();
		con.close();
		return numins;
	}


	//////////////// CERRAR ACTIVIDAD /////////////////////
	public static ArrayList<ActividadDto> getActividadesParaCerrar() throws SQLException{ 
		ResultSet rs=ConnectionUtil.executeQuery("select nombre, FECHA_ABRE_INSCRIPCION, FECHA_CIERRA_INSCRIPCION, FECHA_INICIO_IMPARTICION, FECHA_FIN_IMPARTICION, estado, num_plazas, id_actividad FROM ACTIVIDADES"
				, new Object[] {});
		ArrayList<ActividadDto> resultado =new ArrayList<ActividadDto>();
		while (rs.next()) {
			ActividadDto adto=new ActividadDto();
			ProfesorDto prof = new ProfesorDto();
			adto.nombre=rs.getString(1);
			adto.inicioInscripcion=rs.getTimestamp(2);
			adto.finInscripcion=rs.getTimestamp(3);
			adto.inicioActividad=rs.getTimestamp(4);
			adto.finActividad=rs.getTimestamp(5);
			adto.estado=EstadoActividad.getEnum(rs.getString(6).toUpperCase());
			adto.numPlazas=rs.getInt(7);
			adto.id=rs.getInt(8);
			//prof.nombre = rs.getString(7);
			//prof.apellidos = rs.getString(8);
			//prof.id = rs.getInt(9);
			//prof.dni = rs.getString(10);
			//adto.precio = rs.getDouble(11);
			//adto.profesor = prof;
			resultado.add(adto);
		}
		rs.close();
		return resultado;
	}
	public static void cerrarActividad(String nombre) throws SQLException {
		ConnectionUtil.executeUpdate("update actividades set estado='cerrada' where nombre=?", new Object[] {nombre});
	}

	public static boolean tieneIncidencias(String nombre) throws SQLException {
		ResultSet rs=ConnectionUtil.executeQuery("select a.NOMBRE, a.PRECIO, l.CANTIDAD_PAGADA, p.NOMBRE, p.APELLIDOS from INSCRIPCION i, LISTA_INSCRITOS l, PROFESIONALES p, ACTIVIDADES a where "
				//+ "i.ID_INSCRIPCION in (select id_inscripcion from LISTA_INSCRITOS where estado='incidencia')	and "
				+ "l.ID_PROFESIONAL=p.ID_PROFESIONAL and a.nombre=? and i.ID_ACTIVIDAD=a.ID_ACTIVIDAD and l.cantidad_pagada!=a.precio order by i.ID_ACTIVIDAD", new Object[] {nombre});
		if(rs.next())
			return true;
		else
			return false;
	}
	public static List<IncidenciaDto> getIncidencias() throws SQLException {
		ResultSet rs=ConnectionUtil.executeQuery("select a.NOMBRE, a.PRECIO, l.CANTIDAD_PAGADA, p.NOMBRE, p.APELLIDOS from INSCRIPCION i, LISTA_INSCRITOS l, PROFESIONALES p, ACTIVIDADES a where "
				//+ "i.ID_INSCRIPCION in (select id_inscripcion from LISTA_INSCRITOS where estado='incidencia')	and "
				+ "l.ID_PROFESIONAL=p.ID_PROFESIONAL and i.ID_ACTIVIDAD=a.ID_ACTIVIDAD and l.cantidad_pagada!=a.precio order by i.ID_ACTIVIDAD", new Object[] {});
		List<IncidenciaDto>lista=new ArrayList<IncidenciaDto>();
		while(rs.next()) {
			IncidenciaDto dto=new IncidenciaDto();
			dto.nombreActividad=rs.getString(1);
			dto.precioActividad=rs.getDouble(2);
			dto.cantidadPagada=rs.getDouble(3);
			dto.nombreProfesional=rs.getString(4);
			dto.apellidoProfesional=rs.getString(5);
			lista.add(dto);
		}
		return lista;
	}
	////////////////GENERAR ID INSCRIPCION /////////////////////

	public static int getIdInscripcion() throws SQLException {
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select max(id_inscripcion) from inscripcion ");
		rs.next();
		int idins = rs.getInt(1)+1;
		st.close();
		con.close();
		return idins;
	}
	//////////////// AGREGAR PROFESIONAL A UNA INSCRIPCION /////////////////////

	public static void agregarListaInscrito(int id_pro, int idInscripcion, double precio) throws SQLException {
		String agregarListaInscrito = "insert into lista_inscritos values " 
				+ "('" + id_pro +  "','"

					+ idInscripcion +  "','0.0','recibida'"+ ", " + precio + ", 'sin plaza'"+ ", "+ 0.0+

					" )" ;

		Connection con = getConnection();
		PreparedStatement pst = con.prepareStatement(agregarListaInscrito);
		int up = pst.executeUpdate() ;
		boolean actualizado = false;
		if (up==1) {
			System.out.println("Inscrito agregado.");
			actualizado = true;
		}
		pst.close();
		con.close();

	}

	public static int getNumListaInscritos(int id_ins) throws SQLException {
		Connection con = getConnection();
		Statement st = con.createStatement();
 		ResultSet rs = st.executeQuery("select count("+id_ins+") from lista_inscritos WHERE id_inscripcion="+id_ins);
		rs.next();
		int numins = rs.getInt(1);
		st.close();
		con.close();
		return numins;
	}


	


	
}
