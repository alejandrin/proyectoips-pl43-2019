package logica;

import java.util.Date;

public class FacturaProfesor {
	private int id;
	private int id_profesor;
	private int id_actividad;
	private double precio_a_pagar;
	private double precio_pagado;
	private Date fecha;
	private String estado;
	
	public FacturaProfesor(int i, int p, int a, double precio_a_pagar,
			double precio_pagado, Date fecha,String estado)
	{
		this.id = i;
		this.id_profesor = p;
		this.id_actividad = a;
		this.precio_a_pagar = precio_a_pagar;
		this.precio_pagado = precio_pagado;
		this.fecha = fecha;
		this.estado = estado;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getId_profesor() {
		return id_profesor;
	}

	public void setId_profesor(int id_profesor) {
		this.id_profesor = id_profesor;
	}

	public int getId_actividad() {
		return id_actividad;
	}

	public void setId_actividad(int id_actividad) {
		this.id_actividad = id_actividad;
	}

	public double getPrecio_a_pagar() {
		return precio_a_pagar;
	}
	public void setPrecio_a_pagar(double precio_a_pagar) {
		this.precio_a_pagar = precio_a_pagar;
	}
	public double getPrecio_pagado() {
		return precio_pagado;
	}
	public void setPrecio_pagado(double precio_pagado) {
		this.precio_pagado = precio_pagado;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
}
