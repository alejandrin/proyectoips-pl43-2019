package gui.secretaria;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionUtil;
import db.Persistencia;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JSpinner;
import gui.componentes.tipoDePago.PanelTipoPago;
import logica.EstadoFactura;
import logica.FacturaProfesor;
import logica.TransferenciaProfesional;
import logica.TransferenciaProfesor;
import logica.dto.ActividadDto;
import logica.dto.FacturaProfesorDto;
import logica.dto.InscripcionDto;
import logica.dto.ProfesionalDto;
import logica.dto.ProfesorDto;
import logica.dto.TransferenciaProfesionalDto;
import logica.dto.TransferenciaProfesorDto;
import util.DtoFactory;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class VentanaProfesores extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPaneProfesores;
	private JTable tablaProfesores;
	private JLabel lblProfesores;
	private JButton btnAgregarFactura;
	private JSpinner spinnerFactura;
	private JLabel lblFecha;
	private JSpinner spinnerDia;
	private JSpinner spinnerMes;
	private JSpinner spinnerYear;
	private PanelTipoPago panelTipoPago;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JScrollPane scrollPaneFacturas;
	private JTable tablaFacturas;
	private JLabel lblFacturas;
	private JButton botonAgregarPago;
	private JButton botonDevolucion;
	private JSpinner spinnerPago;
	private PanelTipoPago panelTipoPagoFactura;
	private JSpinner spinnerDiaFactura;
	private JLabel label_3;
	private JLabel label_4;
	private JSpinner spinnerMesFactura;
	private JLabel label_5;
	private JSpinner spinnerYearFactura;
	private JLabel label_6;
	private JScrollPane scrollPaneMovimientos;
	private JButton botonAtras;
	private JTable tablaMovimientos;
	private ActividadDto a;
	private JComboBox<ActividadDto> comboBoxActividades;
	private List<ActividadDto> actividades = new ArrayList<ActividadDto>();
	private DefaultComboBoxModel<ActividadDto> modeloActividades;
	private List<ProfesorDto> acuerdos;
    private DefaultTableModel modeloAcuerdos;
    private DefaultTableModel modeloFacturas;
    private DefaultTableModel modeloMovimientos;
    private JButton btnQuitarFactura;
    private List<TransferenciaProfesorDto> numeroTransaccionesProfesor;
    private List<TransferenciaProfesorDto> numeroTransaccionesTotales;
    private List<TransferenciaProfesorDto> TransaccionesProfesor;
    private List<FacturaProfesorDto> NumeroDefacturas;
    private List<FacturaProfesorDto> facturasProfesor;
    private int id;
    private ProfesorDto p;
    private FacturaProfesorDto f;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 * @param actividades 
	 * @param id 
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public VentanaProfesores(int id, List<ActividadDto> actividades, ProfesorDto p) throws SQLException, IOException {
		a= new DtoFactory().getActividadDto(id);
		this.p = p;
		this.id = id;
		this.actividades = actividades;
		numeroTransaccionesProfesor = new DtoFactory().getNumeroTransferenciasProfesor(id);
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		TransaccionesProfesor = new DtoFactory().getTransferenciasProfesor(id);
		NumeroDefacturas = new DtoFactory().getFacturasProfesor();
		crearModeloActividades();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				volver();
			}
		});
		acuerdos = new DtoFactory().getProfesoresByActividad(id);
		setTitle("Pagar a Profesor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1208, 1020);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getComboBoxActividades());
		contentPane.add(getScrollPaneProfesores());
		contentPane.add(getLblProfesores());
		contentPane.add(getBtnAgregarFactura());
		contentPane.add(getSpinnerFactura());
		contentPane.add(getLblFecha());
		contentPane.add(getSpinnerDia());
		contentPane.add(getSpinnerMes());
		contentPane.add(getSpinnerYear());
		contentPane.add(getPanelTipoPago());
		contentPane.add(getLabel());
		contentPane.add(getLabel_1());
		contentPane.add(getLabel_2());
		contentPane.add(getScrollPaneFacturas());
		contentPane.add(getLblFacturas());
		contentPane.add(getBotonAgregarPago());
		contentPane.add(getBotonDevolucion());
		contentPane.add(getSpinnerPago());
		contentPane.add(getPanelTipoPagoFactura());
		contentPane.add(getSpinnerDiaFactura());
		contentPane.add(getLabel_3());
		contentPane.add(getLabel_4());
		contentPane.add(getSpinnerMesFactura());
		contentPane.add(getLabel_5());
		contentPane.add(getSpinnerYearFactura());
		contentPane.add(getLabel_6());
		contentPane.add(getScrollPaneMovimientos());
		contentPane.add(getBotonAtras());
		contentPane.add(getBtnQuitarFactura());
		setLocationRelativeTo(null);
	}
	private void volver() {
		this.dispose();
	}
	
	private JComboBox<ActividadDto> getComboBoxActividades() {
		if (comboBoxActividades == null) {
			comboBoxActividades = new JComboBox<ActividadDto>();
			comboBoxActividades.setBounds(133, 23, 659, 37);
			comboBoxActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					a = (ActividadDto) modeloActividades.getSelectedItem();
					mostrarDatosActividad(a);

				}
			});
		}
		comboBoxActividades.setModel(modeloActividades);
		return comboBoxActividades;
	}
	
	protected void mostrarDatosActividad(ActividadDto a) {
		// TODO Auto-generated method stub
		try {
			VentanaProfesores vP = new VentanaProfesores(a.id, actividades,p);
			vP.setVisible(true);
			vP.setLocationRelativeTo(null);
			dispose();
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void crearModeloActividades() {
		ActividadDto[] a = new ActividadDto[actividades.size()];
		modeloActividades = new DefaultComboBoxModel<ActividadDto>(actividades.toArray(a));

	}
	
//	@SuppressWarnings("serial")
//	private void mostrarDatosTransferenciasProfesor() {
//		try {
//			List<TransferenciaProfesorDto> transferencias = new DtoFactory().getTransferenciasProfesor(id);
//			Object[] columnNames = new Object[] { "ID", "ID Actividad", "ID Profesional",
//			"cantidad pagada por COIIPA", "cantidad que se debe al profesor", "estado", "fecha", "Tipo de pago" };
//			modeloTransferenciasProfesor = new DefaultTableModel(new Object[transferencias.size()][8], columnNames) {
//				public boolean isCellEditable(int row, int column) {
//					return false;
//				}
//			};
//			a�adirFilasProfesor(transferencias);
//			tableProfesores.setModel(modeloTransferenciasProfesor);
//		} catch (Exception e) {
//			e.printStackTrace();
//			mostrarError();
//		}
//	}
	
	private JScrollPane getScrollPaneProfesores() {
		if (scrollPaneProfesores == null) {
			scrollPaneProfesores = new JScrollPane();
			scrollPaneProfesores.setBounds(60, 118, 1122, 105);
			scrollPaneProfesores.setViewportView(getTablaProfesores());
		}
		return scrollPaneProfesores;
	}
	private JTable getTablaProfesores() {
		if (tablaProfesores == null) {
			tablaProfesores = new JTable();
			tablaProfesores.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					if(tablaProfesores.getSelectedRow()!=-1)
					{
						FacturaProfesorDto trans;
						int t = 0;
						try {
							trans = new DtoFactory().getFacturaProfesor(acuerdos.get(tablaProfesores.getSelectedRow()).id,a.id);
							if(trans!=null)
							{
							try {
								VentanaProfesores vP;
								try {
									vP = new VentanaProfesores(a.id, actividades,acuerdos.get(tablaProfesores.getSelectedRow()));
									vP.setVisible(true);
									vP.setLocationRelativeTo(null);
									dispose();
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							}
						} catch (SQLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}	
					}
				}
			});
			mostrarDatosAcuerdosProfesor();
		}
		return tablaProfesores;
	}
	
	private void mostrarFacturasProfesor(ProfesorDto p) throws SQLException {
			FacturaProfesorDto trans = new DtoFactory().getFacturaProfesor(p.id, a.id);
			modeloFacturas.setValueAt(trans.id, 0, 0);
			modeloFacturas.setValueAt(trans.fecha, 0 , 2);
			modeloFacturas.setValueAt(trans.precio_pagado, 0, 4);
			modeloFacturas.setValueAt(p.nombre + " " + p.apellidos, 0, 1);
			modeloFacturas.setValueAt(trans.precio_a_pagar, 0 , 3);
			modeloFacturas.setValueAt(trans.estado, 0, 5);
	}
	
	private void mostrarDatosMovimientosProfesor(ProfesorDto p) {
		try {
			List<TransferenciaProfesorDto> trans = new DtoFactory().getTransferenciasProfesor(a.id);
			int a=0;
			for(int i=0;i<trans.size();i++)
			{
				if(trans.get(i).profesor.id==p.id && trans.get(i).cantidad_pagada_por_COIIPA<=0)
				{
					a++;
				}
			}
			Object[] columnNames = new Object[] { "Nombre Profesor","Cantidad pagada", "Fecha",  "Tipo", "ID" };
			modeloMovimientos = new DefaultTableModel(new Object[a][5], columnNames) {
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			mostrarMovimientosProfesor(p);
			tablaMovimientos.setModel(modeloMovimientos);
		} catch (Exception e) {
			e.printStackTrace();
			mostrarError();
		}
	}
	private void mostrarMovimientosProfesor(ProfesorDto p) throws SQLException {
		List<TransferenciaProfesorDto> trans = new DtoFactory().getTransferenciasProfesor(a.id);
		int a=0;
		for(int i=0;i<trans.size();i++)
		{
			if(trans.get(i).profesor.id ==p.id && trans.get(i).cantidad_pagada_por_COIIPA<=0)
			{
			modeloMovimientos.setValueAt(trans.get(i).fecha_transferencia, a , 2);
			modeloMovimientos.setValueAt(p.nombre + " " + p.apellidos, a, 0);
			modeloMovimientos.setValueAt(trans.get(i).id, a, 4);
			
			if(trans.get(i).cantidad_debida_profesor==0 && trans.get(i).cantidad_pagada_por_COIIPA<0)
			{
				modeloMovimientos.setValueAt(trans.get(i).cantidad_pagada_por_COIIPA, a , 1);
				modeloMovimientos.setValueAt("DEVOLUCION", a, 3);
			}
			else
			{
				modeloMovimientos.setValueAt(trans.get(i).cantidad_debida_profesor, a , 1);
				modeloMovimientos.setValueAt("PAGO", a, 3);
			}
			a++;
			}
		}
		
	}
	
	private void mostrarDatosAcuerdosProfesor() {
		try {
			Object[] columnNames = new Object[] { "Nombre Profesor", "Precio Acordado", "Fecha" };
			modeloAcuerdos = new DefaultTableModel(new Object[acuerdos.size()][3], columnNames) {
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			a�adirFilasAcuerdos(acuerdos);
			tablaProfesores.setModel(modeloAcuerdos);
		} catch (Exception e) {
			e.printStackTrace();
			mostrarError();
		}
	}
	
	private void mostrarDatosFacturasProfesor(ProfesorDto p) {
		try {
			Object[] columnNames = new Object[] { "ID Factura", "Nombre Profesor", "Fecha", "Cantidad total a Pagar", "Cantidad pagada hasta ahora", "Estado" };
			modeloFacturas = new DefaultTableModel(new Object[1][6], columnNames) {
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			mostrarFacturasProfesor(p);
			tablaFacturas.setModel(modeloFacturas);
		} catch (Exception e) {
			e.printStackTrace();
			mostrarError();
		}
	}
	
	private void a�adirFilasAcuerdos(List<ProfesorDto> acuerdos) throws SQLException {
		for (int i = 0; i < acuerdos.size(); i++) {
			ProfesorDto trans = acuerdos.get(i);;
			modeloAcuerdos.setValueAt(trans.nombre + " " + trans.apellidos, i, 0);
			modeloAcuerdos.setValueAt(getRemuneracionProfesor(trans.id, a.id), i, 1);
			modeloAcuerdos.setValueAt(a.getFechafinact(), i, 2);
		}
		
	}
	private void mostrarError() {
		JOptionPane.showMessageDialog(this, "Se ha producido un error", "Error", JOptionPane.ERROR_MESSAGE);
	}
	private JLabel getLblProfesores() {
		if (lblProfesores == null) {
			lblProfesores = new JLabel("Acuerdos:");
			lblProfesores.setBounds(60, 92, 82, 26);
		}
		return lblProfesores;
	}
	private JButton getBtnAgregarFactura() {
		if (btnAgregarFactura == null) {
			btnAgregarFactura = new JButton("Agregar Factura");
			btnAgregarFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(tablaProfesores.getSelectedRow()!=-1)
					{
					try {
						if(!hayFactura(acuerdos.get(tablaProfesores.getSelectedRow())))
						{
							if((int)spinnerYearFactura.getValue()<2019)
							{
								JOptionPane.showMessageDialog(null, "El a�o elegido es anterior al actual, cuidado");
							}
							if((int)spinnerYearFactura.getValue()>2019)
							{
								JOptionPane.showMessageDialog(null, "El a�o elegido es posterior al actual, cuidado");
							}
								EmitirFactura(a);
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Ya tenemos la factura esperando");
						}
							
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
				}
			});
			btnAgregarFactura.setBounds(85, 258, 147, 37);
		}
		return btnAgregarFactura;
	}
	
	private boolean hayFactura(ProfesorDto p) throws SQLException
	{
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		List<Integer> numeros = new ArrayList<Integer>();
		for(int i=0;i<numeroTransaccionesTotales.size();i++)
		{
			if(numeroTransaccionesTotales.get(i).profesor.id ==p.id && numeroTransaccionesTotales.get(i).actividad.equals(a))
				numeros.add(numeroTransaccionesTotales.get(i).id);
		}
		if(numeros.size()>0)
			return true;
		else
			return false;
	}
	
	private TransferenciaProfesorDto transferencia(int id, ProfesorDto p, ActividadDto a,
			double cantidad1, double cantidad2) {
		TransferenciaProfesorDto t = new TransferenciaProfesorDto();
		t.id = id;
		t.profesor = p; /// bug!

		t.profesor =  p;  

		t.actividad = a;
		t.cantidad_pagada_por_COIIPA = cantidad1;
		t.cantidad_debida_profesor = cantidad2;
		t.tipoPago = panelTipoPago.getTipoPago();
		return t;

	}
	private void EmitirFactura(ActividadDto a) throws NumberFormatException, ParseException {
		//crearTransferenciaPago(transferencia(numeroTransaccionesTotales.size(), acuerdos.get(tablaProfesores.getSelectedRow()), a,(int) spinnerFactura.getValue(), 0));
		try {
			crearFactura(NumeroDefacturas.size(),acuerdos.get(tablaProfesores.getSelectedRow()),a,(int) spinnerFactura.getValue(),0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 VentanaProfesores vP;
			try {
				vP = new VentanaProfesores(id, actividades,acuerdos.get(tablaProfesores.getSelectedRow()));
				vP.setVisible(true);
			} catch (SQLException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 dispose();
	}
private void crearFactura(int size, ProfesorDto profesorDto, ActividadDto a2, double value, double i) throws NumberFormatException, ParseException, SQLException {
		// TODO Auto-generated method stub
	SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
	Date d = objSDF.parse(Integer.parseInt(spinnerYearFactura.getValue().toString()) + "-"
			+ Integer.parseInt(spinnerMesFactura.getValue().toString()) + "-"
			+ Integer.parseInt(spinnerDiaFactura.getValue().toString()));
	FacturaProfesorDto trans = new FacturaProfesorDto();
	trans.actividad = a2;
	trans.profesor = profesorDto;
	trans.id = size;
	trans.precio_a_pagar = value;
	trans.precio_pagado = i;
	trans.fecha = d;
	FacturaProfesor f = new FacturaProfesor(trans.id, trans.profesor.id, trans.actividad.id, trans.precio_a_pagar, trans.precio_pagado, trans.fecha,"recibida");
	
	Persistencia.agregarFacturaProfesor(f);
	}



private void actualizarFactura(int id2, double precio_a_pagar, double precio_pagado) throws SQLException {
	// TODO Auto-generated method stub
	new DtoFactory().actualizarFactura(id2,precio_a_pagar,precio_pagado);
}
//	else
//	{
//		JOptionPane.showMessageDialog(null, "Ya tenemos la factura esperando");
//	}
	
	private void pagarProfesorJusto(ActividadDto a, FacturaProfesorDto f) throws NumberFormatException, SQLException, ParseException
	{
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		actualizarFactura(f.id, f.precio_a_pagar,f.precio_a_pagar);
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		crearTransferenciaPagoFactura(transferencia(numeroTransaccionesTotales.size()+1, f.profesor, a, 0, (int) spinnerPago.getValue()));
	}
	
	
	private void pagarProfesorMenos(ActividadDto a, FacturaProfesorDto f2) throws NumberFormatException, SQLException, ParseException
	{
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		actualizarFactura(f.id, f.precio_a_pagar,f.precio_pagado+(int)spinnerPago.getValue());
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		crearTransferenciaPagoFactura(transferencia(numeroTransaccionesTotales.size()+1, f2.profesor, a, 0, (int) spinnerPago.getValue()));
	}
	
	
	private void crearTransferenciaPago(TransferenciaProfesorDto t) throws SQLException, NumberFormatException, ParseException {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		Date d = objSDF.parse(Integer.parseInt(spinnerYear.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerMes.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerDia.getValue().toString()));
		TransferenciaProfesor trans = new TransferenciaProfesor(t.id+1, t.profesor.id, 0,
				t.actividad.id, t.cantidad_pagada_por_COIIPA, t.cantidad_debida_profesor, d, t.tipoPago);
		
		Persistencia.agregarTransferenciaProfesor(trans);
	
	}
	
	private void crearTransferenciaPagoFactura(TransferenciaProfesorDto t) throws SQLException, NumberFormatException, ParseException {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		Date d = objSDF.parse(Integer.parseInt(spinnerYearFactura.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerMesFactura.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerDiaFactura.getValue().toString()));
		TransferenciaProfesor trans = new TransferenciaProfesor(t.id, t.profesor.id, 0,
				t.actividad.id, t.cantidad_pagada_por_COIIPA, t.cantidad_debida_profesor, d, t.tipoPago);
		
		Persistencia.agregarTransferenciaProfesor(trans);
	
	}
	private void crearTransferenciaDevolucion(TransferenciaProfesorDto t) throws SQLException, NumberFormatException, ParseException {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		Date d = objSDF.parse(Integer.parseInt(spinnerYear.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerMes.getValue().toString()) + "-"
				+ Integer.parseInt(spinnerDia.getValue().toString()));
		TransferenciaProfesor trans = new TransferenciaProfesor(t.id, t.profesor.id, 0,
				t.actividad.id, t.cantidad_pagada_por_COIIPA, 0, d, t.tipoPago);

		Persistencia.agregarTransferenciaProfesor(trans);
	} 
	
	private JSpinner getSpinnerFactura() {
		if (spinnerFactura == null) {
			spinnerFactura = new JSpinner();
			spinnerFactura.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spinnerFactura.setBounds(325, 262, 47, 29);
		}
		return spinnerFactura;
	}
	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha:");
			lblFecha.setBounds(266, 341, 46, 14);
		}
		return lblFecha;
	}
	private JSpinner getSpinnerDia() {
		if (spinnerDia == null) {
			spinnerDia = new JSpinner();
			spinnerDia.setModel(new SpinnerNumberModel(1, 1, 31, 1));
			spinnerDia.setBounds(366, 338, 59, 20);
		}
		return spinnerDia;
	}
	private JSpinner getSpinnerMes() {
		if (spinnerMes == null) {
			spinnerMes = new JSpinner();
			spinnerMes.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			spinnerMes.setBounds(520, 338, 77, 20);
		}
		return spinnerMes;
	}
	private JSpinner getSpinnerYear() {
		if (spinnerYear == null) {
			spinnerYear = new JSpinner();
			spinnerYear.setModel(new SpinnerNumberModel(2010, 2010, 2099, 1));
			spinnerYear.setBounds(676, 338, 191, 20);
		}
		return spinnerYear;
	}
	private PanelTipoPago getPanelTipoPago() {
		if (panelTipoPago == null) {
			panelTipoPago = new PanelTipoPago();
			panelTipoPago.setBounds(954, 237, 228, 87);
			panelTipoPago.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelTipoPago;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("D\u00EDa:");
			label.setBounds(326, 337, 46, 23);
		}
		return label;
	}
	private JLabel getLabel_1() {
		if (label_1 == null) {
			label_1 = new JLabel("Mes:");
			label_1.setBounds(448, 341, 46, 14);
		}
		return label_1;
	}
	private JLabel getLabel_2() {
		if (label_2 == null) {
			label_2 = new JLabel("A\u00F1o:");
			label_2.setBounds(620, 341, 46, 14);
		}
		return label_2;
	}
	private JScrollPane getScrollPaneFacturas() {
		if (scrollPaneFacturas == null) {
			scrollPaneFacturas = new JScrollPane();
			scrollPaneFacturas.setBounds(60, 413, 1122, 105);
			scrollPaneFacturas.setViewportView(getTablaFacturas());
		}
		return scrollPaneFacturas;
	}
	private JTable getTablaFacturas() {
		if (tablaFacturas == null) {
			tablaFacturas = new JTable();
			if(p!=null)
			{
				mostrarDatosFacturasProfesor(p);
			}
		}
		return tablaFacturas;
	}
	private JLabel getLblFacturas() {
		if (lblFacturas == null) {
			lblFacturas = new JLabel("Facturas:");
			lblFacturas.setBounds(60, 376, 66, 26);
		}
		return lblFacturas;
	}
	private JButton getBotonAgregarPago() {
		if (botonAgregarPago == null) {
			botonAgregarPago = new JButton("Agregar Pago");
			botonAgregarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(tablaFacturas.getSelectedRow()!=-1)
					{
						DefaultTableModel modelo1= modeloFacturas;
						if(f.precio_a_pagar!=f.precio_pagado)
						{
						if((int) spinnerPago.getValue()!=0)
						{
						if((f.precio_a_pagar-f.precio_pagado)==(int) spinnerPago.getValue())
						{
							try {
								if((int)spinnerYear.getValue()<2019)
								{
									JOptionPane.showMessageDialog(null, "El a�o elegido es anterior al actual, cuidado");
								}
								if((int)spinnerYear.getValue()>2019)
								{
									JOptionPane.showMessageDialog(null, "El a�o elegido es posterior al actual, cuidado");
								}
								pagarProfesorJusto(a, f);
							} catch (NumberFormatException | SQLException | ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							VentanaProfesores vP;
							try {
								vP = new VentanaProfesores(id, actividades,p);
								vP.setVisible(true);
							} catch (SQLException | IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							 dispose();
						}
						else if((f.precio_a_pagar-f.precio_pagado)>(int) spinnerPago.getValue())
						{
							try {
								if((int)spinnerYear.getValue()<2019)
								{
									JOptionPane.showMessageDialog(null, "El a�o elegido es anterior al actual, cuidado");
								}
								if((int)spinnerYear.getValue()>2019)
								{
									JOptionPane.showMessageDialog(null, "El a�o elegido es posterior al actual, cuidado");
								}
								pagarProfesorMenos(a, f);
							} catch (NumberFormatException | SQLException | ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							VentanaProfesores vP;
							try {
								vP = new VentanaProfesores(id, actividades,p);
								vP.setVisible(true);
							} catch (SQLException | IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							 dispose();
						}
						else
						{
							JOptionPane.showMessageDialog(null, "No le podemos pagar mas dinero del que le debemos al profesor");
						}
						}
						else
						{
							JOptionPane.showMessageDialog(null, "No podemos agregar un Pago que valga 0 euros");
						}
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Ya se le ha pagado el dinero al profesor");
					}
				}
			});
			botonAgregarPago.setFont(new Font("Tahoma", Font.PLAIN, 20));
			botonAgregarPago.setBounds(102, 529, 210, 37);
		}
		return botonAgregarPago;
	}
	private JButton getBotonDevolucion() {
		if (botonDevolucion == null) {
			botonDevolucion = new JButton("A\u00F1adir Devoluci\u00F3n");
			botonDevolucion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(tablaFacturas.getSelectedRow()!=-1)
					{
					if((int) spinnerPago.getValue() !=0)
					{
					if((f.precio_pagado-((int) spinnerPago.getValue())>=0) && (int) spinnerPago.getValue() >0)
					{
					try {
						devolverProfesor(a,f);
						VentanaProfesores vP;
						try {
							vP = new VentanaProfesores(id, actividades,p);
							vP.setVisible(true);
						} catch (SQLException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 dispose();
					} catch (NumberFormatException | SQLException | ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "La devolucion seria mayor que el total pagado por el profesor");
					}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "No podemos crear una devolucion de valor 0 euros");
					}
					}
				}
			});
			botonDevolucion.setFont(new Font("Tahoma", Font.PLAIN, 20));
			botonDevolucion.setBounds(102, 577, 210, 43);
		}
		return botonDevolucion;
	}
	
	private void devolverProfesor(ActividadDto a, FacturaProfesorDto f) throws NumberFormatException, SQLException, ParseException
	{
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		actualizarFactura(f.id, f.precio_a_pagar,f.precio_pagado-(int)spinnerPago.getValue());
		numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		crearTransferenciaDevolucion((transferencia(numeroTransaccionesTotales.size()+1, f.profesor, a, -(int) spinnerPago.getValue(),0 )));
		
	}

	private JSpinner getSpinnerPago() {
		if (spinnerPago == null) {
			spinnerPago = new JSpinner();
			spinnerPago.setFont(new Font("Tahoma", Font.PLAIN, 20));
			spinnerPago.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spinnerPago.setBounds(350, 554, 97, 37);
		}
		return spinnerPago;
	}
	private PanelTipoPago getPanelTipoPagoFactura() {
		if (panelTipoPagoFactura == null) {
			panelTipoPagoFactura = new PanelTipoPago();
			panelTipoPagoFactura.setBounds(952, 533, 230, 87);
			panelTipoPagoFactura.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelTipoPagoFactura;
	}
	private JSpinner getSpinnerDiaFactura() {
		if (spinnerDiaFactura == null) {
			spinnerDiaFactura = new JSpinner();
			spinnerDiaFactura.setModel(new SpinnerNumberModel(1, 1, 31, 1));
			spinnerDiaFactura.setBounds(366, 640, 59, 20);
		}
		return spinnerDiaFactura;
	}
	private JLabel getLabel_3() {
		if (label_3 == null) {
			label_3 = new JLabel("D\u00EDa:");
			label_3.setBounds(326, 639, 46, 23);
		}
		return label_3;
	}
	private JLabel getLabel_4() {
		if (label_4 == null) {
			label_4 = new JLabel("Mes:");
			label_4.setBounds(435, 643, 46, 14);
		}
		return label_4;
	}
	private JSpinner getSpinnerMesFactura() {
		if (spinnerMesFactura == null) {
			spinnerMesFactura = new JSpinner();
			spinnerMesFactura.setModel(new SpinnerNumberModel(1, 1, 12, 1));
			spinnerMesFactura.setBounds(478, 640, 77, 20);
		}
		return spinnerMesFactura;
	}
	private JLabel getLabel_5() {
		if (label_5 == null) {
			label_5 = new JLabel("A\u00F1o:");
			label_5.setBounds(565, 643, 46, 14);
		}
		return label_5;
	}
	private JSpinner getSpinnerYearFactura() {
		if (spinnerYearFactura == null) {
			spinnerYearFactura = new JSpinner();
			spinnerYearFactura.setModel(new SpinnerNumberModel(2010, 2010, 2099, 1));
			spinnerYearFactura.setBounds(601, 640, 191, 20);
		}
		return spinnerYearFactura;
	}
	private JLabel getLabel_6() {
		if (label_6 == null) {
			label_6 = new JLabel("Movimientos:");
			label_6.setBounds(60, 722, 807, 31);
		}
		return label_6;
	}
	private JScrollPane getScrollPaneMovimientos() {
		if (scrollPaneMovimientos == null) {
			scrollPaneMovimientos = new JScrollPane();
			scrollPaneMovimientos.setBounds(60, 750, 1122, 120);
			scrollPaneMovimientos.setViewportView(getTablaMovimientos());
			if(p!=null)
			{
				try {
					f = new DtoFactory().getFacturaProfesor(p.id, a.id);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mostrarDatosMovimientosProfesor(p);
			}
		}
		return scrollPaneMovimientos;
	}
	private JButton getBotonAtras() {
		if (botonAtras == null) {
			botonAtras = new JButton("Atras");
			botonAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					volver();
				}
			});
			botonAtras.setBounds(65, 917, 82, 29);
		}
		return botonAtras;
	}
	private JTable getTablaMovimientos() {
		if (tablaMovimientos == null) {
			tablaMovimientos = new JTable();
		}
		return tablaMovimientos;
	}
	private JButton getBtnQuitarFactura() {
		if (btnQuitarFactura == null) {
			btnQuitarFactura = new JButton("Quitar Factura");
			btnQuitarFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						if(tablaFacturas.getSelectedRow()!=-1)
						{
							new DtoFactory().cancelarFactura(f.id);
							f=null;
						if(modeloMovimientos.getRowCount()!=0)
						{
							quitarTodosLosMovimientos(numeroTransaccionesTotales.get((int) tablaFacturas.getValueAt(0, 0)));
						}
						new DtoFactory().cancelarMovimientoProfesor(
								(int) tablaFacturas.getValueAt(0, 0));
						 VentanaProfesores vP;
							try {
								vP = new VentanaProfesores(id, actividades,null);
								vP.setVisible(true);
							} catch (SQLException | IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							 dispose();
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			btnQuitarFactura.setBounds(926, 687, 133, 23);
		}
		return btnQuitarFactura;
	}
	
	protected void quitarTodosLosMovimientos(TransferenciaProfesorDto t) throws SQLException {
		List<TransferenciaProfesorDto>numeroTransaccionesTotales = new DtoFactory().getNumeroTransferenciasProfesorTotales();
		List<Integer> numeros = new ArrayList<Integer>();
		for(int i=0;i<numeroTransaccionesTotales.size();i++)
		{
			if(numeroTransaccionesTotales.get(i).profesor.id ==t.profesor.id && numeroTransaccionesTotales.get(i).cantidad_pagada_por_COIIPA<=0 && numeroTransaccionesTotales.get(i).actividad.id==a.id)
				numeros.add(numeroTransaccionesTotales.get(i).id);
		}
		for(int j=0;j<numeros.size();j++)
		{
			new DtoFactory().cancelarMovimientoProfesor(numeros.get(j));
		}
	}
	public double getRemuneracionProfesor(int id_profesor, int id_actividad) throws SQLException {
		ResultSet rs;
		String query = "SELECT remuneracion from actividadesprofesores where id_actividad= "+id_actividad+" and id_profesor= "+id_profesor;

		rs = ConnectionUtil.executeQuery(query);

		rs.next();
		return rs.getDouble("remuneracion");

	}
}
