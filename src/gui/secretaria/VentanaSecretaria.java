package gui.secretaria;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.dto.ActividadDto;
import logica.dto.InscripcionDto;
import util.DtoFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.Toolkit;

@SuppressWarnings("unused")
public class VentanaSecretaria extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pnPrincipal;
	private JButton btDatosActividad;
	private JFrame ventanaAnterior;
	private JPanel pnOpciones;
	private JPanel pnAtras;
	private JButton btAtras;
	private JPanel panelCentral;
	private JButton btnConsultarListaDe;
	private JButton btnCerrarActividad;

	/**
	 * Create the frame.
	 */
	public VentanaSecretaria(JFrame ventanaAnterior) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaSecretaria.class.getResource("/img/logo-COIIPA.png")));
		setResizable(false);
		setTitle("Secretaria");
		this.ventanaAnterior = ventanaAnterior;
		
		this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                volver();
            }
        });
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 454, 217);
		pnPrincipal = new JPanel();
		pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnPrincipal);
		pnPrincipal.setLayout(new BorderLayout(0, 0));
		pnPrincipal.add(getPnOpciones(), BorderLayout.NORTH);
		pnPrincipal.add(getPnAtras(), BorderLayout.SOUTH);
		pnPrincipal.add(getPanelCentral(), BorderLayout.CENTER);
	}

	private JButton getBtDatosActividad() {
		if (btDatosActividad == null) {
			btDatosActividad = new JButton("Consultar estado de una actividad");
			btDatosActividad.setMnemonic('C');
			btDatosActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentanaActividad();
				}
			});
		}
		return btDatosActividad;
	}
	
	private void mostrarVentanaActividad() {
		try{
			List<ActividadDto> a  = new DtoFactory().getActividades();
			VentanaActividad frame = new VentanaActividad(a) ;
			frame.setLocationRelativeTo(this);
			frame.setVisible(true);
		}catch(IOException | SQLException e){
			e.printStackTrace();
			mostrarError();
		}
	}
	
	private void mostrarError() {
		JOptionPane.showMessageDialog(this, "Se ha producido un error.", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	private int preguntarIDActividad() {
		try {
//			int id = Integer.parseInt(JOptionPane.showInputDialog(this, "Introduzca el ID de la actividad de que deseas "
//								  + "consultar:","ID Actividad", JOptionPane.QUESTION_MESSAGE));
//			return id;
		}catch(NumberFormatException nfe) {
			JOptionPane.showMessageDialog(this, "El ID debe ser un n�mero.", "Error con el formato", JOptionPane.ERROR_MESSAGE);
		}
		return -1;
	}
	private JPanel getPnOpciones() {
		if (pnOpciones == null) {
			pnOpciones = new JPanel();
			pnOpciones.add(getBtDatosActividad());
		}
		return pnOpciones;
	}
	private JPanel getPnAtras() {
		if (pnAtras == null) {
			pnAtras = new JPanel();
			pnAtras.add(getBtAtras());
		}
		return pnAtras;
	}
	private JButton getBtAtras() {
		if (btAtras == null) {
			btAtras = new JButton("Atr\u00E1s");
			btAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					volver();
				}
			});
			btAtras.setMnemonic('A');
		}
		return btAtras;
	}
	
	private void volver() {
		ventanaAnterior.setLocationRelativeTo(this);
		ventanaAnterior.setVisible(true);
		this.dispose();
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			panelCentral.add(getBtnConsultarListaDe());
			panelCentral.add(getBtnCerrarActividad());
		}
		return panelCentral;
	}
	
	private void monstrarVentanaProfesionales(int id)
	{
		try{
			List<ActividadDto> a  = new DtoFactory().getActividades();
			ActividadDto act = new DtoFactory().getActividadDto(id);
			if(act != null) {
				VentanaProfesionales frame = new VentanaProfesionales(id, a) ;
				frame.setLocationRelativeTo(this);
				frame.setVisible(true);
			}else {
				JOptionPane.showMessageDialog(this, "No existe ninguna actividad registrada con este ID.",
											  "ID inexistente", JOptionPane.INFORMATION_MESSAGE);
			}
		}catch(IOException | SQLException e){
			mostrarError();
		}
	}
	private JButton getBtnConsultarListaDe() {
		if (btnConsultarListaDe == null) {
			btnConsultarListaDe = new JButton("Consultar lista de clientes");
			btnConsultarListaDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
						monstrarVentanaProfesionales(1);
				}
			});
		}
		return btnConsultarListaDe;
	}
	
	/*
	 * ########################### Alejandro ###########################
	 */
	private JButton getBtnCerrarActividad() {
		if (btnCerrarActividad == null) {
			btnCerrarActividad = new JButton("Cerrar Actividad");
			btnCerrarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentanaCerrarActividad();
				}


			});
		}
		return btnCerrarActividad;
	}
	public static void mostrarVentanaCerrarActividad() {
		VentanaCerrarActividad frame=new VentanaCerrarActividad();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
}
