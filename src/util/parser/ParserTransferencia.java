package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.ActividadDto;
import logica.dto.InscripcionDto;
import logica.dto.ProfesionalDto;
import logica.dto.ProfesorDto;
import logica.dto.TransferenciaProfesionalDto;
import logica.dto.TransferenciaProfesorDto;
import util.DtoFactory;

public class ParserTransferencia {
	public List<TransferenciaProfesionalDto> parseTransferenciasProfesional(ResultSet rs) throws SQLException{
		List<TransferenciaProfesionalDto> trans = new ArrayList<TransferenciaProfesionalDto>();
		while(rs.next())
			trans.add(parseTransferenciaProfesional(rs));
		return trans;
	}
	
	public TransferenciaProfesionalDto parseTransferenciaProfesional(ResultSet rs) throws SQLException {
		ActividadDto act = new ActividadDto();
		InscripcionDto ins = new InscripcionDto();
		ProfesionalDto pro = new ProfesionalDto();
		TransferenciaProfesionalDto trans = new TransferenciaProfesionalDto();
		if(!rs.isBeforeFirst() || rs.next()) {
			ins.id = rs.getInt("id_inscripcion");
			
			pro.id = rs.getInt("id_profesional");
			pro.nombre = rs.getString("nombre");
			pro.apellidos = rs.getString("apellidos");
			act.id = rs.getInt("id_actividad");
			
			ins.cantidadPagada = rs.getDouble("cantidad_pagada");
			ins.profesional=pro;
			trans.inscripcion = ins;
			trans.actividad = act;
			trans.fecha_transferencia = rs.getDate("fecha_transferencia");
			trans.id = rs.getInt("id_transferencia_profesional");
			trans.cantidad_pagada_por_COIIPA =rs.getDouble("cantidad_pagada_por_COIIPA");
 			trans.tipoPago = rs.getString("tipo");
			
		}
		return trans;
	}
	
	public List<TransferenciaProfesionalDto> parseTransferenciasMulIns(ResultSet rs) throws SQLException{
		List<TransferenciaProfesionalDto> trans = new ArrayList<TransferenciaProfesionalDto>();
		while(rs.next())
			trans.add(parseTransferenciaMulIns(rs));
		return trans;
	}
	
	public TransferenciaProfesionalDto parseTransferenciaMulIns(ResultSet rs) throws SQLException {
		ActividadDto act = new ActividadDto();
		InscripcionDto ins = new InscripcionDto();
		ProfesionalDto pro = new ProfesionalDto();
		TransferenciaProfesionalDto trans = new TransferenciaProfesionalDto();
		if(!rs.isBeforeFirst() || rs.next()) {
			ins.id = rs.getInt("id_inscripcion");
			
			pro.id = rs.getInt("id_profesional");
 			act.id = rs.getInt("id_actividad");
			
 			trans.inscripcion = ins;
			trans.actividad = act;
			trans.fecha_transferencia = rs.getDate("fecha_transferencia");
			trans.cantidad_pagada = rs.getDouble("cantidad_pagada_por_COIIPA");

			trans.id = rs.getInt("id_transferencia_profesional");
			trans.cantidad_pagada_por_COIIPA =rs.getDouble("cantidad_pagada_por_COIIPA");
			trans.cantidad_pagada_por_profesional =0.0;
			trans.tipoPago = rs.getString("tipo");
			if(trans.cantidad_pagada<=0) {
				trans.movimiento= "devolucion";
			}else {
				trans.movimiento = "pago";
			}
		}
		return trans;
	}
}
