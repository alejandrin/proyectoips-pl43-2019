package logica.dto;

import java.util.Date;

import logica.EstadoFactura;

public class FacturaProfesorDto {
	public int id;
	public ProfesorDto profesor;
	public ActividadDto actividad;
	public double precio_a_pagar;
	public double precio_pagado;
	public EstadoFactura estado;
	public Date fecha;
}
