package logica.dto;

public class DatosEmpresaDto {

	public int id_empresa;
	public String nombre;
	public String representante;
	public String email;
	public String telefono;
	public String cif;
	
	
	@Override
	public String toString() {
		return nombre;
	}
}
