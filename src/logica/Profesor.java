package logica;

public class Profesor {
	String nombre, apellido, email,tfl;
	int dni, id_profesor, id_colectivo;
	public Profesor(String nombre, String apellido, String email,  int id_profesor) {
 		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		
		this.id_profesor = id_profesor;
	}
	
	public Profesor(String nombre, String apellido, String email,  int id_profesor, int id_colectivo) {
 		this(nombre, apellido, email, id_profesor);
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTfl() {
		return tfl;
	}
	public void setTfl(String tfl) {
		this.tfl = tfl;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public int getId_profesor() {
		return id_profesor;
	}
	public void setId_profesor(int id_profesor) {
		this.id_profesor = id_profesor;
	}
}
