package gui.componentes.imparticion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import util.DtoFactory;

@SuppressWarnings("serial")
public class DialogNuevoProfesor extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txNombre;
	private JTextField txApellidos;
	private JTextField txEmail;
	private JTextField txTelefono;
	private JTextField txDni;
	private PanelImparticionEmpresa ventanaAnterior;
	
	/**
	 * Create the dialog.
	 */
	public DialogNuevoProfesor(PanelImparticionEmpresa panelImparticionEmpresa) {
		ventanaAnterior = panelImparticionEmpresa;
		
		setTitle("Registrar nuevo profesor");
		setBounds(100, 100, 391, 261);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txDni = new JTextField();
			txDni.setBounds(183, 134, 157, 26);
			contentPanel.add(txDni);
			txDni.setColumns(10);
		}
		{
			txTelefono = new JTextField();
			txTelefono.setBounds(183, 102, 157, 26);
			contentPanel.add(txTelefono);
			txTelefono.setColumns(10);
		}
		{
			txEmail = new JTextField();
			txEmail.setBounds(183, 70, 157, 26);
			contentPanel.add(txEmail);
			txEmail.setColumns(10);
		}
		{
			txApellidos = new JTextField();
			txApellidos.setBounds(183, 38, 157, 26);
			contentPanel.add(txApellidos);
			txApellidos.setColumns(10);
		}
		{
			txNombre = new JTextField();
			txNombre.setBounds(183, 6, 157, 26);
			contentPanel.add(txNombre);
			txNombre.setColumns(10);
		}
		{
			JLabel lbCif = new JLabel("DNI (opcional):");
			lbCif.setBounds(15, 134, 157, 26);
			contentPanel.add(lbCif);
		}
		{
			JLabel lblTelefonoopcional = new JLabel("Telefono (opcional):");
			lblTelefonoopcional.setBounds(15, 102, 157, 26);
			contentPanel.add(lblTelefonoopcional);
		}
		{
			JLabel lbEmail = new JLabel("Email:");
			lbEmail.setBounds(15, 70, 157, 26);
			contentPanel.add(lbEmail);
		}
		{
			JLabel lbRepresentante = new JLabel("Apellidos:");
			lbRepresentante.setBounds(15, 38, 157, 26);
			contentPanel.add(lbRepresentante);
		}
		{
			JLabel lbNombre = new JLabel("Nombre:");
			lbNombre.setBounds(15, 6, 157, 26);
			contentPanel.add(lbNombre);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String nombre = txNombre.getText();
						String apellidos = txApellidos.getText();
						String email = txEmail.getText();
						String dni = txDni.getText();
						String tlf = txTelefono.getText();
						if(nombre.isEmpty() || apellidos.isEmpty()||email.isEmpty())
						{

							JOptionPane.showMessageDialog(null,
									"Campos sin completar.",
									"Lo sentimos", JOptionPane.WARNING_MESSAGE);
						}
						else {
							registrarProfesor(nombre,apellidos,email,tlf,dni);
							JOptionPane.showMessageDialog(null, "Profesor registrado.",
									"Cambios actualizados con exito.", JOptionPane.INFORMATION_MESSAGE);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

	}
	
	private void registrarProfesor(String nombre,String apellidos,String email,String tlf,String dni) {
		try {
			int id_pro = new DtoFactory().getIdprofesor();
			
			new DtoFactory().registrarprofesor(id_pro,nombre,apellidos,email,tlf,dni);
			
			ventanaAnterior.aņadirProfesor(id_pro);
			
			this.dispose();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se ha producido un error en la base de datos.",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
