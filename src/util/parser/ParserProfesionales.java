package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.ProfesionalDto;

public class ParserProfesionales {


	public List<ProfesionalDto> parseProfesionales(ResultSet rs) throws SQLException{
		List<ProfesionalDto> profesionales = new ArrayList<ProfesionalDto>();
		
		while(rs.next()) {
			profesionales.add(parseProfesional(rs));
		}
		
		return profesionales;
	}
	
	public ProfesionalDto parseProfesional(ResultSet rs) throws SQLException {
		ProfesionalDto prof = new ProfesionalDto();
		
 		prof.id = rs.getInt("id_profesional");
		prof.nombre = rs.getString("nombre");
		prof.apellidos = rs.getString("apellidos");
		
 		return prof;
	}
	
}
