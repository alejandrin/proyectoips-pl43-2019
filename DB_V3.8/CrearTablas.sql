--##################################################################################################
--######################################## v3.4 varias sesiones ##################
--##################################################################################################
create table sesiones(
	id_actividad decimal(10,2) not null,
	espacio varchar(200)
	fecha date,
	duracion decimal(10,2),
	constraint fk_sesiones_actividades foreign key(id_actividad) references actividades(id_actividad)
)

--##################################################################################################
--######################################## cambios V18 para historia 1 2019/11/18 ##################
--##################################################################################################
-- �sta tabla se crea porque ahora las actividades tienen muchos profesores
create table actividadesProfesores(
	id_profesor decimal(10,2) not null,
	id_actividad decimal(10,2) not null,
	
	constraint pk_actividadesProfesores primary key(id_profesor,id_actividad),
	constraint fk_actividadesProfesores_profesores foreign key(id_profesor) references profesores(id_profesor),
	constraint fk_actividadesProfesores_actividades foreign key(id_actividad) references actividades(id_actividad)
);--##################################################################################################
--##################################################################################################
--##################################################################################################


create table profesores(
	id_profesor decimal(10,2) not null,
	
	nombre varchar(100) not null,
	apellidos varchar(100) not null,
	email varchar(100) not null,
	telefono varchar(100),
	dni varchar(9) unique,
	
	constraint pk_profesor primary key(id_profesor)
);
create table profesionales(
	id_profesional decimal(10,2) not null,
	
	nombre varchar(100) not null,
	apellidos varchar(100) not null,
	email varchar(100) not null,
	telefono varchar(100),
	dni varchar(9) unique,-- en la aplicaci�n se deber� controlar que no se inserte un dni que ya existe en la bd
	
	constraint pk_profesionales primary key(id_profesional)
);
create table actividades(
	id_actividad decimal(10,2) not null,
	
	nombre varchar(100) unique,
	objetivos varchar(5000),
	contenidos varchar(1000),
	fecha_abre_inscripcion date,
	fecha_cierra_inscripcion date,
	fecha_inicio_imparticion date,
	fecha_fin_imparticion date,
	num_plazas decimal(10,2),
	estado varchar(100) not null,
	espacio varchar(100),
	precio decimal(10,2),
	
	ingresos_confirmados decimal(10,2),
	gastos_confirmados decimal(10,2),
	ingresos_estimados decimal(10,2),
	gastos_estimados decimal(10,2),

	--El profesor aparece en �sta tabla en lugar de crear una nueva tabla "imparte" porque en la historia 1 pone que una actividad tiene un �nico profesor
	id_profesor decimal(10,2) not null,
	remuneracion_profesor decimal(10,2),
	
	constraint pk_actividades primary key(id_actividad),
	constraint fk_actividades_profesores foreign key(id_profesor) references profesores(id_profesor),
	constraint ck_actividades check (estado in ('planificada', 'en periodo de inscripci�n', 'cerrada', 'cancelada'))
);
create table inscripcion(
	id_actividad decimal(10,2) not null,
	id_profesional decimal(10,2) not null,

	fecha_inscripcion date,
	cantidad_pagada decimal(10,2),
	estado varchar(100) not null,

	constraint pk_inscripcion primary key (id_actividad, id_profesional),
	constraint fk_inscripcion_actividades foreign key (id_actividad) references actividades(id_actividad),
 	constraint ck_inscripcion check (estado in ('recibida', 'cobrada', 'cancelada'))
);
-- La tabla transferencias se crea por que en la historia 3: 
-- 		"Dichas incidencias pueden requerir que los inscritos o el COIIPA realicen transferencias que tambi�n deben quedar registradas en el sistema."
-- Y en la historia 5:
--		"Las devoluciones se realizar�n mediante transferencias cuyos datos deben quedar convenientemente registrados."
-- Por lo tanto cada vez que se transfiera dinero en una inscripci�n tambi�n se deber� actualizar la tabla transferencias
create table transferencias_profesional(
	id_transferencia_profesional decimal(10,2)not null,
	id_actividad decimal(10,2) not null,
	id_profesional decimal(10,2) not null,

	cantidad_pagada_por_COIIPA decimal(10,2),
	cantidad_pagada_por_profesional decimal(10,2),
	
	constraint pk_transferencias_profesional primary key(id_transferencia_profesional),
	constraint fk_transferencias_profesional foreign key (id_actividad, id_profesional) references inscripcion(id_actividad, id_profesional)
);
-- Historia 6:
-- 		"Al igual que con los cobros, pueden existir incidencias en el pago que requieran la realizaci�n de transferencias posteriores para compensar posibles errores."
create table transferencias_profesor(
	id_transferencia_profesor decimal(10,2)not null,
	id_actividad decimal(10,2) not null,
	id_profesor decimal(10,2) not null,

	cantidad_pagada_por_COIIPA decimal(10,2),
	cantidad_pagada_por_profesor decimal(10,2),
	
	constraint pk_transferencias_profesor primary key(id_transferencia_profesor),
	constraint fk_transferencias_profesor_actividades foreign key (id_actividad) references actividades(id_actividad),
	constraint fk_transferencias_profesor_profesores foreign key (id_profesor) references profesores(id_profesor)
);
-- Historia 7:
-- 		"Como profesional quiero inscribir a m�s de una persona en una actividad. El pago puede ser individual o conjunto."
create table lista_inscritos(
 	id_profesional decimal(10,2) not null,
	id_inscripcion decimal(10,2) not null,
	id_inscritos decimal(10,2) not null,

	constraint pk_inscripcion_profesional primary key(id_inscritos),
	constraint fk_inscripcion_lista foreign key (id_inscripcion) references inscripciones(id_inscripcion),
	constraint fk_inscripcion_profesionales foreign key (id_profesional) references profesionales(id_profesional),
);
---historia 8 y 9 empresa
create table empresas(
	id_empresa decimal(10,2) not null,
	nombre varchar(100) not null,	
	constraint pk_empresas primary key(id_empresa)	
);

create table profesores_empresa(
	id_empresa decimal(10,2) not null,
	id_profesor decimal(10,2) not null,	
	constraint pk_profesores_empresa primary key (id_empresa, id_profesor),
	constraint fk_profesores_empresa_empresa foreign key (id_empresa) references actividades(id_empresa),
	constraint fk_profesores_empresa_profesores foreign key (id_profesor) references profesores(id_profesor)
);
--historia 8 pago empresa
create table transferencias_empresa(
	id_empresa decimal(10,2) not null,
	nif varchar(100)  ,
	facturaid varchar(100) ,
	id_transferencia_empresa decimal(10,2) not null,
	fecha_transferencia_empresa date,
	cantidad_pagada_por_COIIPA number,
	cantidad_pagada_por_empresa number,

	constraint pk_tranferencias_empresa primary key (id_transferencia_empresa),
	constraint fk_tranferencia_empresa foreign key (id_empresa) references actividades(id_empresa),
);