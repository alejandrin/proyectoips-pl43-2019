package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.dto.DatosEmpresaDto;

public class ParserDatosEmpresa {

	public DatosEmpresaDto parseEmpresa(ResultSet rs) throws SQLException {
		DatosEmpresaDto empresa = null;
		
		if(rs.next()) {
			empresa = new DatosEmpresaDto();
			empresa.id_empresa = rs.getInt("id_empresa");
			empresa.nombre = rs.getString("nombre");
			empresa.cif = rs.getString("cif");
			empresa.email = rs.getString("email");
			empresa.representante = rs.getString("representante");
			empresa.telefono = rs.getString("telefono");
		}
		
		return empresa;
	}
	
	public List<DatosEmpresaDto> parseEmpresas(ResultSet rs) throws SQLException{
		List<DatosEmpresaDto> empresas = new ArrayList<DatosEmpresaDto>();
		
		DatosEmpresaDto empresa = parseEmpresa(rs);
		while(empresa != null) {
			empresas.add(empresa);
			empresa = parseEmpresa(rs);
		}
		
		return empresas;
	}
	
}
