package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtil {
	private static String URL = "jdbc:hsqldb:hsql://localhost";
	private static String USER = "sa";
	private static String PASS = "";
	
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USER, PASS);
	}
	
	/**
	 * Ejecuta una query a partir de un String y un grupo de datos.
	 * @param query, String que contiene la consulta
	 * @param datos conjunto de datos utilizados como parametros en la consulta
	 * @return resultado de la consulta
	 * @throws SQLException
	 */
	public static ResultSet executeQuery(String query, Object... datos) throws SQLException {
		Connection con = getConnection();
		PreparedStatement pst = con.prepareStatement(query);
		for(int i=0; i<datos.length; i++){
			pst.setObject(i+1, datos[i]);
		}
		ResultSet res = pst.executeQuery();
		pst.close();
		con.close();
		return res;
	}
	
	public static int executeUpdate(String query, Object... datos) throws SQLException {
		Connection con = getConnection();
		PreparedStatement pst = con.prepareStatement(query);
		for(int i=0; i<datos.length; i++){
			pst.setObject(i+1, datos[i]);
		}
		int res = pst.executeUpdate();
		if(res==0)
		 
		pst.close();
		con.close();
		return res;
	}

	public static void close(ResultSet rs, Statement st, Connection c) {
		close(rs);
		close(st);
		close(c);
	}

	public static void close(ResultSet rs, Statement st) {
		close(rs);
		close(st);
	}

	protected static void close(ResultSet rs) {
		if (rs != null) try { rs.close(); } catch(SQLException e) {/* ignore */}
	}

	public static void close(Statement st) {
		if (st != null ) try { st.close(); } catch(SQLException e) {/* ignore */}
	}

	public static void close(Connection c) {
		if (c != null) try { c.close(); } catch(SQLException e) {/* ignore */}
	}

	public static Connection createThreadConnection() throws SQLException {
		Connection con = getConnection();
		threadConnection.set(con);
		return con;
	}

	private static ThreadLocal<Connection> threadConnection = new ThreadLocal<Connection>();

	public static Connection getCurrentConnection() {
		return threadConnection.get();
	}
}
