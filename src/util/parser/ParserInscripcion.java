package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.EstadoInscripcion;
import logica.dto.ActividadDto;
import logica.dto.InscripcionDto;
import logica.dto.ProfesionalDto;

public class ParserInscripcion {

	public List<InscripcionDto> parseInscripciones(ResultSet rs) throws SQLException{
		List<InscripcionDto> inscripciones = new ArrayList<InscripcionDto>();
		
		while(rs.next()) {
			inscripciones.add(parseInscripcion(rs));
		}
		
		return inscripciones;
	}
	
	public InscripcionDto parseInscripcion(ResultSet rs) throws SQLException {
		InscripcionDto ins = new InscripcionDto();
		ProfesionalDto prof = new ProfesionalDto();
		ActividadDto act = new ActividadDto();
 		prof.id = rs.getInt("id_profesional");
		prof.nombre = rs.getString("nombre");
		
		act.id = rs.getInt("id_actividad");
		act.precio = rs.getInt("precio");
 		
		ins.id = rs.getInt("id_inscripcion"); 
		ins.actividad = act;
		ins.profesional = prof;
		ins.cantidadPagada = rs.getDouble("cantidad_pagada");
		ins.fechaInscripcion = rs.getDate("fecha_inscripcion");
 		ins.estado = EstadoInscripcion.valueOf(EstadoInscripcion.class, rs.getString("estado").toUpperCase());
 		ins.precio = rs.getDouble("precio");
 		ins.plaza = rs.getString("plaza");
 		ins.cantidad_a_devolver = rs.getDouble("cd");
 		return ins;
	}
	
}