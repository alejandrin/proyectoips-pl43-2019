package util.parser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logica.EstadoFactura;
import logica.dto.ActividadDto;
import logica.dto.FacturaProfesorDto;
import logica.dto.ProfesorDto;

public class ParserFacturasProfesor {
	
	public List<FacturaProfesorDto> parseFacturas(ResultSet rs) throws SQLException{
		List<FacturaProfesorDto> inscripciones = new ArrayList<FacturaProfesorDto>();
		
		while(rs.next()) {
			inscripciones.add(parseFactura(rs));
		}
		
		return inscripciones;
	}
	
	public FacturaProfesorDto parseFactura(ResultSet rs) throws SQLException {
		FacturaProfesorDto ins = new FacturaProfesorDto();
		ProfesorDto prof = new ProfesorDto();
		ActividadDto act = new ActividadDto();
 		prof.id = rs.getInt("id_profesor");
		
		act.id = rs.getInt("id_actividad");
 		
		ins.id = rs.getInt("id_factura"); 
		ins.actividad = act;
		ins.profesor = prof;
		ins.precio_a_pagar = rs.getDouble("precio_a_pagar");
		ins.precio_pagado = rs.getDouble("precio_pagado");
		ins.fecha = rs.getDate("fecha_factura");
 		ins.estado = EstadoFactura.valueOf(EstadoFactura.class, rs.getString("estado_factura").toUpperCase());
 		return ins;
	}
	
	public FacturaProfesorDto parseFacturaB(ResultSet rs) throws SQLException {
		FacturaProfesorDto ins = null;
		if(rs.next()) {
		ins = new FacturaProfesorDto();
		ProfesorDto prof = new ProfesorDto();
		ActividadDto act = new ActividadDto();
 		prof.id = rs.getInt("id_profesor");
		
		act.id = rs.getInt("id_actividad");
 		
		ins.id = rs.getInt("id_factura"); 
		ins.actividad = act;
		ins.profesor = prof;
		ins.precio_a_pagar = rs.getDouble("precio_a_pagar");
		ins.precio_pagado = rs.getDouble("precio_pagado");
		ins.fecha = rs.getDate("fecha_factura");
 		ins.estado = EstadoFactura.valueOf(EstadoFactura.class, rs.getString("estado_factura").toUpperCase());
		}
 		return ins;
	}
}
