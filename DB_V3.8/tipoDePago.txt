ALTER TABLE transferencias_profesor ADD COLUMN TIPO varchar(15);

ALTER TABLE transferencias_profesional ADD COLUMN TIPO varchar(15);

ALTER TABLE transferencias_empresa ADD COLUMN TIPO varchar(15);

UPDATE transferencias_empresa SET tipo='TRANSFERENCIA';

UPDATE transferencias_profesional SET tipo='TRANSFERENCIA';

UPDATE transferencias_profesor SET tipo='TRANSFERENCIA';